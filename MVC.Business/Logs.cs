﻿
using DocFlow.Data;
using DocFlow.Data.Repository;
using System;

namespace DocFlow.Repository
{
    public class Logs : Core
    {
        private readonly LogIntegrationRepository _integrationLog;


        public Logs() : base()
        {
            _integrationLog = new LogIntegrationRepository();
        }

        public bool AddIntegrationLog(string entityName, long? entityId, string erpId, string status, string msg)
        {
            try
            {
                _integrationLog.Add(new Log_Integration()
                {
                    Date = DateTime.Now,
                    EntityERPId = erpId,
                    EntityId = entityId,
                    EntityName = entityName,
                    Msg = msg,
                    Status = status
                });
            }
            catch (Exception)
            {

                return false;
            }
            return true;
        }
    }
}
