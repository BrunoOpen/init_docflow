﻿
using System;
using System.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;
using DocFlow.Data.Repository;
using DocFlow.Data.Texts;


namespace DocFlow.Repository
{
    public class Texts:Core
    {
        private readonly TextRepository _textRepository;
        private readonly TextTypeRepository _texttypeRepository;

        public Texts():base()
        {
            _textRepository = new TextRepository();
            _texttypeRepository = new TextTypeRepository();
        }

        public List<Data.SelectItem> GetTextTypes()
        {
            return _texttypeRepository.GetAll().Select(x => new Data.SelectItem()
            {
                Text = x.Name,
                Value = x.Name
            }).ToList();
        }

        public void CRUDText(DocFlow.Data.Texts.TextModel value, int type)
        {
            SqlParameter[] sqlparams = {
                SqlHelper.BuildInt("@Id",value.Id),
                SqlHelper.BuildString("@TextType",value.TextType),
                SqlHelper.BuildString("@Text",value.Text),
                SqlHelper.BuildInt("@Type",type)
                };
            value = _spRepository.GetSingle<TextModel>("CRUDTextModel", sqlparams);
        }

        public List<DocFlow.Data.Texts.TextModel> GetTexts()
        {
            List<DocFlow.Data.Texts.TextModel> result = null;
            try
            {
                SqlParameter[] sqlparams = {
                };
                result = _spRepository.GetList<DocFlow.Data.Texts.TextModel>("GetTexts", sqlparams);
            }
            catch (Exception)
            {
            }
            return result;
        }

        public void AddNewGarantee(string text)
        {
            _textRepository.Add(new DocFlow.Data.TextModel {
                Description = text,
                TextModelTypeID = 1
            });
        }

        public void AddNewExclusion(string text)
        {
            _textRepository.Add(new DocFlow.Data.TextModel
            {
                Description = text,
                TextModelTypeID = 2
            });
        }

        public void AddNewObservation(string text)
        {
            _textRepository.Add(new DocFlow.Data.TextModel
            {
                Description = text,
                TextModelTypeID = 3
            });
        }

        public void AddNewDeadline(string text)
        {
            _textRepository.Add(new DocFlow.Data.TextModel
            {
                Description = text,
                TextModelTypeID = 4
            });
        }

        public void AddNewTechnician(string text)
        {
            _textRepository.Add(new DocFlow.Data.TextModel
            {
                Description = text,
                TextModelTypeID = 5
            });
        }

        public void AddNewText(int texttypeid, string text)
        {
            _textRepository.Add(new DocFlow.Data.TextModel
            {
                Description = text,
                TextModelTypeID = texttypeid
            });
        }

    }
}
