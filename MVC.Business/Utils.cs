﻿using System;


namespace DocFlow.Repository
{
    public class Utils
    {
        public static string[] Months = { "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" };

        public static string[] CompostYearPart = { "T", "S" };

        public static string GetMonth(DateTime date)
        {
            return Months[date.Month - 1];
        }

        public static string GetMonth(int monthNumber)
        {
            return Months[monthNumber - 1];
        }

        public static string GetMonthYear(DateTime date)
        {
            return string.Format("{0} {1}", Months[date.Month - 1], date.Year);
        }

        public static string GetMonthYear(int monthNumber, int year)
        {
            return string.Format("{0} {1}", Months[monthNumber - 1], year);
        }

        public static string GetTrimesterYear(int period, int year)
        {
            return string.Format("{0}.º {1} {2}", period, CompostYearPart[0], year);
        }

        public static string GetSemesterYear(int period, int year)
        {
            return string.Format("{0}.º {1} {2}", period, CompostYearPart[1], year);
        }
    }
}
