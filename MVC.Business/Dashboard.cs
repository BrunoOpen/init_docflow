﻿
using DocFlow.Data;
using DocFlow.Data.Dashboards;
using DocFlow.Data.Repository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace DocFlow.Repository
{
    public class Dashboard
    {
        private readonly GenericSProcRepository _spRepository;
        private readonly DashSourcesRepository _sourceRepository;
        private readonly DashValueTypesRepository _valueTypesRepository;
        private readonly DashDetailTypesRepository _detailtypeRepository;

        public Dashboard()
        {
            _spRepository = new GenericSProcRepository();
            _sourceRepository = new DashSourcesRepository();
            _detailtypeRepository = new DashDetailTypesRepository();
            _valueTypesRepository = new DashValueTypesRepository();
        }

        public List<DashValue> GetKPI(List<int> valueTypes, DateTime monthDate)
        {
            SqlParameter[] sqlparams = {
                SqlHelper.BuildList("@ValueTypes", valueTypes),
                SqlHelper.BuildDate("@MonthDate",monthDate)
            };
            return _spRepository.GetList<DashValue>("GetKPI", sqlparams);
        }


        public List<dash_Source> GetAllSources()
        {
            return _sourceRepository.GetAll();
        }


        public List<DashValue> GetSourceValueList(int sourceid, DateTime monthDate)
        {
            SqlParameter[] sqlparams = {
                SqlHelper.BuildInt("@SourceID", sourceid),
                SqlHelper.BuildDate("@MonthDate",monthDate)
            };
            return _spRepository.GetList<DashValue>("GetList", sqlparams);
        }

        public List<SelectItem> GetDropItemsDetailTypes(int ValueTypeID)
        {
            List<SelectItem> result = _detailtypeRepository.GetAll().Select(x => new SelectItem { Selected = false, Text = x.Name, Value = x.DetailTypeID.ToString() }).ToList();
            result.OrderBy(x => x.Value).First().Selected = true;
            return result;
        }

        public List<ChartValue> GetChartValueList(int columnValueTypeId, int? lineValueTypeId, DateTime monthDate)
        {
            SqlParameter[] sqlparams = {
                SqlHelper.BuildInt("@ColumnValueTypeID", columnValueTypeId),
                SqlHelper.BuildInt("@LineValueTypeID", lineValueTypeId),
                SqlHelper.BuildDate("@MonthDate",monthDate)
            };
            return _spRepository.GetList<ChartValue>("GetGraph", sqlparams);
        }

        public List<ChartValueDetails> GetChartDetailedValueList(int columnValueTypeId, int detailTypeId, DateTime monthDate)
        {
            SqlParameter[] sqlparams = {
                SqlHelper.BuildInt("@ColumnValueTypeID", columnValueTypeId),
                SqlHelper.BuildInt("@DetailTypeId", detailTypeId),
                SqlHelper.BuildDate("@MonthDate",monthDate)
            };
            return _spRepository.GetList<ChartValueDetails>("GetGraphDetail", sqlparams);
        }

        public dash_ValueType GetDashboardValueType(int id)
        {
            return _valueTypesRepository.GetSingle(u => u.ValueTypeID == id);
        }
    }
}
