﻿using System.Collections.Generic;
using System.Linq;
using System;
using DocFlow.Data.Repository;

namespace DocFlow.Repository
{
    public class Drops
    {
        private readonly GenericSProcRepository _spRepository;


        private readonly PeriodRepository _periodRepository;

        private readonly TextRepository _textRepository;

        public Drops()
        {
            _spRepository = new GenericSProcRepository();
            _periodRepository = new PeriodRepository();
            _textRepository = new TextRepository();
       
        }

        public List<string> GetExclusionList()
        {
            return _textRepository.GetList(x => x.TextModelTypeID == 2).Select(x => x.Description).ToList();
        }

    }
}
