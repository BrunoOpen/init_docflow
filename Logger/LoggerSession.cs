﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Logger
{
    public static class LoggerSession
    {
        private const string LOG = "CURRENT_LOG_ACTIVITY";

        public static int? GetLastLogId()
        {
            return HttpContext.Current.Session != null ? HttpContext.Current.Session[LOG] as int? : null;
        }

        public static void SetLastLogId(int logId)
        {
            HttpContext.Current.Session[LOG] = logId;
        }

    }
}
