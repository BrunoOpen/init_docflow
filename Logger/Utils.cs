﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;

namespace Logger
{
   
        public static class Utils
        {
            public static string GetIPAddress()
            {
                System.Web.HttpContext context = System.Web.HttpContext.Current;
                string ipAddress = "";
                try
                {
                    string tempIp = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                    if (!string.IsNullOrEmpty(tempIp))
                    {
                        string[] addresses = tempIp.Split(',');
                        if (addresses.Length != 0)
                        {
                            ipAddress = addresses[0];
                        }
                    }
                    else
                    {
                        ipAddress = context.Request.ServerVariables["REMOTE_ADDR"];
                    }
                }
                catch (Exception)
                {

                }


                return ipAddress;
            }

            public static string GetHostName()
            {
                HttpContext context = HttpContext.Current;
                string host = "";
                try
                {
                    host = HttpContext.Current.Request.ServerVariables["REMOTE_HOST"];
                }
                catch (Exception)
                {

                }
                return host;
            }

            public static string GetSessionId()
            {
                string sessionID = "";
                try
                {
                    if (HttpContext.Current.Session == null)
                    {
                        sessionID = HttpContext.Current.Request.Params.Get("ASP.NET_SessionId");
                    }
                    else
                    {
                        sessionID = HttpContext.Current.Session.SessionID;
                    }

                }
                catch (Exception)
                {

                }
                return sessionID;
            }

            public static string GetUserAgent()
            {
                string userAgent = "";
                try
                {
                    userAgent = HttpContext.Current.Request.UserAgent;
                }
                catch (Exception)
                {

                }
                return userAgent;
            }

            public static string GetParameters()
            {
                string parameters = "";
                try
                {
                    parameters = new JavaScriptSerializer().Serialize(HttpContext.Current.Request.Params.ToDictionary());
                }
                catch (Exception)
                {

                }
                return parameters;
            }

            public static string GetCurrentPageUrl()
            {
                string url = "";
                try
                {
                    url = HttpContext.Current.Request.Url.AbsolutePath;
                }
                catch (Exception)
                {

                }

                return url;
            }

          
    }

    public static class NVCExtender
    {
        public static IDictionary<string, string[]> ToDictionary(
                                    this NameValueCollection source)
        {
            return source.AllKeys.ToDictionary(k => k, k => source.GetValues(k));
        }
    }
}
