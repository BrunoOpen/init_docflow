﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logger
{
    public static class Log
    {


        public static int Error(Exception ex, string className, string method, int userId)
        {
            return (new LoggerRepository()).LogError(ex, className, method, userId);
        }

        public static int Error(string message, string className, string method, int userId)
        {
            return (new LoggerRepository()).LogError(message, className, method, userId);
        }

        public static int UserActivity(int logType, string username = "", int? userId = null)
        {
            return (new LoggerRepository()).LogUserActivity(logType, username, userId);
        }

        public static void ProcessLog(string controller, string action)
        {

            if (!string.IsNullOrEmpty(controller) && !string.IsNullOrEmpty(action) && action != "Login" && action != "LogOff")
            {
                (new LoggerRepository()).LogUserActivity(4, null, null);
            }
        }

        public static void AddErrorToLogActivity(int errorId)
        {
            (new LoggerRepository()).AddErrorToLogActivity(errorId);
        }

        public static void Debug(string context, int? contextId, string message, int userId)
        {
            (new LoggerRepository()).LogDebugMessage(context, contextId, message, userId);
        }


    }
}

