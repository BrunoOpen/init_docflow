﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System.Security.Claims;
using System.Threading.Tasks;
using DocFlow.DAL;

namespace DocFlow
{
    public class ApplicationSignInManager : SignInManager<AppAccount, int>
    {
        public ApplicationSignInManager(AppAccountManager userManager, IAuthenticationManager authenticationManager) :
            base(userManager, authenticationManager) { }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(AppAccount user)
        {
            return user.GenerateUserIdentityAsync((AppAccountManager)UserManager);
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<AppAccountManager>(), context.Authentication);
        }
    }
}