namespace DocFlow.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Permissions : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AppInstances",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InstanceId = c.Int(nullable: false),
                        ApplicationId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Applications", t => t.ApplicationId, cascadeDelete: true)
                .ForeignKey("dbo.Instances", t => t.InstanceId, cascadeDelete: true)
                .Index(t => t.InstanceId)
                .Index(t => t.ApplicationId);
            
            CreateTable(
                "dbo.Applications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(maxLength: 50),
                        Name = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Instances",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 50),
                        Name = c.String(maxLength: 250),
                        ConnString = c.String(),
                        DBName = c.String(maxLength: 250),
                        BillEntityId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BillEntities", t => t.BillEntityId)
                .Index(t => t.BillEntityId);
            
            CreateTable(
                "dbo.BillEntities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(maxLength: 50),
                        Name = c.String(maxLength: 250),
                        VatNumber = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Permissions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 250),
                        ParentId = c.Int(nullable: false),
                        PermissionTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PermissionTypes", t => t.PermissionTypeId, cascadeDelete: true)
                .Index(t => t.PermissionTypeId);
            
            CreateTable(
                "dbo.PermissionTypes",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 250),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProfilePermissions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PermissionL3Id = c.Int(),
                        PermissionL2Id = c.Int(),
                        PermissionL1Id = c.Int(),
                        ProfileId = c.Int(),
                        InstanceId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Instances", t => t.InstanceId)
                .ForeignKey("dbo.Permissions", t => t.PermissionL1Id)
                .ForeignKey("dbo.Permissions", t => t.PermissionL2Id)
                .ForeignKey("dbo.Permissions", t => t.PermissionL3Id)
                .ForeignKey("dbo.Profiles", t => t.ProfileId)
                .Index(t => t.PermissionL3Id)
                .Index(t => t.PermissionL2Id)
                .Index(t => t.PermissionL1Id)
                .Index(t => t.ProfileId)
                .Index(t => t.InstanceId);
            
            CreateTable(
                "dbo.Profiles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 250),
                        ApplicationId = c.Int(),
                        InstanceId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Applications", t => t.ApplicationId)
                .ForeignKey("dbo.Instances", t => t.InstanceId)
                .Index(t => t.ApplicationId)
                .Index(t => t.InstanceId);
            
            CreateTable(
                "dbo.Resources",
                c => new
                    {
                        Culture = c.String(nullable: false, maxLength: 10, unicode: false),
                        Name = c.String(nullable: false, maxLength: 100, unicode: false),
                        Value = c.String(maxLength: 4000),
                    })
                .PrimaryKey(t => new { t.Culture, t.Name });
            
            CreateTable(
                "dbo.UserProfiles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AppAcountId = c.Int(nullable: false),
                        ProfileId = c.Int(nullable: false),
                        InstanceId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Accounts", t => t.AppAcountId, cascadeDelete: true)
                .ForeignKey("dbo.Instances", t => t.InstanceId)
                .ForeignKey("dbo.Profiles", t => t.ProfileId, cascadeDelete: true)
                .Index(t => t.AppAcountId)
                .Index(t => t.ProfileId)
                .Index(t => t.InstanceId);
            
            CreateTable(
                "dbo.UserInstaceApps",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AppAcountId = c.Int(nullable: false),
                        InstanceId = c.Int(),
                        ApplicationId = c.Int(),
                        isDefault = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Accounts", t => t.AppAcountId, cascadeDelete: true)
                .ForeignKey("dbo.Applications", t => t.ApplicationId)
                .ForeignKey("dbo.Instances", t => t.InstanceId)
                .Index(t => t.AppAcountId)
                .Index(t => t.InstanceId)
                .Index(t => t.ApplicationId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserInstaceApps", "InstanceId", "dbo.Instances");
            DropForeignKey("dbo.UserInstaceApps", "ApplicationId", "dbo.Applications");
            DropForeignKey("dbo.UserInstaceApps", "AppAcountId", "dbo.Accounts");
            DropForeignKey("dbo.UserProfiles", "ProfileId", "dbo.Profiles");
            DropForeignKey("dbo.UserProfiles", "InstanceId", "dbo.Instances");
            DropForeignKey("dbo.UserProfiles", "AppAcountId", "dbo.Accounts");
            DropForeignKey("dbo.ProfilePermissions", "ProfileId", "dbo.Profiles");
            DropForeignKey("dbo.Profiles", "InstanceId", "dbo.Instances");
            DropForeignKey("dbo.Profiles", "ApplicationId", "dbo.Applications");
            DropForeignKey("dbo.ProfilePermissions", "PermissionL3Id", "dbo.Permissions");
            DropForeignKey("dbo.ProfilePermissions", "PermissionL2Id", "dbo.Permissions");
            DropForeignKey("dbo.ProfilePermissions", "PermissionL1Id", "dbo.Permissions");
            DropForeignKey("dbo.ProfilePermissions", "InstanceId", "dbo.Instances");
            DropForeignKey("dbo.Permissions", "PermissionTypeId", "dbo.PermissionTypes");
            DropForeignKey("dbo.AppInstances", "InstanceId", "dbo.Instances");
            DropForeignKey("dbo.Instances", "BillEntityId", "dbo.BillEntities");
            DropForeignKey("dbo.AppInstances", "ApplicationId", "dbo.Applications");
            DropIndex("dbo.UserInstaceApps", new[] { "ApplicationId" });
            DropIndex("dbo.UserInstaceApps", new[] { "InstanceId" });
            DropIndex("dbo.UserInstaceApps", new[] { "AppAcountId" });
            DropIndex("dbo.UserProfiles", new[] { "InstanceId" });
            DropIndex("dbo.UserProfiles", new[] { "ProfileId" });
            DropIndex("dbo.UserProfiles", new[] { "AppAcountId" });
            DropIndex("dbo.Profiles", new[] { "InstanceId" });
            DropIndex("dbo.Profiles", new[] { "ApplicationId" });
            DropIndex("dbo.ProfilePermissions", new[] { "InstanceId" });
            DropIndex("dbo.ProfilePermissions", new[] { "ProfileId" });
            DropIndex("dbo.ProfilePermissions", new[] { "PermissionL1Id" });
            DropIndex("dbo.ProfilePermissions", new[] { "PermissionL2Id" });
            DropIndex("dbo.ProfilePermissions", new[] { "PermissionL3Id" });
            DropIndex("dbo.Permissions", new[] { "PermissionTypeId" });
            DropIndex("dbo.Instances", new[] { "BillEntityId" });
            DropIndex("dbo.AppInstances", new[] { "ApplicationId" });
            DropIndex("dbo.AppInstances", new[] { "InstanceId" });
            DropTable("dbo.UserInstaceApps");
            DropTable("dbo.UserProfiles");
            DropTable("dbo.Resources");
            DropTable("dbo.Profiles");
            DropTable("dbo.ProfilePermissions");
            DropTable("dbo.PermissionTypes");
            DropTable("dbo.Permissions");
            DropTable("dbo.BillEntities");
            DropTable("dbo.Instances");
            DropTable("dbo.Applications");
            DropTable("dbo.AppInstances");
        }
    }
}
