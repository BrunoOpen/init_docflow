namespace DocFlow.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Actions_I18N",
                c => new
                    {
                        ActionID = c.Int(nullable: false),
                        LanguageID = c.Int(nullable: false),
                        NameT = c.String(),
                        DescriptionT = c.String(),
                    })
                .PrimaryKey(t => new { t.ActionID, t.LanguageID })
                .ForeignKey("dbo.Actions", t => t.ActionID, cascadeDelete: true)
                .ForeignKey("dbo.Languages", t => t.LanguageID, cascadeDelete: true)
                .Index(t => t.ActionID)
                .Index(t => t.LanguageID);
            
            CreateTable(
                "dbo.Actions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Description = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.ActionGroupActions",
                c => new
                    {
                        ActionID = c.Int(nullable: false),
                        ActionGroupID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ActionID, t.ActionGroupID })
                .ForeignKey("dbo.Actions", t => t.ActionID, cascadeDelete: true)
                .ForeignKey("dbo.ActionGroups", t => t.ActionGroupID, cascadeDelete: true)
                .Index(t => t.ActionID)
                .Index(t => t.ActionGroupID);
            
            CreateTable(
                "dbo.ActionGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Code = c.String(),
                        Description = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        MainActionGroupID = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ActionGroups", t => t.MainActionGroupID)
                .Index(t => t.MainActionGroupID);
            
            CreateTable(
                "dbo.RoleActionGroups",
                c => new
                    {
                        RoleID = c.Int(nullable: false),
                        ActionGroupID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.RoleID, t.ActionGroupID })
                .ForeignKey("dbo.ActionGroups", t => t.ActionGroupID, cascadeDelete: true)
                .ForeignKey("dbo.Roles", t => t.RoleID, cascadeDelete: true)
                .Index(t => t.RoleID)
                .Index(t => t.ActionGroupID);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AccountsRoles",
                c => new
                    {
                        AccountID = c.Int(nullable: false),
                        RoleID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.AccountID, t.RoleID })
                .ForeignKey("dbo.Accounts", t => t.AccountID, cascadeDelete: true)
                .ForeignKey("dbo.Roles", t => t.RoleID, cascadeDelete: true)
                .Index(t => t.AccountID)
                .Index(t => t.RoleID);
            
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        IsSystem = c.Boolean(nullable: false),
                        EntityID = c.Int(),
                        OldUser = c.Guid(),
                        Image = c.Binary(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Entitys", t => t.EntityID)
                .Index(t => t.EntityID)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AccountsClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Accounts", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.DebugLogs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserID = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Context = c.String(),
                        ContextID = c.Int(),
                        Msg = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Accounts", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.Entitys",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        PhoneNumber = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ErrorLogs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserID = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Msg = c.String(),
                        Method = c.String(),
                        Class = c.String(),
                        Source = c.String(),
                        Target = c.String(),
                        StackTrace = c.String(),
                        Action = c.String(),
                        Parameters = c.String(storeType: "ntext"),
                        SessionID = c.String(),
                        UserAgent = c.String(),
                        IPAddress = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Accounts", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.AccountsLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.Accounts", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AccountsActions",
                c => new
                    {
                        AccountID = c.Int(nullable: false),
                        ActionID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.AccountID, t.ActionID })
                .ForeignKey("dbo.Accounts", t => t.AccountID, cascadeDelete: true)
                .ForeignKey("dbo.Actions", t => t.ActionID, cascadeDelete: true)
                .Index(t => t.AccountID)
                .Index(t => t.ActionID);
            
            CreateTable(
                "dbo.Languages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ShortName = c.String(),
                        Code = c.String(),
                        Image = c.String(),
                        SQLServerAlias = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        IsDefault = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ActionGroups_I18N",
                c => new
                    {
                        AppActionGroupID = c.Int(nullable: false),
                        LanguageID = c.Int(nullable: false),
                        NameT = c.String(),
                        DescriptionT = c.String(),
                    })
                .PrimaryKey(t => new { t.AppActionGroupID, t.LanguageID })
                .ForeignKey("dbo.ActionGroups", t => t.AppActionGroupID, cascadeDelete: true)
                .ForeignKey("dbo.Languages", t => t.LanguageID, cascadeDelete: true)
                .Index(t => t.AppActionGroupID)
                .Index(t => t.LanguageID);
            
            CreateTable(
                "dbo.ActionVariables",
                c => new
                    {
                        AppActionId = c.Int(nullable: false),
                        VariableID = c.Int(nullable: false),
                        IsRequired = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.AppActionId, t.VariableID })
                .ForeignKey("dbo.Actions", t => t.AppActionId, cascadeDelete: true)
                .ForeignKey("dbo.Variables", t => t.VariableID, cascadeDelete: true)
                .Index(t => t.AppActionId)
                .Index(t => t.VariableID);
            
            CreateTable(
                "dbo.Variables",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        EntityTable = c.String(),
                        EntityColumn = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.VariableValues",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        VariableID = c.Int(nullable: false),
                        Value = c.Int(nullable: false),
                        Code = c.String(),
                        ShortName = c.String(),
                        LongName = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Variables", t => t.VariableID, cascadeDelete: true)
                .Index(t => t.VariableID);
            
            CreateTable(
                "dbo.Contexts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Account_Id = c.Int(),
                        Action_Id = c.Int(),
                        ActionGroup_Id = c.Int(),
                        VariableValue_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Accounts", t => t.Account_Id)
                .ForeignKey("dbo.Actions", t => t.Action_Id)
                .ForeignKey("dbo.ActionGroups", t => t.ActionGroup_Id)
                .ForeignKey("dbo.VariableValues", t => t.VariableValue_Id)
                .Index(t => t.Account_Id)
                .Index(t => t.Action_Id)
                .Index(t => t.ActionGroup_Id)
                .Index(t => t.VariableValue_Id);
            
            CreateTable(
                "dbo.Roles_I18N",
                c => new
                    {
                        AppRoleID = c.Int(nullable: false),
                        LanguageID = c.Int(nullable: false),
                        NameT = c.String(),
                        DescriptionT = c.String(),
                    })
                .PrimaryKey(t => new { t.AppRoleID, t.LanguageID })
                .ForeignKey("dbo.Roles", t => t.AppRoleID, cascadeDelete: true)
                .ForeignKey("dbo.Languages", t => t.LanguageID, cascadeDelete: true)
                .Index(t => t.AppRoleID)
                .Index(t => t.LanguageID);
            
            CreateTable(
                "dbo.LogActivities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LogActivityTypeID = c.Int(nullable: false),
                        UserID = c.Int(),
                        UserName = c.String(),
                        Date = c.DateTime(nullable: false),
                        Action = c.String(),
                        Parameters = c.String(storeType: "ntext"),
                        SessionID = c.String(),
                        UserAgent = c.String(),
                        IPAddress = c.String(),
                        ErrorLogID = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ErrorLogs", t => t.ErrorLogID)
                .ForeignKey("dbo.LogActivityTypes", t => t.LogActivityTypeID, cascadeDelete: true)
                .ForeignKey("dbo.Accounts", t => t.UserID)
                .Index(t => t.LogActivityTypeID)
                .Index(t => t.UserID)
                .Index(t => t.ErrorLogID);
            
            CreateTable(
                "dbo.LogActivityTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MenuActions",
                c => new
                    {
                        MenuID = c.Int(nullable: false),
                        ActionID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.MenuID, t.ActionID })
                .ForeignKey("dbo.Actions", t => t.ActionID, cascadeDelete: true)
                .ForeignKey("dbo.Menus", t => t.MenuID, cascadeDelete: true)
                .Index(t => t.MenuID)
                .Index(t => t.ActionID);
            
            CreateTable(
                "dbo.Menus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ParentID = c.Int(),
                        Name = c.String(),
                        Href = c.String(),
                        Icon = c.String(),
                        Order = c.Int(nullable: false),
                        VariableValueID = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Menus", t => t.ParentID)
                .ForeignKey("dbo.VariableValues", t => t.VariableValueID)
                .Index(t => t.ParentID)
                .Index(t => t.VariableValueID);
            
            CreateTable(
                "dbo.Menus_I18N",
                c => new
                    {
                        MenuID = c.Int(nullable: false),
                        LanguageID = c.Int(nullable: false),
                        NameT = c.String(),
                    })
                .PrimaryKey(t => new { t.MenuID, t.LanguageID })
                .ForeignKey("dbo.Languages", t => t.LanguageID, cascadeDelete: true)
                .ForeignKey("dbo.Menus", t => t.MenuID, cascadeDelete: true)
                .Index(t => t.MenuID)
                .Index(t => t.LanguageID);
            
            CreateTable(
                "dbo.Services",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Color = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Services_I18N",
                c => new
                    {
                        ServiceID = c.Int(nullable: false),
                        LanguageID = c.Int(nullable: false),
                        NameT = c.String(),
                    })
                .PrimaryKey(t => new { t.ServiceID, t.LanguageID })
                .ForeignKey("dbo.Languages", t => t.LanguageID, cascadeDelete: true)
                .ForeignKey("dbo.Services", t => t.ServiceID, cascadeDelete: true)
                .Index(t => t.ServiceID)
                .Index(t => t.LanguageID);
            
            CreateTable(
                "dbo.Operators",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Entitys", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Persons",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Entitys", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Providers",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsCommercialization = c.Boolean(nullable: false),
                        IsDESPProcessed = c.Boolean(nullable: false),
                        IRF = c.Decimal(nullable: false, precision: 18, scale: 2),
                        VendorCode = c.String(),
                        ShortName = c.String(),
                        NIF = c.String(),
                        Color = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Entitys", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Providers", "Id", "dbo.Entitys");
            DropForeignKey("dbo.Persons", "Id", "dbo.Entitys");
            DropForeignKey("dbo.Operators", "Id", "dbo.Entitys");
            DropForeignKey("dbo.Services_I18N", "ServiceID", "dbo.Services");
            DropForeignKey("dbo.Services_I18N", "LanguageID", "dbo.Languages");
            DropForeignKey("dbo.Menus_I18N", "MenuID", "dbo.Menus");
            DropForeignKey("dbo.Menus_I18N", "LanguageID", "dbo.Languages");
            DropForeignKey("dbo.MenuActions", "MenuID", "dbo.Menus");
            DropForeignKey("dbo.Menus", "VariableValueID", "dbo.VariableValues");
            DropForeignKey("dbo.Menus", "ParentID", "dbo.Menus");
            DropForeignKey("dbo.MenuActions", "ActionID", "dbo.Actions");
            DropForeignKey("dbo.LogActivities", "UserID", "dbo.Accounts");
            DropForeignKey("dbo.LogActivities", "LogActivityTypeID", "dbo.LogActivityTypes");
            DropForeignKey("dbo.LogActivities", "ErrorLogID", "dbo.ErrorLogs");
            DropForeignKey("dbo.Roles_I18N", "LanguageID", "dbo.Languages");
            DropForeignKey("dbo.Roles_I18N", "AppRoleID", "dbo.Roles");
            DropForeignKey("dbo.Contexts", "VariableValue_Id", "dbo.VariableValues");
            DropForeignKey("dbo.Contexts", "ActionGroup_Id", "dbo.ActionGroups");
            DropForeignKey("dbo.Contexts", "Action_Id", "dbo.Actions");
            DropForeignKey("dbo.Contexts", "Account_Id", "dbo.Accounts");
            DropForeignKey("dbo.ActionVariables", "VariableID", "dbo.Variables");
            DropForeignKey("dbo.VariableValues", "VariableID", "dbo.Variables");
            DropForeignKey("dbo.ActionVariables", "AppActionId", "dbo.Actions");
            DropForeignKey("dbo.ActionGroups_I18N", "LanguageID", "dbo.Languages");
            DropForeignKey("dbo.ActionGroups_I18N", "AppActionGroupID", "dbo.ActionGroups");
            DropForeignKey("dbo.Actions_I18N", "LanguageID", "dbo.Languages");
            DropForeignKey("dbo.Actions_I18N", "ActionID", "dbo.Actions");
            DropForeignKey("dbo.AccountsActions", "ActionID", "dbo.Actions");
            DropForeignKey("dbo.ActionGroupActions", "ActionGroupID", "dbo.ActionGroups");
            DropForeignKey("dbo.ActionGroups", "MainActionGroupID", "dbo.ActionGroups");
            DropForeignKey("dbo.RoleActionGroups", "RoleID", "dbo.Roles");
            DropForeignKey("dbo.AccountsRoles", "RoleID", "dbo.Roles");
            DropForeignKey("dbo.AccountsActions", "AccountID", "dbo.Accounts");
            DropForeignKey("dbo.AccountsLogins", "UserId", "dbo.Accounts");
            DropForeignKey("dbo.ErrorLogs", "UserID", "dbo.Accounts");
            DropForeignKey("dbo.Accounts", "EntityID", "dbo.Entitys");
            DropForeignKey("dbo.DebugLogs", "UserID", "dbo.Accounts");
            DropForeignKey("dbo.AccountsClaims", "UserId", "dbo.Accounts");
            DropForeignKey("dbo.AccountsRoles", "AccountID", "dbo.Accounts");
            DropForeignKey("dbo.RoleActionGroups", "ActionGroupID", "dbo.ActionGroups");
            DropForeignKey("dbo.ActionGroupActions", "ActionID", "dbo.Actions");
            DropIndex("dbo.Providers", new[] { "Id" });
            DropIndex("dbo.Persons", new[] { "Id" });
            DropIndex("dbo.Operators", new[] { "Id" });
            DropIndex("dbo.Services_I18N", new[] { "LanguageID" });
            DropIndex("dbo.Services_I18N", new[] { "ServiceID" });
            DropIndex("dbo.Menus_I18N", new[] { "LanguageID" });
            DropIndex("dbo.Menus_I18N", new[] { "MenuID" });
            DropIndex("dbo.Menus", new[] { "VariableValueID" });
            DropIndex("dbo.Menus", new[] { "ParentID" });
            DropIndex("dbo.MenuActions", new[] { "ActionID" });
            DropIndex("dbo.MenuActions", new[] { "MenuID" });
            DropIndex("dbo.LogActivities", new[] { "ErrorLogID" });
            DropIndex("dbo.LogActivities", new[] { "UserID" });
            DropIndex("dbo.LogActivities", new[] { "LogActivityTypeID" });
            DropIndex("dbo.Roles_I18N", new[] { "LanguageID" });
            DropIndex("dbo.Roles_I18N", new[] { "AppRoleID" });
            DropIndex("dbo.Contexts", new[] { "VariableValue_Id" });
            DropIndex("dbo.Contexts", new[] { "ActionGroup_Id" });
            DropIndex("dbo.Contexts", new[] { "Action_Id" });
            DropIndex("dbo.Contexts", new[] { "Account_Id" });
            DropIndex("dbo.VariableValues", new[] { "VariableID" });
            DropIndex("dbo.ActionVariables", new[] { "VariableID" });
            DropIndex("dbo.ActionVariables", new[] { "AppActionId" });
            DropIndex("dbo.ActionGroups_I18N", new[] { "LanguageID" });
            DropIndex("dbo.ActionGroups_I18N", new[] { "AppActionGroupID" });
            DropIndex("dbo.AccountsActions", new[] { "ActionID" });
            DropIndex("dbo.AccountsActions", new[] { "AccountID" });
            DropIndex("dbo.AccountsLogins", new[] { "UserId" });
            DropIndex("dbo.ErrorLogs", new[] { "UserID" });
            DropIndex("dbo.DebugLogs", new[] { "UserID" });
            DropIndex("dbo.AccountsClaims", new[] { "UserId" });
            DropIndex("dbo.Accounts", "UserNameIndex");
            DropIndex("dbo.Accounts", new[] { "EntityID" });
            DropIndex("dbo.AccountsRoles", new[] { "RoleID" });
            DropIndex("dbo.AccountsRoles", new[] { "AccountID" });
            DropIndex("dbo.RoleActionGroups", new[] { "ActionGroupID" });
            DropIndex("dbo.RoleActionGroups", new[] { "RoleID" });
            DropIndex("dbo.ActionGroups", new[] { "MainActionGroupID" });
            DropIndex("dbo.ActionGroupActions", new[] { "ActionGroupID" });
            DropIndex("dbo.ActionGroupActions", new[] { "ActionID" });
            DropIndex("dbo.Actions", "RoleNameIndex");
            DropIndex("dbo.Actions_I18N", new[] { "LanguageID" });
            DropIndex("dbo.Actions_I18N", new[] { "ActionID" });
            DropTable("dbo.Providers");
            DropTable("dbo.Persons");
            DropTable("dbo.Operators");
            DropTable("dbo.Services_I18N");
            DropTable("dbo.Services");
            DropTable("dbo.Menus_I18N");
            DropTable("dbo.Menus");
            DropTable("dbo.MenuActions");
            DropTable("dbo.LogActivityTypes");
            DropTable("dbo.LogActivities");
            DropTable("dbo.Roles_I18N");
            DropTable("dbo.Contexts");
            DropTable("dbo.VariableValues");
            DropTable("dbo.Variables");
            DropTable("dbo.ActionVariables");
            DropTable("dbo.ActionGroups_I18N");
            DropTable("dbo.Languages");
            DropTable("dbo.AccountsActions");
            DropTable("dbo.AccountsLogins");
            DropTable("dbo.ErrorLogs");
            DropTable("dbo.Entitys");
            DropTable("dbo.DebugLogs");
            DropTable("dbo.AccountsClaims");
            DropTable("dbo.Accounts");
            DropTable("dbo.AccountsRoles");
            DropTable("dbo.Roles");
            DropTable("dbo.RoleActionGroups");
            DropTable("dbo.ActionGroups");
            DropTable("dbo.ActionGroupActions");
            DropTable("dbo.Actions");
            DropTable("dbo.Actions_I18N");
        }
    }
}
