﻿var OLSort = function () {
    var sortDate = function () {
        //estes dois implicam que exista na pagina os scripts:
        //<script src="//cdn.datatables.net/plug-ins/1.10.11/sorting/datetime-moment.js"></script>
        //<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
        moment.locale('en', {
            months: [
                "Janeiro", "Fevereiro", "Marco", "Abril", "Maio", "Junho", "Julho",
                "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"
            ]
        });
        moment.locale('pt', {
            months: [
                "Janeiro", "Fevereiro", "Marco", "Abril", "Maio", "Junho", "Julho",
                "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"
            ]
        });

        $.fn.dataTable.moment('DD/MM/YYYY');
        $.fn.dataTable.moment('MMMM YYYY');
    }


    var sortNumeric = function () {
        //Para odenação por numeros formatados
        $.fn.dataTable.ext.order['custom-numeric'] = function (settings, col) {
            //return this.api().column(col, { order: 'index' }).nodes().map(function (td, i) {
            //    return $('input', td).val();
            //});

            //return this.api().column(col, { order: 'index' }).nodes().map(function (td, i) {
            //     return Utils.UnFormatNumber($(td).html());
            // });
            return this.api().column(col, { order: 'index' }).data().map(function (td, i) {
                return Utils.UnFormatNumber(td);
            });
        };


        $.fn.dataTable.ext.order['date-MMMM-YYYY'] = function (settings, col) {
            //return this.api().column(col, { order: 'index' }).nodes().map(function (td, i) {
            //    return $('input', td).val();
            //});

            //return this.api().column(col, { order: 'index' }).nodes().map(function (td, i) {
            //     return Utils.UnFormatNumber($(td).html());
            // });
            return this.api().column(col, { order: 'index' }).data().map(function (td, i) {
                return Number(td.split(' ')[1]) * 100 + Months.indexOf(td.split(' ')[0]);
            });
        };
    }

    //var sortText = function () {
    //    $.fn.dataTable.ext.order['string'] = function (settings, col) {
    //        //return this.api().column(col, { order: 'index' }).nodes().map(function (td, i) {
    //        //    return $('input', td).val();
    //        //});

    //        //return this.api().column(col, { order: 'index' }).nodes().map(function (td, i) {
    //        //     return Utils.UnFormatNumber($(td).html());
    //        // });

    //        return this.api().column(col, { order: 'index' }).data().map(function (td, i) {
    //            return td.replace(/Á/g, 'Ã');
    //        });
    //    };
    //}

    var textSort = function () {
        $.fn.DataTable.ext.order['std'] = function (settings, col) {
            return this.api().column(col, { order: 'index' }).data().map(function (td, i) {
                return IsNullOrEmpty(td) ? '' : (td.toString().indexOf("Março") >= 0 ? td : td.toString().trim().latinize());
                    });
        };
    }
    return {
        SortDate: function () {
            sortDate();
        },
        SortNumeric: function () {
            sortNumeric();
        }
        ,
        SortAll: function () {
            sortDate();
            sortNumeric();
            //sortText();
            textSort();
        }
    };
}();


//$.fn.DataTable.ext.oSort["string-desc"] = function (settings, col) {
//    debugger;
//    return this.api().column(col, { order: 'index' }).data().map(function (td, i) {
//        debugger;
//        return Utils.UnFormatNumber(td);
//    });
//};



