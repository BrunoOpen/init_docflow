﻿using System;

namespace DocFlow.Core.Error
{
    public class ExceptionExtensions
    {

    }
    [Serializable]
    public class SessionExpiredException : Exception
    {

        public SessionExpiredException()
            : base()
        {

        }
        public SessionExpiredException(string message)
            : base(message)
        {

        }
        public SessionExpiredException(string message, Exception inner)
            : base(message, inner)
        {

        }
    }
}