﻿using DocFlow.Controllers;
using DocFlow.Core.Extensions;
using DocFlow.Services;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace DocFlow.Core.Error
{
    public static class ErrorHandler
    {
        public static void ProccessError(){
            HttpResponse response = HttpContext.Current.Response;
            HttpServerUtility server = HttpContext.Current.Server;
            
            try{

                if (server.GetLastError().GetType() == typeof(HttpException))
                {
                    if (server.GetLastError().Message.Contains("NoCatch") || server.GetLastError().Message.Contains("maxUrlLength"))
                        return;
                    HttpException httpException = (HttpException)server.GetLastError();
                   
                    if (httpException.GetHttpCode() == 404)
                    {
                        response.Redirect("/Error/404");
                    }
                    else if (httpException.GetHttpCode() == 500)
                    {
                        
                        ErrorHandler.RedirectToErrorPage(server.GetLastError());
                    }
                }
                else if (server.GetLastError().GetType() == typeof(SessionExpiredException))
                {
                    response.Redirect("/Login");
                }
                else
                {
                    ErrorHandler.RedirectToErrorPage(server.GetLastError());
                }
                server.ClearError(); 
            } catch(Exception){

            }
          
        } 
        
        private static void RedirectToErrorPage(Exception e)
        {
            var user = SessionManager.GetUserContext();
            int errorId = Logger.Log.Error(e, "", "", user!=null? user.UserId : 2);
           // response.Redirect("/Error/" + errorId);

            RouteData routeData = new RouteData();

            routeData.Values.Add("controller", "Error");
            routeData.Values.Add("action", "Index");
            routeData.Values.Add("errorId", errorId);
            ((IController)new ErrorController())
                .Execute(new RequestContext(new HttpContextWrapper(HttpContext.Current), routeData));
        }
    }

   
}