﻿public static class Globals
{
    public static readonly string[] DefaultColors = {
        "#E43A45", //red
        "#4B77BE", //blue
        "#F4D03F", //yellow
        "#2ce095", //green
        "#8877A9", //purple
        "#F2784B", //orange
        "#26C281", //green 2 (darker)
        "#BFBFBF", //grey
        "#f98484", //pink
        "#2AB4C0", //green blue
        "#C5B96B", //gold
        "#BF55EC", //purple rock
        "#5E738B", //indigo
        "#D91E18", //red-flamingo
        "#E87E04", //orange rock
        "#555555", //grey (dark)
        "#29B6F6", //Light blue
        "#26C6DA", //Cyan
        "#6D4C41", //Brown 
        "#C0CA33",//Lime
        "#C67171",//Salmon
        "#808000"//Olive
    };


}
