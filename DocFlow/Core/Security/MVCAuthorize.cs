﻿using DocFlow.Services;
using System;
using System.Security.Principal;

namespace DocFlow.ViewModels
{
    public class MVCAuthorize : IAuthorize
    {
        private readonly IPrincipal _principal;
        private readonly AuthorizationService _service;
        public MVCAuthorize(IPrincipal principal, AuthorizationService service) { _principal = principal; _service = service; SetUserId(); }
        public IIdentity Identity { get { return _principal.Identity; } }
        public AuthorizationService AuthService { get { return _service; } }
        public int UserId { get; private set; }
        //SuperAdmin Interno
        private bool IsSystemAdmin { get;  set; }
        private void SetUserId()
        {
            if (this.UserId == 0 && this._principal != null && this._principal.Identity != null && !string.IsNullOrEmpty(this._principal.Identity.Name))
            {
                DocFlow.Services.UserService uService = new DocFlow.Services.UserService();
                var userdetails = uService.GetUserDetails(this._principal.Identity.Name);
                this.UserId = userdetails.UserId;
                this.IsSystemAdmin = userdetails.IsSystem;
            }
        }


        // Chamado nas Views
        public bool IsInAction(string actioncode)
        {
            if (!_principal.Identity.IsAuthenticated)
                return false;
            if (this.IsSuperAdmin())
                return true;
            return AuthService.ValidateUserAction(UserId, actioncode);
        }



        //// Chamado nas Views
        //[Obsolete("IsInAction with variable is deprecated. User IsInAction(code) instead.")]
        //public bool IsInAction(string actioncode, string variable)
        //{
        //    if (!_principal.Identity.IsAuthenticated)
        //        return false;
        //    if (this.IsSuperAdmin())
        //        return true;
        //    return AuthService.ValidateUserAction(UserId, actioncode);
        //}

        //// Chamado nas Views
        //public bool IsInAction(string actioncode)
        //{
        //    if (!_principal.Identity.IsAuthenticated)
        //        return false;
        //    if (this.IsSuperAdmin())
        //        return true;
        //    return AuthService.ValidateUserAction(UserId, actioncode);
        //}


        //usado em algumas views
        public bool IsInAnyAction(params string[] actioncodes)
        {
            if (!_principal.Identity.IsAuthenticated)
                return false;
            if (this.IsSuperAdmin())
                return true;
            return AuthService.ValidateUserAction(UserId, actioncodes);
        }

        public bool IsInRole(string role)
        {
            //if (!_principal.Identity.IsAuthenticated)
            //    return false;

            //return AuthService.ValidateUserRole(UserId, role);
            throw new NotImplementedException("IsInRole");

        }

        public bool IsSuperAdmin()
        {
            if (!_principal.Identity.IsAuthenticated)
                return false;
            if (IsSystemAdmin)
                return true;
            return AuthService.IsSuperAdmin(UserId);

        }

        // BF MUDAR ISTO PARA RECEBER A ACAO
        //public List<int> GetVariableIdsForActionGroup(string group)
        //{
        //    if (!_principal.Identity.IsAuthenticated)
        //        return new List<int> { 0 };
        //    return AuthService.GetVariableIdsForActionGroup(group);
        //}

    }
}