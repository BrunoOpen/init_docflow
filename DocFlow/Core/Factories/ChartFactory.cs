﻿using DocFlow.Core.Objects;
using DocFlow.Data;

using System;
using System.Collections.Generic;
using System.Linq;

namespace DocFlow.Core.Factories
{
    public static class ChartFactory
    {
        public static Chart CreateStackedChart(List<ChartValue> values, string ColumnTitle, string LineTitle)
        {
            Queue<string> DefaultColors = new Queue<string>(Globals.DefaultColors);

            Chart chart = new Chart()
            {
                Title = string.Empty,
                Label = string.Empty,
                Graphs = new List<Graph>(),
                DataProviders = new List<DataProvider>(),
                Axis = new List<Axis>()
            };

            foreach (ChartValue data in values)
            {
                DataProvider provider = new DataProvider();
                provider.date = data.MonthDate.ToString("dd/MM/yyyy");
                provider.Bars = new List<Bar>();
                provider.Bars.Add(new Bar("total", data.ColumnValue.ToString()));
                provider.Bars.Add(new Bar("media", data.AverageValue.ToString()));
                provider.Bars.Add(new Bar("locals", data.LineValue.ToString()));
                chart.DataProviders.Add(provider);
            }

            // Barras
            string color = DefaultColors.Dequeue();
            Graph bars = new Graph()
            {
                valueAxis = "v1",
                balloonText = "[[total]]",
                fillAlphas = "0.8",
                id = "total",
                type = "column",
                lineColor = color,// : color;// "#E60000";string.IsNullOrEmpty(color) ? 
                fillColor = color,//
                valueField = "total",
                title = ColumnTitle
            };

            chart.Graphs.Add(bars);

            // Media
            Graph average = new Graph()
            {
                valueAxis = "v1",
                balloonText = "[[media]]",
                fillAlphas = "0",
                id = "media",
                type = "line",
                lineColor = "#1BA39C",// : color;// "#E60000";string.IsNullOrEmpty(color) ? 
                fillColor = "#1BA39C",//
                valueField = "media",
                title = Globalization.Language.MEAN
            };

            chart.Graphs.Add(average);

            // Linha
            Graph line = new Graph()
            {
                valueAxis = "v2",
                balloonText = "[[locals]]",
                fillAlphas = "0",
                id = "locals",
                type = "line",
                valueField = "locals",
                lineColor = "#543CD9",// : color;// "#E60000";string.IsNullOrEmpty(color) ? 
                fillColor = "#543CD9",//
                                      // valueField = "total",
                title = LineTitle
            };

            chart.Graphs.Add(line);

            // Eixo Esquerdo
            chart.Axis.Add(new Axis()
            {
                axisColor = "#000",
                position = "left",
                title = ColumnTitle,
                id = "v1",
                axisAlpha = "1"
            });

            // Eixo Direito
            chart.Axis.Add(new Axis()
            {
                axisColor = "#543CD9",
                position = "right",
                title = LineTitle,
                id = "v2",
                axisAlpha = "1"
            });

            return chart;
        }

        public static Chart CreateStackedDetailedChart(List<ChartValueDetails> values)
        {
            Queue<string> DefaultColors = new Queue<string>(Globals.DefaultColors);

            Chart chart = new Chart()
            {
                Title = string.Empty,
                Label = string.Empty,
                Graphs = new List<Graph>(),
                DataProviders = new List<DataProvider>(),
                Axis = new List<Axis>()
            };

            foreach (DateTime data in values.GroupBy(u=>u.MonthDate).Select(u=>u.Key).ToList())
            {
                DataProvider provider = new DataProvider();
                provider.date = data.ToString("dd/MM/yyyy");
                provider.Bars = new List<Bar>();
                var totals = values.Where(a => a.MonthDate == data).Select(a => new { Id=a.DetailName.Replace(" ","_"), DetailName = a.DetailName, Total = a.ColumnValue }).ToList();
                foreach (var total in totals)
                {
                    provider.Bars.Add(new Bar(total.Id, total.Total.ToNumberFormat()));

                }
                chart.DataProviders.Add(provider);
            }


            // Eixo Esquerdo
            chart.Axis.Add(new Axis()
            {
                axisColor = "#000",
                position = "left",
                title = "Total",
                id = "v1",
                axisAlpha = "1"
            });


            foreach (var categories in values.GroupBy(u=> new { u.DetailName, u.Color }).Select(f=> new { Id = f.Key.DetailName.Replace(" ", "_"), DetailName = f.Key.DetailName, Color=f.Key.Color }))
            {
                // string color = string.IsNullOrEmpty(categories.Color) ? DefaultColors.Dequeue() : categories.Color;
                string color = DefaultColors.Dequeue();
                chart.Graphs.Add(new Graph() {

                    balloonText = "[[" + categories.Id + "]]",
                valueAxis = "v1",
                fillAlphas = "0.8",
                lineColor = color,
                fillColor = color,
                id = categories.Id,
                type = "column",
                valueField = categories.Id,
                title = categories.DetailName,
                showBalloon = false,


            });
            }

            return chart;
        }
    }
}

















