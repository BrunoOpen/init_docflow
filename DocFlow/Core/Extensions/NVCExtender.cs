﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace DocFlow.Core.Extensions
{
    public static class NVCExtender
    {
        public static IDictionary<string, string[]> ToDictionary(
                                    this NameValueCollection source)
        {
            return source.AllKeys.ToDictionary(k => k, k => source.GetValues(k));
        }
    }
}