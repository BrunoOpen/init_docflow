﻿using DocFlow.Core.Error;
using DocFlow.Core.Objects;
using DocFlow.ViewModels;

using System;
using System.Web;

namespace DocFlow.Core.Extensions
{
    public static class SessionManager
    {
        // private const string MENU = "NAV_BAR_MAIN_MENU";

        private const string VARIABLE = "CURRENT_ACTIVE_SERVICE_VARIABLE";
        private const string CULTURE = "CURRENT_ACTIVE_CULTURE";
        private const string INSTANCE = "CURRENT_ACTIVE_INSTANCE";
        private const string USER = "CURRENT_ACTIVE_USER_DETAILS";
        private const string LOG = "CURRENT_LOG_ACTIVITY";

        private const string MAIN_COLOR = "red-flamingo";
        private const string WATER_COLOR = "blue";

        public static Enums.Services GetActiveService()
        {
            Enums.Services? service = HttpContext.Current.Session[VARIABLE] as Enums.Services?;
            if (service != null)
                return service.Value;
            else
                return Enums.Services.Energy;
        }

        public static int GetActiveServiceId()
        {
            Enums.Services? service = HttpContext.Current.Session[VARIABLE] as Enums.Services?;
            if (service != null)
                return (int)service.Value;
            else
                return (int)Enums.Services.Energy;
        }

        public static string GetActiveServiceClass()
        {
            Enums.Services? service = HttpContext.Current.Session[VARIABLE] as Enums.Services?;
            if (service != null && service == Enums.Services.Energy)
                return MAIN_COLOR;
            if (service != null && service == Enums.Services.Water)
                return WATER_COLOR;
            else
                return MAIN_COLOR;
        }
      

        public static string GetActiveContext()
        {
            Enums.Services? service = HttpContext.Current.Session[VARIABLE] as Enums.Services?;
            if (service != null && service == Enums.Services.Energy)
                return "Energy";
            if (service != null && service == Enums.Services.Water)
                return "Water";
            else
                return "Energy";
        }

        public static void SetActiveService(Enums.Services service)
        {
            HttpContext.Current.Session[VARIABLE] = service;
        }

        public static Language GetActiveCulture()
        {
            Language lang = HttpContext.Current.Session[CULTURE] as Language;
            if (lang != null)
                return lang;
            else
                return null;
        }

        public static void SetActiveCulture(Language lang)
        {
            HttpContext.Current.Session[CULTURE] = lang;
        }


        public static InstanceRecord GetActiveInstance()
        {
            InstanceRecord instance = HttpContext.Current.Session[INSTANCE] as InstanceRecord;
            if (instance != null)
                return instance;
            else
                return null;
        }

        public static void SetActiveInstance(InstanceRecord instance)
        {
            HttpContext.Current.Session[INSTANCE] = instance;
        }


        public static void SetUserContext(UserDetails userdetails)
        {
            HttpContext.Current.Session[USER] = userdetails;
        }

        public static UserDetails GetUserContext()
        {
            UserDetails details = null;
            try
            {
                details = HttpContext.Current.Session != null ? HttpContext.Current.Session[USER] as UserDetails : null;
                if (details != null)
                    return details;

                if (HttpContext.Current != null && HttpContext.Current.User != null && HttpContext.Current.User.Identity != null && !string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name))
                {
                    Services.UserService uService = new Services.UserService();
                    details = uService.GetUserDetails(HttpContext.Current.User.Identity.Name);
                 
                    SessionManager.SetUserContext(details);
                    return details;
                }
                throw new SessionExpiredException();
            }
            catch (Exception e)
            {
               
                Logger.Log.Error(e, "SessionManager", System.Reflection.MethodBase.GetCurrentMethod().Name, 2); //userid=2 para nao ocorrer cascata de erros 
            }
            return details;


        }

        public static bool CheckUserContext()
        {
               return HttpContext.Current.Session != null && HttpContext.Current.Session[USER]!=null;
        }

        public static int? GetActiveLogActivity()
        {
            return HttpContext.Current.Session != null ? HttpContext.Current.Session[LOG] as int? : null;
        }

        public static void SetActiveLogActivity(int logId)
        {
            HttpContext.Current.Session[LOG] = logId;
        }

        public static void FlushCache()
        {
            // HttpContext.Current.Session.Remove(MENU);
            HttpContext.Current.Session.Remove(VARIABLE);
            HttpContext.Current.Session.Remove(CULTURE);
            HttpContext.Current.Session.Remove(USER);
        }

    }

}