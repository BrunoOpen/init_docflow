﻿using DocFlow.Core.Objects;
using DocFlow.ViewModels;
using System.Collections.Generic;
using System.Web;


namespace DocFlow.Core.Extensions
{
    public static class AuthorizationCache
    {
        private const string SERVICES = "CACHE_MENU_SERVICES";
        private const string LANGS = "CACHE_MENU_LANGUAGES";
        private const string MENU = "CACHE_USER_MENU";
        private const string ACTIONS = "CACHE_USER_ACTIONS";
        private const string ISADMIN = "CACHE_USER_ISADMIN";
        public static bool? ForceGetLanguage { get; set; }
        #region Languages
        public static List<DocFlow.ViewModels.Language> GetLanguages()
        {
            List<DocFlow.ViewModels.Language> languages = HttpContext.Current.Session[LANGS] as List<DocFlow.ViewModels.Language>;
            if (languages != null && (AuthorizationCache.ForceGetLanguage.HasValue && !AuthorizationCache.ForceGetLanguage.Value))
                return languages;
            else
                return null;
        }
        public static void CacheLanguages(List<DocFlow.ViewModels.Language> languages)
        {
            HttpContext.Current.Session[LANGS] = languages;
        }
        #endregion

        #region Services
        public static List<DocFlow.ViewModels.Service> GetServices()
        {
            List<DocFlow.ViewModels.Service> services = HttpContext.Current.Session[SERVICES] as List<DocFlow.ViewModels.Service>;
            if (services != null)
                return services;
            else
                return null;
        }
        public static List<DocFlow.ViewModels.Service> CacheServices(List<DocFlow.ViewModels.Service> services)
        {
            HttpContext.Current.Session[SERVICES] = services;
            return services;
        }
        #endregion

        #region Menu
        public static MenuViewModel GetMenu()
        {
            MenuViewModel menu = HttpContext.Current.Session[MENU] as MenuViewModel;
            if (menu != null)
                return menu;
            else
                return null;
        }
        public static void CacheMenu(MenuViewModel menu)
        {
            HttpContext.Current.Session[MENU] = menu;
        }
        #endregion

        #region Actions
        public static List<AccountAction> GetActions()
        {
            List<AccountAction> actions = HttpContext.Current.Session[ACTIONS] as List<AccountAction>;
            if (actions != null)
                return actions;
            else
                return null;
        }
        public static void CacheActions(List<AccountAction> actions)
        {
            HttpContext.Current.Session[ACTIONS] = actions;
        }
        #endregion

        #region IsAdmin
        public static bool? GetIsAdmin()
        {
            bool? isAdmin = HttpContext.Current.Session[ISADMIN] as bool?;
            if (isAdmin != null)
                return isAdmin.Value;
            else
                return null;
        }
        public static void CacheIsAdmin(bool isAdmin)
        {
            HttpContext.Current.Session[ISADMIN] = isAdmin;
        }
        #endregion

        public static void FlushCache() {
            HttpContext.Current.Session.Remove(MENU);
            HttpContext.Current.Session.Remove(ACTIONS);
            HttpContext.Current.Session.Remove(ISADMIN);
        }
    }
}