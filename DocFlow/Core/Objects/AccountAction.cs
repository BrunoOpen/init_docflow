﻿namespace DocFlow.Core.Objects
{
    public class AccountAction
    {
        public bool ServiceDependent { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public bool Energy { get; set; }
        public bool Water { get; set; }
        public bool Both { get; set; }
        

        public bool CheckPermission(string code, Enums.Services service)
        {
            //Check ACTION_CODE
            if(this.Code.Trim().ToLower()==code.Trim().ToLower())
            {
                    //SE NAO DEPENDE DO SERVICO, TENHO-A.
                    if (service == Enums.Services.Energy)
                            return !this.ServiceDependent || this.Energy || this.Both;
                    else
                        return !this.ServiceDependent || this.Water || this.Both;
            
            }
            return false;

        }
    }
}
