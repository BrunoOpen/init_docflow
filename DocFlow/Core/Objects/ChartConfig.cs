﻿using System.Collections.Generic;
using System.Linq;

namespace DocFlow.Core.Objects
{
    public class ChartConfig
    {
        public ChartType Type { get; set; }
        public bool HasAverage { get; set; }

        //id,  id
        public List<ChartButtonConfig> Buttons { get; set; }

        public ChartButtonConfig Default { get { return this.Buttons != null ? (this.Buttons.Any(f => f.Selected) ? this.Buttons.First(u => u.Selected) : this.Buttons.First()) : null; } }

        /// <summary>
        /// Only avaiable when Type Is Detailed
        /// Options Avaiable on Chart DropDownList
        /// </summary>
        public List<int> Detailypes { get; set; }

        public string Title { get; set; }
    }

    public class ChartButtonConfig
    {
        public int ValueTypeId { get; set; }
        public string DisplayText { get; set; }
        /// <summary>
        /// Only Avaiable When Type Is Basic
        /// </summary>
        public int? LineTypeId { get; set; }

        public bool Selected { get; set; }
    }

    public class ChartButton : ChartButtonConfig
    {
        public string Link { get; set; }
        public bool Active { get; set; }

        public ChartButton(ChartButtonConfig cfg)
        {
            this.DisplayText = cfg.DisplayText;
            this.LineTypeId = cfg.LineTypeId;
            this.ValueTypeId = cfg.ValueTypeId;
        }

        public ChartButton() { }
    }

}
