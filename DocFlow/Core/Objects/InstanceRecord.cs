﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DocFlow.Core.Objects
{
    public class InstanceRecord
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string CnnString { get; set; }
        public string DataBase { get; set; }
        public bool? IsDefault { get; set; }
    }
}

