﻿namespace DocFlow.Enums
{
    public enum Services : int
    {
        Energy = 1,
        Water = 2
    }

    public enum MeteringContext : int { Analysis = 1, Reading = 2, Contracts = 3, Meters = 4, Documents = 5, Billing = 6, Locals = 7, Files = 8 }

    public enum RecordState : int
    {
        UnChanged = 0,
        Inserted = 1,
        Updated = 2,
        Deleted = 3
    }

    public enum ActionType : int
    {
        /// <summary>
        /// Novo
        /// </summary>
        New = 1,
        /// <summary>
        /// Editar
        /// </summary>
        Edit = 2,
        /// <summary>
        /// Renovar
        /// </summary>
        Renew = 3,
        /// <summary>
        /// Terminar
        /// </summary>
        Finish = 4,
        /// <summary>
        /// Mudar
        /// </summary>
        Change = 5,
        /// <summary>
        /// Alterar
        /// </summary>
        Alter = 6,
        /// <summary>
        /// Anular
        /// </summary>
        Cancel = 7
    };

    public enum RuleCategory : int { Contracts = 3, Locals = 7, Analysis = 1, Readings = 2 };

    public enum EndUserDataType : int { Integer = 1, Decimal = 2, String = 3, Datetime = 4, Boolean = 5 };

    public enum AlertDataType : int { Files = 1, Documents = 2, Locals = 3, Meters = 4, Contracts = 5, Readings = 6 };

    public enum AlertType : int { Active = 1, Pending = 2, Validated = 3 };

    public enum ExportFormat : int { PDF = 1, XLSX = 2, FILESYSTEM_PDF = 3, FILESYSTEM_XLSX = 4 };

    public enum ExcelTemplateContext
    {
        Document
    }
}