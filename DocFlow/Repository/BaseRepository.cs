﻿using DocFlow.Core.Extensions;
using DocFlow.DAL;
using DocFlow.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;

namespace DocFlow.Repository
{
    public abstract class BaseRepository
    {
      


        private int _serviceId;
        public int ServiceId
        {
            get
            {
                if (this._serviceId == 0)
                    this._serviceId = SessionManager.GetActiveServiceId();
                return this._serviceId;
            }
            set
            {
                this._serviceId = value;
            }
        }

        private int _languageId;
        public int LanguageId
        {
            get
            {
                if (this._languageId == 0)
                    this._languageId = SessionManager.GetActiveCulture().Id;
                return this._languageId;
            }
            set
            {
                this._languageId = value;
            }
        }

        private int _userId;
        public int UserId
        {
            get
            {
                if (this._userId == 0)
                    this._userId = SessionManager.GetUserContext()!= null? SessionManager.GetUserContext().UserId : 2;
                return this._userId;
            }
            set
            {
                this._userId = value;
            }
        }

        protected int LogError(Exception e, string currentClass, string method)
        {
          return  Logger.Log.Error(e, currentClass, method,UserId);
        }




        public DataTable GetDataTable(string spName, SqlParameter[] sqlParams)
        {
            DataTable dt = new DataTable();
            try
            {
                LogSpExec(spName, sqlParams);
                using (ApplicationDBContext cont = new ApplicationDBContext())
                {
                    using (var conn = cont.Database.Connection)
                    {
                        var command = conn.CreateCommand();
                        command.CommandTimeout = 0;
                        command.CommandText = spName;
                        command.CommandType = CommandType.StoredProcedure;
                        if (sqlParams != null)
                        {
                            foreach (var param in sqlParams)
                            {
                                command.Parameters.Add(param);
                            }
                        }
                        conn.Open();
                        var output = command.ExecuteReader();
                        if (output.HasRows)
                        {
                            dt.Load(output);
                            //if (dt.Rows.Count == 0)
                            //    return null;
                            //return dt;
                        }
                        output.Close();
                        conn.Close();

                    }
                }
            }
            catch (Exception e)
            {
                LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
            return dt;

        }

        public T GetRecord<T>(string spName, SqlParameter[] sqlParams) where T : new()
        {
            T dataset = new T();
            try
            {
                LogSpExec(spName, sqlParams);
                using (ApplicationDBContext ctx = new ApplicationDBContext())
                {
                    string spExec = CreateSpExec(spName, sqlParams);
                    dataset = ctx.Database.SqlQuery<T>(spExec, sqlParams).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
               LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
                throw new Exception(Globalization.Language.ERROR_MESSAGE_REPOSITORY, e);
            }
            return dataset;
        }

        public List<T> GetRecords<T>(string spName, SqlParameter[] sqlParams) where T : new()
        {
            List<T> dataset = new List<T>();
            try
            {
                LogSpExec(spName, sqlParams);
                using (ApplicationDBContext ctx = new ApplicationDBContext())
                {
                    string spExec = CreateSpExec(spName, sqlParams);
                    ctx.Database.CommandTimeout = 0;
                    dataset = ctx.Database.SqlQuery<T>(spExec, sqlParams.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
              LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
                throw new Exception(Globalization.Language.ERROR_MESSAGE_REPOSITORY, e);
            }
            return dataset;
        }

        public DataTable GetTable(string spName, SqlParameter[] sqlParams)
        {
            DataTable table = new DataTable();
            try
            {
                LogSpExec(spName, sqlParams);
                using (ApplicationDBContext ctx = new ApplicationDBContext())
                {
                    string spExec = CreateSpExec(spName, sqlParams);
                    ctx.Database.CommandTimeout = 0;
                    DbDataAdapter da = DbProviderFactories.GetFactory(ctx.Database.Connection).CreateDataAdapter();
                    DbCommand cmd = DbProviderFactories.GetFactory(ctx.Database.Connection).CreateCommand();
                    cmd.CommandText = spExec;
                    cmd.Parameters.AddRange(sqlParams);
                    cmd.Connection = ctx.Database.Connection;
                    da.SelectCommand = cmd;
                    da.Fill(table);
                }
            }
            catch (Exception e)
            {
                LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
                throw new Exception(Globalization.Language.ERROR_MESSAGE_REPOSITORY, e);
            }
            return table;
        }

        private string CreateSpExec(string spName, params SqlParameter[] sqlParams)
        {
            string spExec = "EXEC " + spName;
            foreach (SqlParameter sqlParam in sqlParams)
            {
                spExec = spExec + " " + sqlParam.ParameterName + " = " + sqlParam.ParameterName + ", ";
            }
            return spExec.Trim().Trim(',');
        }

        private void LogSpExec(string spName, params SqlParameter[] sqlParams)
        {
            try
            {
                string spExec = "EXEC " + spName;
                if (sqlParams != null)
                {
                    foreach (SqlParameter sqlParam in sqlParams)
                    {
                        spExec = string.Format("{0} {1}={2}, ", spExec, sqlParam.ParameterName, (sqlParam.Value == null || sqlParam.Value == DBNull.Value ? " null " : sqlParam.Value.ToString()));
                    }
                }
                Logger.Log.Debug("EXEC", (int?)null, spExec.Trim().Trim(','), UserId);

            }
            catch (Exception e)
            {
                LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
        }

        
    }
}