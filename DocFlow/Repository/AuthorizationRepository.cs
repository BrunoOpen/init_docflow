﻿using DocFlow.Core.Extensions;
using DocFlow.Core.Objects;
using DocFlow.DAL;
using DocFlow.Helpers;
using DocFlow.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace DocFlow.Repository
{

    public class AuthorizationRepository : BaseRepository
    {

        public List<AccountAction> GetUserActions(int userid)
        {
            List<AccountAction> list = new List<AccountAction>();
            try
            {

                SqlParameter[] sqlparams = {
                                        SqlHelper.BuildInt("@UserId",userid)
                    };

                list = this.GetRecords<AccountAction>("GetUserActions", sqlparams);

            }
            catch (Exception e)
            {
              LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
            return list;
        }

        /// <summary>
        /// Returns menu according to user permissions
        /// </summary>
        public MenuViewModel GetMenuByUser()
        {
            MenuViewModel menu = new MenuViewModel();

            try
            {
                int userid =SessionManager.GetUserContext().UserId;
                SqlParameter[] sqlparams = {
                                        SqlHelper.BuildInt("@UserId",userid),
                                        SqlHelper.BuildInt("@ServiceId",this.ServiceId),
                                         SqlHelper.BuildInt("@LanguageId",this.LanguageId)
                    };

                menu.Menus = this.GetRecords<MenuItem>("GetUserMenu", sqlparams);
                menu.Variable = SessionManager.GetActiveService();


            }
            catch (Exception e)
            {
                LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
            return menu;
        }

        //public MenuViewModel GetMenuByUser(int userid, int variablevalueId)
        //{
        //    MenuViewModel menumodel = new MenuViewModel();
        //    try
        //    {
        //        using (ApplicationDBContext ctx = new ApplicationDBContext())
        //        {

        //            dynamic obj = new System.Dynamic.ExpandoObject();

        //            var menus = (from menu in ctx.Menus

        //                         join menua in ctx.MenuActions on menu.Id equals menua.MenuID
        //                         join actions in ctx.Roles on menua.AppActionId equals actions.Id 
        //                         join acgroup in ctx.AppActionGroupActions on actions.Id equals acgroup.AppActionId
        //                         join groups in ctx.AppActionGroups on acgroup.AppActionGroupId equals groups.Id
        //                         join rolegroups in ctx.AppRoleActionGroups on groups.Id equals rolegroups.AppActionGroupId
        //                         join roles in ctx.AppRoles on rolegroups.AppRoleId equals roles.Id
        //                         join acroles in ctx.AppAccountRoles on roles.Id equals acroles.AppRoleId
        //                         join contexts in ctx.AppContexts on actions.Id equals contexts.Action.Id
        //                         where acroles.AppAccountId == userid && menu.VariableValueID == variablevalueId && menu.VariableValueID == contexts.VariableValue.Id
        //                         select new MenuItem() { Id = menu.Id, ParentId = menu.ParentID }).ToList();

        //            menumodel.Menus = (from menu in ctx.Menus
        //                               from menu_i18n in ctx.Menus_I18N.Where(a => a.MenuID == menu.Id && a.LanguageID == menu.Id).DefaultIfEmpty()
        //                               select new MenuItem()
        //                               {
        //                                   CssClass = menu.Icon,
        //                                   Id = menu.Id,
        //                                   ParentId = menu.ParentID,
        //                                   Text = !string.IsNullOrEmpty(menu_i18n.NameT) ? menu_i18n.NameT : menu.Name,
        //                                   Url = menu.Href,
        //                                   Order = menu.Order

        //                               }).ToList().Where(m => menus.Any(a => a.Id == m.Id || (a.ParentId != null && a.ParentId == m.Id))).OrderBy(f => f.Order).ToList();

        //        }
        //        menumodel.Variable = (Enums.Services)variablevalueId;
        //    }
        //    catch (Exception e)
        //    {
        //        this.ErrorService.LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
        //        /// TODO: Handle Exception
        //    }

        //    return menumodel;
        //}

        //public MenuViewModel GetMenu(int variablevalueId)
        //{
        //    MenuViewModel menumodel = new MenuViewModel();
        //    int langId = SessionManager.GetActiveCulture().Id;
        //    try
        //    {
        //        using (ApplicationDBContext ctx = new ApplicationDBContext())
        //        {
        //            menumodel.Menus = (from menu in ctx.Menus
        //                               from menu_i18n in ctx.Menus_I18N.Where(a => a.LanguageID == langId && a.MenuID == menu.Id).DefaultIfEmpty()
        //                               //   join menua in ctx.MenuActions on menu.Id equals menua.MenuID
        //                               where menu.VariableValueID == variablevalueId
        //                               select new MenuItem()
        //                               {
        //                                   CssClass = menu.Icon,
        //                                   Id = menu.Id,
        //                                   ParentId = menu.ParentID,
        //                                   Text = !string.IsNullOrEmpty(menu_i18n.NameT)?menu_i18n.NameT:menu.Name,
        //                                   Url = menu.Href,
        //                                   Order = menu.Order

        //                               }).OrderBy(f => f.Order).ToList();
        //            menumodel.Variable = (Enums.Services)variablevalueId;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        this.ErrorService.LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
        //        /// TODO: Handle Exception
        //    }

        //    return menumodel;
        //}


        //public bool ValidateUserRole(int userid, string role)
        //{
        //    try
        //    {
        //        using (ApplicationDBContext ctx = new ApplicationDBContext())
        //        {
        //            return (from roles in ctx.AppAccountRoles
        //                    where roles.AppAccountId == userid && roles.AppRole.Name == role
        //                    select roles.AppRoleId).Any();
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        this.ErrorService.LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
        //        /// TODO: Handle Exception
        //    }

        //    return false;
        //}

        public bool ValidateUserSystemAdmin(int userid)
        {
            try
            {
                using (ApplicationDBContext ctx = new ApplicationDBContext())
                {
                    return (from u in ctx.Users
                            where u.IsSystem == true && u.Id == userid
                            select u.Id).Any();
                }
            }
            catch (Exception e)
            {
              LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }

            return false;
        }

        //public List<int> GetVariableIdsForActionGroup(string groupcode)
        //{
        //    List<int> returnObj = new List<int>();
        //    try
        //    {
        //        using (ApplicationDBContext ctx = new ApplicationDBContext())
        //        {
        //            var g = ctx.AppActionGroups.FirstOrDefault(a => a.Code == groupcode);

        //            returnObj = (from groups in ctx.AppActionGroupActions
        //                         join act in ctx.Roles on groups.AppActionId equals act.Id
        //                         join avar in ctx.AppActionVariables on act.Id equals avar.AppActionId
        //                         join values in ctx.VariableValues on avar.VariableID equals values.VariableID
        //                         where groups.AppActionGroupId == g.Id
        //                         select values.Value).Distinct().ToList();
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        this.ErrorService.LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
        //        /// TODO: Handle Exception
        //    }
        //    return returnObj;

        //}
    }
}