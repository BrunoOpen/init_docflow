﻿using DocFlow.Repository;
using DocFlow.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace DocFlow.Services
{
    public class RolesService
    {
        private RolesRepository _rolesRep;
        private RolesRepository RolesRepository
        {
            get
            {
                if (this._rolesRep == null)
                    this._rolesRep = new RolesRepository();
                return this._rolesRep;
            }
        }
        private UserService _userService;
        private UserService UserService
        {
            get
            {
                if (this._userService == null)
                    this._userService = new UserService();
                return this._userService;
            }
        }

        public List<RolesSearchRecord> Search()
        {
            return RolesRepository.SearchRoles();
        }

        public UserProfileEditModel GetRoleDetails(int roleId, bool allActions = false)
        {
            UserProfileEditModel model = RolesRepository.GetRoleById(roleId);
            if (!allActions)
                model.ActionsGroups = this.GetRoleActionsGroups(roleId);
            else
            {
                model.ActionsGroups = RolesRepository.GetAllActionsGroups();
                var selecteds = this.GetRoleActionsGroups(roleId);
                foreach (var item in model.ActionsGroups)
                {
                    if (selecteds.Any(f => f.Id == item.Id))
                        item.Permission = true;
                }
            }
            return model;
        }

        public void DeleteRole(int roleId)
        {
            RolesRepository.DeleteRole(roleId);
        }

        public void SaveRole(UserProfileEditModel model)
        {
            if (model.Id != 0)
                RolesRepository.UpdateRole(model);
            else
                RolesRepository.InsertRole(model);
        }

        public List<ActionGroup> GetRoleActionsGroups(int roleId)
        {
            return RolesRepository.GetRoleActionsGroups(roleId);
        }

        public List<ActionGroup> GetAllActionsGroups()
        {
            return RolesRepository.GetAllActionsGroups();
        }

        public List<DisplayRole> GetAllUserRoles()
        {
            return RolesRepository.GetAllUserRoles();
        }

       

        public List<string> GetAllUserActions(int userId)
        {
            return RolesRepository.GetAllUserActions(userId);
        }


        public List<UserActionGroup> GetGroupsForCreation()
        {
            return RolesRepository.GetGroupsForCreation();
        }
        public List<UserActionGroup> GetGroupsForEdit(int userId)
        {
            return RolesRepository.GetGroupsForEdit(userId);
        }
        public List<UserActionGroup> GetGroupsForDisplay(int userId)
        {
            return RolesRepository.GetGroupsForDisplay(userId);
        }
        
        
    }
}