﻿using DocFlow.Core.Extensions;
using DocFlow.Core.Objects;
using DocFlow.Enums;
using DocFlow.Repository;
using DocFlow.ViewModels;
using DocFlow.ViewModels.Dashboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace DocFlow.Services
{
    public class SharedService
    {
        private SharedRepository _sharedRep;
        private SharedRepository Repository
        {
            get
            {
                return this._sharedRep ?? new SharedRepository();
            }
            set
            {
                this._sharedRep = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>All configurated languages</returns>
        public List<DocFlow.ViewModels.Language> GetAppLanguages()
        {
            List<DocFlow.ViewModels.Language> languages = AuthorizationCache.GetLanguages();

            if (languages != null)
                return languages;

            languages = Repository.GetAppLanguages();

            AuthorizationCache.CacheLanguages(languages);

            return languages;
        }
        
        public List<InstanceRecord> GetInstances()
        {
          return   Repository.GetInstances(SessionManager.GetUserContext().UserId,1);
        }
        public List<TemplateModel> GetAppTemplates()
        {
            List<TemplateModel> list = new List<TemplateModel>();

            //if (SessionManager.GetActiveService() == Enums.Services.Energy)
            //    list.Add(new TemplateModel(1, Globalization.Language.TEMPLATE_FILES_UPLOAD, "TemplateEnergia.xlsx"));
            //else
            //{
            //    if (SessionManager.GetActiveService() == Enums.Services.Water)
            //        list.Add(new TemplateModel(2, Globalization.Language.TEMPLATE_FILES_UPLOAD, "TemplateAgua.xlsx"));
            //}

            //list.Add(new TemplateModel(3, Globalization.Language.TEMPLATE_LOCALGROUPS_UPLOAD, "TemplateGrupos.xlsx"));

            //list.Add(new TemplateModel(4, Globalization.Language.TEMPLATE_READINGS_UPLOAD, "TemplateLeituras.xlsx"));

            return list;

        }
        public void UpdateLanguage(int languageId, bool active)
        {
            Repository.UpdateLanguage(languageId, active);
        }

        public Language GetDefaultLanguage()
        {
            return Repository.GetDefaultLanguage();
        }

        public Language GetLanguageById(int langId)
        {
            return Repository.GetLanguage(langId);
        }

      
        /// <summary>
        /// 
        /// </summary>
        /// <returns>All languages in DB</returns>
        public List<DocFlow.ViewModels.Language> GetAllLanguages()
        {
            return Repository.GetAllLanguages();
        }

        public List<DocFlow.ViewModels.Service> GetServices()
        {
            List<DocFlow.ViewModels.Service> services = AuthorizationCache.GetServices();
            if (services != null)
                return services;
            else
                return AuthorizationCache.CacheServices(Repository.GetServices());
        }

        public List<SelectListItem> GetServicesItems()
        {
            return this.Repository.GetServicesItems();
        }

      

        #region Translations
        public List<ConfigTranslation> GetServiceTranslations(int languageId)
        {
            return Repository.GetServiceTranslations(languageId);
        }
        public void TranslateService(ConfigTranslation translation)
        {
            Repository.TranslateService(translation);
        }

        public List<ConfigTranslation> GetMenuTranslations(int languageId)
        {
            return Repository.GetMenuTranslations(languageId);
        }
        public void TranslateMenu(ConfigTranslation translation)
        {
            Repository.TranslateMenu(translation);
        }
        
        public List<ConfigTranslation> GetRolesTranslations(int languageId)
        {
            return Repository.GetRolesTranslations(languageId);
        }
        public void TranslateRole(ConfigTranslation translation)
        {
            Repository.TranslateRole(translation);
        }

        public List<ConfigTranslation> GetActionGroupTranslations(int languageId)
        {
            return Repository.GetActionGroupTranslations(languageId);
        }
        public void TranslateActionGroup(ConfigTranslation translation)
        {
            Repository.TranslateActionGroup(translation);
        }
        #endregion


        public DropDownListEditModel GetSelectTypeData(int? type)
        {
            DropDownListEditModel dropdown = null;
            int t = type.HasValue ? type.Value : 0;
            int serviceId = (int)SessionManager.GetActiveService();
            if (serviceId == (int)Enums.Services.Water)
            {
                dropdown = new DropDownListEditModel
                {
                    Values = new List<SelectListItem> 
                        { 
                            new SelectListItem 
                            { 
                                Text = Globalization.Language.DASHBOARD_BY_INSTALLATION_TYPE, 
                                Value = "0" 
                            },
                            new SelectListItem 
                            { 
                                Text = Globalization.Language.CLASSIFICATIONS, 
                                Value = "1" 
                            },
                            new SelectListItem 
                            { 
                                Text = Globalization.Language.DASHBOARD_ZONES, 
                                Value = "2" 
                            },
                            new SelectListItem 
                            {
                                Text=Globalization.Language.PROVIDER,
                                Value="5"
                            }
                        }
                };
            }
            else
            {
                switch (t)
                {
                    // consumo
                    case 1:
                        dropdown = new DropDownListEditModel
                        {
                            Values = new List<SelectListItem> 
                        { 
                            new SelectListItem 
                            { 
                                Text = Globalization.Language.DASHBOARD_BY_INSTALLATION_TYPE, 
                                Value = "0" 
                            },
                            new SelectListItem 
                            { 
                                Text = Globalization.Language.DASHBOARD_ZONES, 
                                Value = "2" 
                            },
                            new SelectListItem 
                            { 
                                Text = Globalization.Language.CONNECTIONTYPES, 
                                Value = "3" 
                            } ,
                            new SelectListItem 
                            {
                                Text=Globalization.Language.PROVIDER,
                                Value="5"
                            }
                        }
                        };
                        break;
                    // Produção
                    case 3:
                        dropdown = new DropDownListEditModel
                        {
                            Values = new List<SelectListItem> 
                        { 
                            new SelectListItem 
                            { 
                                Text = Globalization.Language.DASHBOARD_BY_INSTALLATION_TYPE, 
                                Value = "0" 
                            },
                            new SelectListItem 
                            { 
                                Text = Globalization.Language.PRODUCTIONTYPES, 
                                Value = "4" 
                            },
                            new SelectListItem 
                            { 
                                Text = Globalization.Language.CONNECTIONTYPES, 
                                Value = "3" 
                            } ,
                        }
                        };
                        break;
                    case 2:
                    default:
                        dropdown = new DropDownListEditModel
                        {
                            Values = new List<SelectListItem> 
                        { 
                            new SelectListItem 
                            { 
                                Text = Globalization.Language.DASHBOARD_BY_INSTALLATION_TYPE, 
                                Value = "0" 
                            },
                            new SelectListItem 
                            { 
                                Text = Globalization.Language.CLASSIFICATIONS, 
                                Value = "1" 
                            },
                            new SelectListItem 
                            { 
                                Text = Globalization.Language.DASHBOARD_ZONES, 
                                Value = "2" 
                            },
                            new SelectListItem 
                            { 
                                Text = Globalization.Language.CONNECTIONTYPES, 
                                Value = "3" 
                            },
                            new SelectListItem 
                            {
                                Text=Globalization.Language.PROVIDER,
                                Value="5"
                            }
                        }
                        };
                        break;
                }
            }

            return dropdown;
        }
        public List<ConfigTranslation> GetActionsTranslations(int langId)
        {
            return this.Repository.GetActionsTranslations(langId);
        }

        public void TranslateAction(ConfigTranslation translation)
        {
            this.Repository.TranslateAction(translation);
        }
    }
}