﻿using DocFlow.Core.Extensions;
using System;
using System.Web.Mvc;

namespace DocFlow.Filters
{
    [AttributeUsage(AttributeTargets.All)]
    public class ServiceAuthorization : ActionFilterAttribute
    {

        protected int Service {get ;set;}

        public ServiceAuthorization(int Service)
        {
            this.Service = Service;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!CheckSessionService())
            {
                filterContext.Result = new RedirectResult("/");
                return;
            }
        }
        

        private bool CheckSessionService()
        {
            return (int)SessionManager.GetActiveService() == ((int)this.Service)? true:false;
        }
    }
}