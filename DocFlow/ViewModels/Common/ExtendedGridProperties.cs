﻿using Syncfusion.JavaScript.Models;
using System.Collections.Generic;


public class ExtendedGridProperties<T> : GridProperties
    {
        public List<T> GridDataSource { get; set; }
    }
