﻿using DocFlow.Core.Extensions;
using DocFlow.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace DocFlow.ViewModels
{
    public class MultiSelectEditModel
    {
        public List<string> SelectedValues { get; set; }
        public IEnumerable<SelectListItem> Values { get; set; }
        public MultiSelectEditModel()
        {
            SelectedValues = new List<string>();
        }
        public MultiSelectEditModel(List<SelectItem> items)
        {
            SelectedValues = new List<string>();
            this.Values = items.Select(x=>new SelectListItem
            {
                Selected = x.Selected,
                Text = x.Text,
                Value = x.Value
            });
        }
        public MultiSelectEditModel(List<SelectItem> items, List<string> values)
        {
            this.Values = items.Select(x => new SelectListItem
            {
                Selected = values!=null ? values.Where(z=>z==x.Value).Any() : false,
                Text = x.Text,
                Value = x.Value
            });
            this.SelectedValues = values;
        }

        public MultiSelectEditModel(IEnumerable<SelectListItem> items)
        {
            SelectedValues = new List<string>();
            this.Values = items;
        }
        public MultiSelectEditModel(IEnumerable<SelectListItem> items, List<string> values)
        {
            this.Values = items;
            this.SelectedValues = values;
            if (values != null && items != null)
            {
                values.ForEach(v =>
                    this.Values.Where(f => f.Value == v).ForEach(
                    x => x.Selected = true
                ));

            }
        }

        /// <summary>
        /// Clears the previous selected ones
        /// </summary>
        /// <param name="selectvalue"></param>
        public void PreSelect(string selectvalue)
        {
            this.Values.Where(w => w.Value != selectvalue && w.Selected == true).ForEach(u => u.Selected = false);//cleaning
            this.Values.Where(w => w.Value == selectvalue).ForEach(u => u.Selected = true);
            this.SelectedValues = this.Values.Where(u => u.Value == selectvalue).Select(u => u.Value).ToList();
        }
        public void PreSelect(List<string> selectvalue)
        {
            this.Values.Where(w => !selectvalue.Contains(w.Value) && w.Selected == true).ForEach(u => u.Selected = false);//cleaning
            this.Values.Where(w => selectvalue.Contains(w.Value)).ForEach(u => u.Selected = true);
            this.SelectedValues = this.Values.Where(u => selectvalue.Contains(u.Value)).Select(u => u.Value).ToList();
        }
    }

    public class MultiSelectModel<T> where T : SelectListItem
    {
        public List<string> SelectedValues { get; set; }
        public IEnumerable<T> Values { get; set; }

        public MultiSelectModel()
        {
        }

        public MultiSelectModel(IEnumerable<T> items)
        {
            this.Values = items;
        }
        public MultiSelectModel(IEnumerable<T> items, List<string> values)
        {
            this.Values = items;
            this.SelectedValues = values;
            if (values != null && items != null)
            {
                values.ForEach(v =>
                    this.Values.Where(f => f.Value == v).ForEach(
                    x => x.Selected = true
                ));

            }
        }
        /// <summary>
        /// Clears the previous selected ones
        /// </summary>
        /// <param name="selectvalue"></param>
        public void PreSelect(string selectvalue)
        {
            this.Values.Where(w => w.Value != selectvalue && w.Selected == true).ForEach(u => u.Selected = false);//cleaning
            this.Values.Where(w => w.Value == selectvalue).ForEach(u => u.Selected = true);
            this.SelectedValues = this.Values.Where(u => u.Value == selectvalue).Select(u => u.Value).ToList();
        }
    }
}