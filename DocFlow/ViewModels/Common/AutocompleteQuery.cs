﻿using System.Collections.Generic;
using System.Linq;

namespace DocFlow.ViewModels.Common
{
    public class AutocompleteQuery
    {
        public List<string> Select { get; set; }
        public List<Where> Where { get; set; }
        public List<Sorted> Sorted { get; set; }

        public string Text
        {
            get
            {
                return this.Where != null && this.Where.Any() ? this.Where.FirstOrDefault().Value : string.Empty;
            }
        }
    }

    public class Where
    {
        public bool IsComplex { get; set; }
        public string Field { get; set; }
        public string Operator { get; set; }
        public string Value { get; set; }
        public bool IgnoreCase { get; set; }
    }

    public class Sorted
    {
        public string Name { get; set; }
        public string Direction { get; set; }
    }
}