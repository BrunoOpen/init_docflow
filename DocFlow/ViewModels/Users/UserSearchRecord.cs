﻿namespace DocFlow.ViewModels
{
    public class UserSearchRecord
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public bool Active { get; set; }
        public bool Valid { get; set; }

        public UserSearchRecord()
        {
            this.Valid = true;
        }
    }
}