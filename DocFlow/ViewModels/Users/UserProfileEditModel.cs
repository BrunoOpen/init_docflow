﻿using System.Collections.Generic;

namespace DocFlow.ViewModels
{
    public class UserProfileEditModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public List<ActionGroup> ActionsGroups { get; set; }
        public List<UserSearchRecord> Users { get; set; }
    }

    public class ActionGroup
    {
        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public bool Permission { get; set; }
    }

}