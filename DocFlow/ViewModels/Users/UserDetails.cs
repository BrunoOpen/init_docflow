﻿using System.Drawing;

namespace DocFlow.ViewModels
{
    public class UserDetails
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public bool IsSystem { get; set; }
        public Image Picture { get; set; }
        public byte[] Image { get; set; }
        //public int? CommercialId { get; set; }

//       Image to byte array
//           public byte[] imageToByteArray(System.Drawing.Image imageIn)
//{
// MemoryStream ms = new MemoryStream();
// imageIn.Save(ms,System.Drawing.Imaging.ImageFormat.Gif);
// return  ms.ToArray();
//}

//        byte to image
//            public Image byteArrayToImage(byte[] byteArrayIn)
//{
//     MemoryStream ms = new MemoryStream(byteArrayIn);
//     Image returnImage = Image.FromStream(ms);
//     return returnImage;
//}
    }
}