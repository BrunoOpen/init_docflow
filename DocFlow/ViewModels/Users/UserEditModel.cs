﻿using DocFlow.Core;
using DocFlow.Services;
using System.Collections.Generic;

namespace DocFlow.ViewModels
{
    public class UserEditModel : UserSearchRecord
    {
        public string Password { get; set; }
        public string PasswordConfirm { get; set; }
        public List<DisplayRole> Roles { get; set; }
        public List<UserActionGroup> ActionGroups { get; set; }

        //  public List<UserActionGroup> Actions { get; set; }
        public List<DisplayRole> AllRoles { get; set; }

        //public bool IsValid()
        //{
        //    if (string.IsNullOrEmpty(Password) || string.IsNullOrEmpty(PasswordConfirm))
        //        return false;
        //    if (Password != PasswordConfirm)
        //        return false;
        //    if (!Utils.PasswordReg.IsMatch(Password) || !Utils.PasswordReg.IsMatch(PasswordConfirm))
        //        return false;
        //    if (string.IsNullOrEmpty(Email) || !Utils.ValidateEmail(Email))
        //        return false;
        //    UserService service = new UserService();
        //    var account = service.GetUserByEmail(Email);
        //    if (account != null)
        //        return false;
        //    return true;
        //}

        public List<string> GetValidationMessages()
        {
            UserService service = new UserService();
            List<string> errors = new List<string>();
            DocFlow.DAL.AppAccount user = null;
            if (Id == 0)
            {
                if (string.IsNullOrEmpty(Username) || Username.Length<3)
                    errors.Add(Globalization.Language.ACCOUNTS_MESSAGES_USERNAME_DIMENSION);

                if (string.IsNullOrEmpty(Password))
                    errors.Add(Globalization.Language.ACCOUNTS_MESSAGES_PASSWORD_REQUIRED);

                if (string.IsNullOrEmpty(PasswordConfirm))
                    errors.Add(Globalization.Language.ACCOUNTS_MESSAGES_CONFIRMATION_REQUIRED);

                if (Password != PasswordConfirm)
                    errors.Add(Globalization.Language.ACCOUNTS_MESSAGES_DONT_MATCH);

                if (!Utils.PasswordReg.IsMatch(Password))
                    errors.Add(Globalization.Language.ACCOUNTS_MESSAGES_PASSWORD_RULES);

                 user = service.GetUserByUserName(Username);
                if (user != null)
                    errors.Add(Globalization.Language.ACCOUNTS_MESSAGES_USERNAME_USED);
            }
            if (string.IsNullOrEmpty(Email) || !Utils.ValidateEmail(Email))
                errors.Add(Globalization.Language.ACCOUNTS_MESSAGES_EMAIL_INVALID);
            else
            {
                user = service.GetUserByEmail(Email);
                if (user != null && user.Id != Id)
                    errors.Add(Globalization.Language.ACCOUNTS_MESSAGES_EMAIL_USED);
            }
            
            
            return errors;
        }
    }
    public class UserActionGroup
    {
        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public bool Permission { get; set; }
        public List<DisplayRole> Roles { get; set; }
        public List<ServiceVariable> Variables { get; set; }
    }

    public class DisplayRole
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Selected { get; set; }
    }
    public class ServiceVariable
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Selected { get; set; }
        public bool Required { get; set; }
    }
}