﻿using DocFlow.Core;
using DocFlow.Core.Objects;
using DocFlow.Services;
using System.Collections.Generic;

namespace DocFlow.ViewModels
{
    public class ChangePasswordModel
    {
        public int UserId { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }

        public bool IsValid()
        {
            if (UserId < 1 || string.IsNullOrEmpty(OldPassword) || string.IsNullOrEmpty(NewPassword) || string.IsNullOrEmpty(ConfirmPassword))
                return false;
            if (NewPassword != ConfirmPassword)
                return false;
            if ( !Utils.PasswordReg.IsMatch(NewPassword) || !Utils.PasswordReg.IsMatch(ConfirmPassword))
                return false;
            UserService service = new UserService();
            DataOperationResult<bool> result = service.VerifyOldPassword(UserId, OldPassword);
            if (!result.Data)
                return false;
            return true;
        }

        public List<string> GetValidationMessages()
        {
            List<string> errors = new List<string>();
            if (UserId < 1)
                errors.Add(Globalization.Language.ACCOUNTS_MESSAGES_USER_INVALID);

            if (string.IsNullOrEmpty(NewPassword))
                errors.Add(Globalization.Language.ACCOUNTS_MESSAGES_NEW_PASSWORD_REQUIRED);

            if (string.IsNullOrEmpty(ConfirmPassword))
                errors.Add(Globalization.Language.ACCOUNTS_MESSAGES_CONFIRMATION_REQUIRED);

            if (NewPassword != ConfirmPassword)
                errors.Add(Globalization.Language.ACCOUNTS_MESSAGES_NEW_DONT_MATCH);

            if (!Utils.PasswordReg.IsMatch(NewPassword))
                errors.Add(Globalization.Language.ACCOUNTS_MESSAGES_NEW_PASSWORD_RULES);

            UserService service = new UserService();
            DataOperationResult<bool> result = service.VerifyOldPassword(UserId, OldPassword);
            if (!result.Data)
                errors.Add(Globalization.Language.ACCOUNTS_MESSAGES_PASSWORD_INCORRECT);
            return errors;
        }


    }
}