﻿using System.Security.Principal;

namespace DocFlow.ViewModels
{
    public interface IAuthorize : IPrincipal
    {
        bool IsInAction(string actioncode);
        //bool IsInAction(string actioncode, string variable);
        bool IsInAnyAction(params string[] actioncodes);
        bool IsSuperAdmin();
        int UserId { get; }

    }
}
