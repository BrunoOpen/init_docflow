﻿using DocFlow.Core.Objects;
using System.Collections.Generic;
using System.Linq;
namespace DocFlow.ViewModels
{
    public class TopMenuViewModel
    {
        public List<Language> Languages { get; set; }
        public List<InstanceRecord> Instances { get; set; }

        public InstanceRecord CurrentInstance { get; set; }
        public Service CurrentService { get; set; }
        public List<TemplateModel> Templates  { get; set; }
      
        public List<Service> Services { get; set; }
        //public List<FileProcessingLog> FileNotifications { get; set; }
        public List<DocFlow.ViewModels.Dashboard.DashboardNotificationModel> FileNotifications { get; set; }

       
        public Language CurrentLanguage
        {
            get
            {
                return
                    this.Languages.FirstOrDefault(x => x.Selected == true);
            }
        }
    }

    public class Language
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Image { get; set; }
        public string DisplayCode { get; set; }
        public bool Selected { get; set; }
        public bool Default { get; set; }
        public int Id { get; set; }
    }
    public class TemplateModel
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public string FileName { get; set; }

        public TemplateModel(int id, string name, string filename)
        {
            this.FileName = filename;
            this.Id = id;
            this.Name = name;
        }
        public TemplateModel() { }
    }
    public class Service
    {
        public string Name { get; set; }
        public Enums.Services Type { get; set; }
        public bool Selected { get; set; }
    }
}