﻿using System;

namespace DocFlow.ViewModels
{
    public class ExampleSearchRecord
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string Email { get; set; }
        public bool Active { get; set; }
        public decimal Decimal { get; set; }

    }
}