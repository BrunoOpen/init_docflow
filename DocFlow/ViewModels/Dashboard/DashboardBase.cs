﻿using System;

namespace DocFlow.ViewModels
{
    public class DashboardBase
    {
        public DateTime MaxDate { get; set; }
        public DateTime CurrentDate { get; set; }
        public DateTime MinDate { get; set; }
    }
}