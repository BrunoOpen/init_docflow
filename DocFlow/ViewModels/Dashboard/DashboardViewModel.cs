﻿using System;
using System.Collections.Generic;

namespace DocFlow.ViewModels
{
    public class DashboardViewModel
    {
        public DateTime Month { get; set; }
        public EnergyKPIViewModel EnergyKPI { get; set; }
        public WaterKPIViewModel WaterKPI { get; set; }
        public List<FinancialMonthKPIViewModel> FinancialKPI { get; set; }

        public DashboardViewModel()
        {
            this.FinancialKPI = new List<FinancialMonthKPIViewModel>(2);
        }

        public DashboardViewModel(DateTime month)
        {
            this.Month = month;
            this.FinancialKPI = new List<FinancialMonthKPIViewModel>(2);
        }
    }

    public class EnergyKPIViewModel
    {
        public EnergyMonthKPIViewModel current { get; set; }
        public EnergyMonthKPIViewModel last { get; set; }
        public string IconSupplier
        {
            get
            {
                if(this.current.LocalsSupplier < this.last.LocalsSupplier)
                    return "fa fa-arrow-down";
                if (this.current.LocalsSupplier > this.last.LocalsSupplier)
                    return "fa fa-arrow-up";
                return "fa fa-arrow-right";
            }
        }
        public string IconPrivate
        {
            get
            {
                if (this.current.LocalsPrivate < this.last.LocalsPrivate)
                    return "fa fa-arrow-down";
                if (this.current.LocalsPrivate > this.last.LocalsPrivate)
                    return "fa fa-arrow-up";
                return "fa fa-arrow-right";
            }
        }
        public string IconProduction
        {
            get
            {
                if (this.current.Production < this.last.Production)
                    return "fa fa-arrow-down";
                if (this.current.Production > this.last.Production)
                    return "fa fa-arrow-up";
                return "fa fa-arrow-right";
            }
        }
        public string IconCost
        {
            get
            {
                if (this.current.Cost < this.last.Cost)
                    return "fa fa-arrow-down";
                if (this.current.Cost > this.last.Cost)
                    return "fa fa-arrow-up";
                return "fa fa-arrow-right";
            }
        }
        public string IconConsumption
        {
            get
            {
                if (this.current.Consumption < this.last.Consumption)
                    return "fa fa-arrow-down";
                if (this.current.Consumption > this.last.Consumption)
                    return "fa fa-arrow-up";
                return "fa fa-arrow-right";
            }
        }
    }

    public class EnergyMonthKPIViewModel
    {
        /*public EnergyMonthKPIViewModel()
        {
            this.LocalsSupplier = 0;
            this.Cost = 0;
            this.Consumption = 0;
            this.Production = 0;
        }*/
        public int LocalsSupplier { get; set; }
        public int LocalsPrivate { get; set; }
        public decimal Cost { get; set; }
        public decimal Consumption { get; set; }
        public decimal Production { get; set; }
    }

    public class WaterKPIViewModel
    {
        public WaterKPIViewModel()
        {
            this.LocalsDone = 0;
            this.Cost = 0;
            this.Consumption = 0;
        }
        public int LocalsDone { get; set; }
        public decimal Cost { get; set; }
        public decimal Consumption { get; set; }
    }

    public class FinancialKPIViewModel
    {
        public FinancialKPIViewModel()
        {
            //this.EnergyCost = 0;
            //this.EnergyPayments = 0;
            //this.WaterCost = 0;
            //this.WaterPayments = 0;
            this.KpiValue = 0;
        }
        //public decimal WaterCost { get; set; }
        //public decimal WaterPayments { get; set; }
        //public decimal EnergyCost { get; set; }
        //public decimal EnergyPayments { get; set; }

        public decimal KpiValue {get;set; }
    }

    public class FinancialKPIModel
    {
        public List<FinancialMonthKPIViewModel> FinancialKpi { get; set; }

        public FinancialKPIModel()
        {
            this.FinancialKpi = new List<FinancialMonthKPIViewModel>();
        }
    }

    public class FinancialMonthKPIViewModel
    {
        public FinancialKPIViewModel Last { get; set; }

        public FinancialKPIViewModel Current { get; set; }

        public string Icon 
        { 
            get 
            {
                return this.Last.KpiValue > this.Current.KpiValue ? "fa fa-arrow-down" : this.Last.KpiValue < this.Current.KpiValue ? "fa fa-arrow-up" : "fa fa-arrow-right";
            } 
            
        }

    }

  
}