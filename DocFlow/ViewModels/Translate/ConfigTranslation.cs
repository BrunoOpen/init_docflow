﻿namespace DocFlow.ViewModels
{
    public class ConfigTranslation
    {
      //  public int? Id { get; set; }
        public int LanguageId { get; set; }

        public int EntityId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string TranslatedName { get; set; }
        public string TranslatedDescription { get; set; }
        public string SaveAction { get; set; }
        public bool IsTranslated { get; set; }
    }
}