﻿using System.Collections.Generic;

namespace DocFlow.ViewModels.Translate
{
    public class TranslateLanguage
    {
        public List<ConfigTranslation> Translations { get; set; }
        public string LanguageCode { get; set; }
    }

    public class TranslateTable
    {
        /// <summary>
        /// List of x languages - n itens to translate
        /// </summary>
        public List<TranslateLanguage> Languages { get; set; }
        public string TableName { get; set; }
     //   public string Description { get; set; }
    }
}