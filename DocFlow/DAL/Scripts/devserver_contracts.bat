@Echo off
SET LOGFILE=SP_logFile.log
IF EXIST %LOGFILE% del /F %LOGFILE%


call :Logit >> %LOGFILE% 
SET /p fim="Processo terminado!	Prima ENTER para terminar e verifique o ficheiro de logs!"
exit /b 0

:Logit

FOR /r %%G IN (*.sql) DO (
	
	ECHO.%%G| FIND /I "\DatabaseStarting">Nul && ( 
		echo *NAO EXECUTOU %%G *
	) || (
		ECHO.%%G| FIND /I "\SPs">Nul && ( 
			echo *NAO EXECUTOU %%G *
		) || (
			ECHO.%%G| FIND /I "\DatabaseChanging">Nul && ( 
				echo *NAO EXECUTOU %%G *
			) || (
				echo *EXECUTOU %%G *
				sqlcmd /S . /d VDFMeteringContracts -E -i"%%G"
			)
		)
	)
)






