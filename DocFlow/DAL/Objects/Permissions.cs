﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace DocFlow.DAL
{
    public class Permissions
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [MaxLength(250)]
        [Required]
        [Column(TypeName = "NVARCHAR")]
        public string Name { get; set; }

        public int ParentId { get; set; }

        [ForeignKey("PermissionType")]
        public int PermissionTypeId { get; set; }
        public PermissionType PermissionType { get; set; }



    }
}