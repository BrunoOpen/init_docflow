﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DocFlow.DAL
{
    public class AppContext
    {
        [Key]
        public int Id { get; set; }
        public virtual AppAccount Account { get; set; }
        public virtual VariableValue VariableValue { get; set; }
        public virtual AppActionGroup ActionGroup { get; set; }
        public virtual AppAction Action { get; set; }
    }
}