﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DocFlow.DAL
{
   
    [Table("MenuActions")]
    public class MenuAction
    {
        [Key, Column("MenuID", Order = 1)]
        [ForeignKey("Menu")]
        public int MenuID { get; set; }
        public virtual Menu Menu { get; set; }

        [Key, Column("ActionID", Order = 2)]
        [ForeignKey("AppAction")]
        public int AppActionId { get; set; }
        public virtual AppAction AppAction { get; set; }

    }
}