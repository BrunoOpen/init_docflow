﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DocFlow.DAL
{
    [Table("Providers")]
    public class Provider : Entity
    {
        [DefaultValue(true)]
        public bool IsActive { get; set; }
        [DefaultValue(true)]
        public bool IsCommercialization { get; set; }
        public bool IsDESPProcessed { get; set; }
        public decimal IRF { get; set; }

        public string VendorCode { get; set; }
        public string ShortName { get; set; }
        public string NIF { get; set; }
        public string Color { get; set; }
        
    }
}