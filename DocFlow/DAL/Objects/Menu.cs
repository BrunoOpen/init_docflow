﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DocFlow.DAL
{
    public class Menu
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("ParentMenu")]
        public int? ParentID { get; set; }
        public Menu ParentMenu { get; set; }
        public string Name { get; set; }
        public string Href { get; set; }
        public string Icon { get; set; }
        public int Order { get; set; }

        [ForeignKey("VariableValue")]
        public int? VariableValueID { get; set; }
        public VariableValue VariableValue { get; set; }

    }

    [Table("Menus_I18N")]
    public class Menu_I18N
    {
        [Key, Column("MenuID", Order = 0)]
        [ForeignKey("Menu")]
        public int MenuID { get; set; }
        public virtual Menu Menu { get; set; }
        [Key, Column("LanguageID", Order = 1)]
        [ForeignKey("Language")]
        public int LanguageID { get; set; }
        public virtual Language Language { get; set; }
        public string NameT { get; set; }

    }
}