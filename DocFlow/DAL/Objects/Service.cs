﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DocFlow.DAL
{
    public class Service
    {
        public Service()
        {

        }

        public Service(string name)
            : this()
        {
            this.Name = name;
        }

        [Key]
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
        public string Code { get; set; }
        public string Database { get; set; }
    }

    [Table("Services_I18N")]
    public class Service_I18N
    {
        [Key, Column("ServiceID", Order = 0)]
        [ForeignKey("Service")]
        public int ServiceID { get; set; }
        public virtual Service Service { get; set; }
        [Key, Column("LanguageID", Order = 1)]
        [ForeignKey("Language")]
        public int LanguageID { get; set; }
        public virtual Language Language { get; set; }
        public string NameT { get; set; }
    }
}