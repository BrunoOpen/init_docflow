﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DocFlow.DAL
{
    public class Application
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [MaxLength(50)]
        [Column(TypeName ="NVARCHAR")]
        public string Code { get; set; }

        [MaxLength(250)]
        [Column(TypeName = "NVARCHAR")]
        public string Name { get; set; }

    }
}