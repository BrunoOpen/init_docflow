﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DocFlow.DAL
{
    public class Profile
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [MaxLength(250)]
        [Required]
        [Column(TypeName = "NVARCHAR")]
        public string Name { get; set; }

        [ForeignKey("Application")]
        public int? ApplicationId { get; set; }
        public Application Application { get; set; }

        [ForeignKey("Instance")]
        public int? InstanceId { get; set; }
        public Instance Instance { get; set; }

    }
}