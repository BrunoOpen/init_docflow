﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DocFlow.DAL
{
    public class Variable
    {
        public Variable() { }

        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string EntityTable { get; set; }
        public string EntityColumn { get; set; }

        public virtual ICollection<VariableValue> VariableValues { get; set; }
        //public virtual ICollection<AppActionGroupVariable> AppActionGroupVariables { get; set; }
        public virtual ICollection<AppActionVariable> AppActionVariables { get; set; }
    }
}