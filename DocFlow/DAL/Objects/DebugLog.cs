﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DocFlow.DAL
{
    public class DebugLog
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("User")]
        public int UserID { get; set; }
        public virtual AppAccount User { get; set; }
        public DateTime Date { get; set; }
        public string Context { get; set; }
        public int? ContextID { get; set; }
        public string Msg { get; set; }
    }
}