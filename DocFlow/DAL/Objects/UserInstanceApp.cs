﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DocFlow.DAL
{
    public class UserInstaceApp
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("AppAccount")]
        public int AppAcountId { get; set; }
        public AppAccount AppAccount { get; set; }

        [ForeignKey("Instance")]
        public int? InstanceId { get; set; }
        public Instance Instance { get; set; }

        [ForeignKey("Application")]
        public int? ApplicationId { get; set; }
        public Application Application { get; set; }

        public bool? isDefault { get; set; }

    }
}