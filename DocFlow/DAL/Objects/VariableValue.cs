﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DocFlow.DAL
{
    public class VariableValue
    {
        [Key]
        public int Id { get; set; }
        public int VariableID { get; set; }
        public virtual Variable Variable { get; set; }
        public int Value { get; set; }
        public string Code { get; set; }
        public string ShortName { get; set; }
        public string LongName { get; set; }
    }
}