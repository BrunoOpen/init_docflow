﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Migrations.History;

namespace DocFlow.Migrations
{
    public sealed class MyHistoryRow : HistoryRow
    {
        //We will just add a text column
        public bool IsConfigured { get; set; }
    }

    public class MyHistoryContext : HistoryContext
    {
        public new DbSet<MyHistoryRow> History { get; set; }

        public MyHistoryContext(DbConnection dbConnection, string defaultSchema)
            : base(dbConnection, defaultSchema)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<HistoryRow>().ToTable(tableName: "MigrationHistory", schemaName: "admin");
            modelBuilder.Entity<HistoryRow>().Property(p => p.MigrationId).HasColumnName("MigrationID");

        }
    } 


}