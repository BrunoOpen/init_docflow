﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DocFlow.DAL
{
    public class AppRole
    {
        public AppRole()
        {
            this.AppAcounts = new List<AppAccountRole>();
            this.AppRoleActionGroups = new List<AppRoleActionGroup>();
        }

        public AppRole(string name)
            : this()
        {
            this.Name = name;
            this.IsActive = true;
        }

        public AppRole(string name, string description)
            : this(name)
        {
            this.Description = description;
            this.IsActive = true;
        }

        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<AppAccountRole> AppAcounts { get; set; }
        public virtual ICollection<AppRoleActionGroup> AppRoleActionGroups { get; set; }

    }

    [Table("Roles_I18N")]
    public class AppRole_I18N
    {
        [Key, Column("AppRoleID", Order = 0)]
        [ForeignKey("AppRole")]
        public int AppRoleID { get; set; }
        public virtual AppRole AppRole { get; set; }
        [Key, Column("LanguageID", Order = 1)]
        [ForeignKey("Language")]
        public int LanguageID { get; set; }
        public virtual Language Language { get; set; }
        public string NameT { get; set; }
        public string DescriptionT { get; set; }

    }
}