﻿using DocFlow.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DocFlow.DAL
{
    public class AppActionGroup
    {
        public AppActionGroup() { }

        [Key]
        public int Id { get; set; }

        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

        public bool IsActive { get; set; }

        public int? MainActionGroupID { get; set; }

        public virtual AppActionGroup MainActionGroup { get; set; }

        public virtual ICollection<AppActionGroupActions> ActionGroupActions { get; set; }
        public virtual ICollection<AppRoleActionGroup> AppRoleActionGroups { get; set; }
    }
    [Table("ActionGroups_I18N")]
    public class AppActionGroup_I18N
    {
        [Key, Column("AppActionGroupID", Order = 0)]
        [ForeignKey("AppActionGroup")]
        public int AppActionGroupID { get; set; }
        public virtual AppActionGroup AppActionGroup { get; set; }

        [Key, Column("LanguageID", Order = 1)]
        [ForeignKey("Language")]
        public int LanguageID { get; set; }
        public virtual Language Language { get; set; }
        public string NameT { get; set; }
        public string DescriptionT { get; set; }
    }

}