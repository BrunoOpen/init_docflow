﻿using Microsoft.AspNet.Identity.EntityFramework;
using OpenAdmin.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Migrations.History;
using System.Linq;
using System.Web;

namespace DocFlow.DAL
{


    // Must be expressed in terms of our custom types:
    public class ApplicationDBContext
        : IdentityDbContext<AppAccount, AppAction,
        int, AppAccountLogin, AppAccountAction, AppAccountClaim>
    {
        public ApplicationDBContext()
            : base("ApplicationDBConnection")
        {
        }

        static ApplicationDBContext()
        {
            //Database.SetInitializer<ApplicationDBContext>(new ApplicationDBInitializer());
        }

        public static ApplicationDBContext Create()
        {
            return new ApplicationDBContext();
        }
        
        /* Permissions */
        public virtual DbSet<AppRoleActionGroup> AppRoleActionGroups { get; set; }
        public virtual DbSet<AppActionGroupActions> AppActionGroupActions { get; set; }
        public virtual DbSet<AppRole> AppRoles { get; set; }
        public virtual DbSet<AppAccountRole> AppAccountRoles { get; set; }
        public virtual DbSet<AppActionGroup> AppActionGroups { get; set; }
        public virtual DbSet<Variable> Variables { get; set; }
        public virtual DbSet<VariableValue> VariableValues { get; set; }
        public virtual DbSet<AppActionVariable> AppActionVariables { get; set; }
        public virtual DbSet<AppContext> AppContexts { get; set; }
        public virtual DbSet<Entity> Entitys { get; set; }
        public virtual DbSet<Action_I18N> Actions_I18N { get; set; }
        public virtual DbSet<Provider> Providers { get; set; }
        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<Person> Persons { get; set; }
        public virtual DbSet<Operator> Operators { get; set; }

        public virtual DbSet<Resource> Resources { get; set; }

        public virtual DbSet<Application> Applications { get; set; }

        public virtual DbSet<BillEntity> BillEntities { get; set; }

        public virtual DbSet<Instance> Instances { get; set; }

        public virtual DbSet<Profile> Profiles { get; set; }

        public virtual DbSet<AppInstance> AppInstances { get; set; }

        public virtual DbSet<UserInstaceApp> UsersInstancesApps { get; set; }

        public virtual DbSet<UserProfile> UserProfiles { get; set; }

        public virtual DbSet<PermissionType> PermissionTypes { get; set; }
        public virtual DbSet<Permissions> Permissions { get; set; }

        public virtual DbSet<ProfilePermission> ProfilePermissions { get; set; }

        //Internationalization Tables
        public virtual DbSet<Language> Languages { get; set; }
        public virtual DbSet<AppActionGroup_I18N> AppActionGroups_I18N { get; set; }
        public virtual DbSet<AppRole_I18N> AppRoles_I18N { get; set; }
        public virtual DbSet<Menu_I18N> Menus_I18N { get; set; }
        public virtual DbSet<Service_I18N> Services_I18N { get; set; }
        
        //Aggregates
        //public virtual DbSet<Agr_Period> Agr_Periods { get; set; }
        //public virtual DbSet<Agr_ServicePeriod> Agr_ServicePeriods { get; set; }

        //Processing
        public virtual DbSet<Menu> Menus { get; set; }
        public virtual DbSet<MenuAction> MenuActions { get; set; }


        public virtual DbSet<ErrorLog> ErrorLogs { get; set; }
        public virtual DbSet<DebugLog> DebugLogs { get; set; }
        public virtual DbSet<LogActivity> LogActivitys { get; set; }
        public virtual DbSet<LogActivityType> LogActivityTypes { get; set; }

        // Override OnModelsCreating:
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);


            modelBuilder.Entity<AppAction>()
                .ToTable("Actions");
            modelBuilder.Entity<AppRole>()
                .ToTable("Roles");
            modelBuilder.Entity<AppAccount>()
                .ToTable("Accounts");
            modelBuilder.Entity<AppAccountAction>()
                .ToTable("AccountsActions");
            modelBuilder.Entity<AppAccountAction>()
                .Property(t => t.RoleId).HasColumnName("ActionID");
            modelBuilder.Entity<AppAccountAction>()
                .Property(t => t.UserId).HasColumnName("AccountID");
            modelBuilder.Entity<AppAccountClaim>()
                .ToTable("AccountsClaims");
            modelBuilder.Entity<AppAccountLogin>()
                .ToTable("AccountsLogins");
            modelBuilder.Entity<AppActionGroup>()
                .ToTable("ActionGroups");
            modelBuilder.Entity<AppActionVariable>()
                .ToTable("ActionVariables");
            modelBuilder.Entity<AppContext>()
                .ToTable("Contexts");
            modelBuilder.Entity<AppAccountRole>()
               .HasKey(cp => new { cp.AppAccountId, cp.AppRoleId })
               .ToTable("AccountsRoles");
            modelBuilder.Entity<AppRole>()
                        .HasMany(c => c.AppAcounts)
                        .WithRequired()
                        .HasForeignKey(cp => cp.AppRoleId);
            modelBuilder.Entity<AppAccount>()
                        .HasMany(p => p.AppAccountRoles)
                        .WithRequired()
                        .HasForeignKey(cp => cp.AppAccountId);

            //modelBuilder.Entity<Agr_Period>()
            //            .HasOptional(s => s.ModifiedByUser)
            //            .WithMany(s => s.ModifiedPeriods)
            //            .HasForeignKey(s => s.ModifiedBy)
            //            .WillCascadeOnDelete(false);
        }



    }



}