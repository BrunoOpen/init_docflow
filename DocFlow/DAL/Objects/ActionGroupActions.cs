﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DocFlow.DAL
{
    [Table("ActionGroupActions")]
    public class AppActionGroupActions
    {
        [Key, Column("ActionID", Order = 1)]
        [ForeignKey("AppAction")]
        public int AppActionId { get; set; }
        public virtual AppAction AppAction { get; set; }

        [Key, Column("ActionGroupID", Order = 2)]
        [ForeignKey("AppActionGroup")]
        public int AppActionGroupId { get; set; }
        public virtual AppActionGroup AppActionGroup { get; set; }

    }
}