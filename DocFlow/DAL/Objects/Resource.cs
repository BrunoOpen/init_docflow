﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DocFlow.DAL
{

    public class Resource
    {


        [MaxLength(10)]
        [Key, Column(Order = 0, TypeName = "VARCHAR")]
        public string Culture { get; set; }

        [MaxLength(100)]
        [Key, Column(Order = 1, TypeName = "VARCHAR")]
        public string Name { get; set; }

        [MaxLength(4000)]
        [Column(TypeName = "NVARCHAR")]
        public string Value { get; set; }

    }

}