﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DocFlow.DAL
{
    public class LogActivity
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("LogActivityType")]
        public int LogActivityTypeID { get; set; }
        public virtual LogActivityType LogActivityType { get; set; }

        [ForeignKey("User")]
        public int? UserID { get; set; }
        public virtual AppAccount User { get; set; }

        public string UserName { get; set; }
        public DateTime Date { get; set; }
        public string Action { get; set; }
        [Column(TypeName = "ntext")]
        public string Parameters { get; set; }
        public string SessionID { get; set; }
        public string UserAgent { get; set; }
        public string IPAddress { get; set; }
        [ForeignKey("ErrorLog")]
        public int? ErrorLogID { get; set; }
        public virtual ErrorLog ErrorLog { get; set; }

    }
}