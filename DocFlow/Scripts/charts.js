﻿var Charts = function () {

    function pieChart(targetElem, dataset, titleField, legend, clickHandler) {
        if (IsNullOrEmpty(legend))
            legend = '';

        var piechart = AmCharts.makeChart(targetElem, {
            "type": "pie",
            "startDuration": 0,
            "theme": "none",
            "addClassNames": true,
            "decimalSeparator": ",",
            "colorField": "Color",
            "precision": 2,
            "thousandsSeparator": ".",
            "colors": [
		        "#E43A45",
		        "#4B77BE",
		        "#F4D03F",
		        "#26C281",
		        "#8877A9",
		        "#F2784B",
		        "#BFBFBF",
		        "#E26A6A",
		        "#2AB4C0",
		        "#C5B96B",
		        "#BF55EC",
		        "#5E738B",
		        "#D91E18",
		        "#E87E04",
		        "#555555"
            ],
            "legend": {
                "position": "bottom",
                "valueText": "[[value]] " + legend,
                "equalWidths": false,
                "valueAlign": "right",
                "valueWidth": 100,
                "align": "center",
                "autoMargins": false,
                "textClickEnabled": true,
                //"marginRight": 100,
                //"autoMargins": false
            },
            "innerRadius": "30%",
            "defs": {
                "filter": [{
                    "id": "shadow",
                    "width": "200%",
                    "height": "200%",
                    "feOffset": {
                        "result": "offOut",
                        "in": "SourceAlpha",
                        "dx": 0,
                        "dy": 0
                    },
                    "feGaussianBlur": {
                        "result": "blurOut",
                        "in": "offOut",
                        "stdDeviation": 5
                    },
                    "feBlend": {
                        "in": "SourceGraphic",
                        "in2": "blurOut",
                        "mode": "normal"
                    }
                }]
            },
            "dataProvider": dataset,
            "valueField": "valueField",
            "titleField": "titleField",
            "export": {
                "enabled": true,
                "libs": {
                    "path": "http://www.amcharts.com/lib/3/plugins/export/libs/"
                },
                position: 'top-left'
            }
        });


        if (!IsNullOrEmpty(clickHandler))
            piechart.addListener("clickSlice", clickHandler);
    }

    var pieChartNewVersion = function (targetElem, dataset) {
        AmCharts.makeChart(targetElem,
				{
				    "type": "pie",
				    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
				    "labelText": "[[percents]]%",
				    "minRadius": 10,
				    "alpha": 0.8,
				    "colors": [
		                "#E43A45",
		                "#4B77BE",
		                "#F4D03F",
		                "#26C281",
		                "#8877A9",
		                "#F2784B",
		                "#BFBFBF",
		                "#E26A6A",
		                "#2AB4C0",
		                "#C5B96B",
		                "#BF55EC",
		                "#5E738B",
		                "#D91E18",
		                "#E87E04",
		                "#555555"
				    ],
				    "pullOutEffect": "easeOutSine",
				    "valueField": "valueField",
				    "titleField": "titleField",
				    "colorField": "Color",
				    "decimalSeparator": ",",
				    "thousandsSeparator": ".",
				    "allLabels": [],
				    "balloon": {},
				    "legend": {
				        "enabled": true,
				        "align": "center",
				        "maxColumns": 5,
				        "equalWidths": true,
				        "valueWidth": 50,
				        "valueText": "[[percents]] %"
				    },
				    "titles": [],
				    "dataProvider": dataset,
				    "export": {
				        "enabled": true,
				        "libs": {
				            "path": "http://www.amcharts.com/lib/3/plugins/export/libs/"
				        },
				        position: 'top-left'
				    }
				}
			);
    }

    function reportPieChartClickHandler(sliceIndex) {
        var numberOfSlices = sliceIndex.chart.dataProvider.length;
        var pieChartId = sliceIndex.chart.div.id;
        var drillBtn = $("#" + pieChartId).closest(".portlet").find(".drillBtn");
        if (sliceIndex.dataItem.dataContext.infoValue != undefined) {

            var id = sliceIndex.dataItem.dataContext.infoValue;
            var exists = $.inArray(id, Reporting.PieCharts[pieChartId]);
            if (exists == -1) {
                // adicionar
                Reporting.PieCharts[pieChartId].push(id);
                // ativar botao de detalhes
                if (Reporting.PieCharts[pieChartId].length == numberOfSlices) {
                    // desactivar o botão de detalhe (drill)
                    $(drillBtn).attr("disabled", true);
                } else {
                    $(drillBtn).removeAttr("disabled");
                }
            } else {
                // remover
                Reporting.PieCharts[pieChartId].splice(exists, 1);
                if (Reporting.PieCharts[pieChartId].length == 0) {
                    // desactivar o botão de detalhe (drill)
                    $(drillBtn).attr("disabled", true);
                } else {
                    $(drillBtn).removeAttr("disabled");
                }

            }
        }
        else {
            selected = undefined;
        }
    }

    function barChartCreate(targetElement, graphType, categoryField, graphs, dataSet, axisTitle, isClickable) {
        createSerialChart(targetElement, categoryField, graphs, dataSet, axisTitle, isClickable);

    }
    var zoomChart = function (zoom) {

        var graphVal = $.grep(zoom.chart.graphs, function (n, i) {
            return n.valueField != 'media';
        });

        var valueItem = graphVal[0].valueField;
        var dataProvider = [];
        var media = 0;
        var total = 0;
        for (var i = zoom.startIndex; i <= zoom.endIndex; i++) {
            media += parseFloat(zoom.chart.dataProvider[i][valueItem]);
            total++;
            //dataProvider.push(zoom.chart.dataProvider[i]);
        }

        media = media / total;
        $.each(zoom.chart.dataProvider, function (idx, elm) {
            elm.media = media;
        });
        // different zoom methods can be used - zoomToIndexes, zoomToDates, zoomToCategoryValues
        if (zoom.chart.ignoreZoomEvent) {
            zoom.chart.ignoreZoomEvent = false;
            zoom.chart.validateNow(true, true);
            return;
        }

        //chart.dataProvider = dataProvider;
        zoom.chart.ignoreZoomEvent = true;
        //zoom.chart.validateData();
        zoom.chart.validateNow(true, true);
        console.log(zoom);

        //zoom.chart.zoomToIndexes(zoom.startIndex, zoom.endIndex);
        // restore same zoom
        //zoom.chart.ignoreZoom = true;
        //zoom.chart.zoomToIndexes(zoom.startIndex, zoom.endIndex);


    }

    var createStackableChart = function (targetElement, categoryField, graphs, dataset, axisTitle, guides, handleClick) {

        if (IsNullOrEmpty(window.__chartsinithandler)) {
            // Hack para que os eixos do xx comecem no minimo valor  ou entao 0.
            //SRC: http://stackoverflow.com/questions/30362230/amcharts-replace-0-with-1-in-vertical-valueaxis
            AmCharts.addInitHandler(function (chart) {

                // check if there are any value axes defined
                if (typeof chart.valueAxes !== "object" || !chart.valueAxes.length)
                    return;

                // check if chart's value axis has "tentativeMinimum" set
                // For the sake of simplicity we're just gonna take the first 
                // value axis and first graph.
                // Normally we would want to check all value axes and their attached
                // graphs to check for their respective values.
                var min = 0;
                var axis = chart.valueAxes[0];
                var axis2 = null;
                if (chart.valueAxes.length > 1 && chart.valueAxes[1].position == "right")
                    axis2 = chart.valueAxes[1];
                axis.tentativeMinimum = 0;
                // if (!IsNullOrEmpty(axis2))
                //  axis.tentativeMinimum = 0;
                //axis.minimum = 0;

                for (var i = 0; i < chart.graphs.length; i++) {


                    var field = chart.graphs[i].valueField;

                    for (var x = 0; x < chart.dataProvider.length; x++) {
                        if (min === undefined || (chart.dataProvider[x][field] < min))
                            min = chart.dataProvider[x][field];
                    }

                }

                //if (min <= (axis.tentativeMinimum))
                if (min >= 0) {
                    axis.minimum = 0;
                    axis.strictMinMax = true;
                    if (axis2 != null) {
                        axis2.minimum = 0;
                        axis2.strictMinMax = true;
                    }
                }


                //if (axis.tentativeMinimum === undefined || axis.minimumThreshold === undefined)
                //    return;
                //axis.tentativeMinimum = 0;
                //axis.minimum = 0;
                //// get first charts valueField to check agains
                //var field = chart.graphs[0].valueField;

                //// cycle through the data

                //for (var x = 0; x < chart.dataProvider.length; x++) {
                //    if (min === undefined || (chart.dataProvider[x][field] < min))
                //        min = chart.dataProvider[x][field];
                //}

                // check if min is within the threshold
                //if (min <= (axis.tentativeMinimum ))
                //    axis.minimum = axis.tentativeMinimum;

            }, ["serial"]);

            window.__chartsinithandler = true;

        }

        var chart = AmCharts.makeChart(targetElement, {
            "type": "serial",
            "theme": "light",
            "marginRight": 30,
            "legend": {
                "markerBorderThickness": 0,
                "useGraphSettings": true,
                "useMarkerColorForLabels": true,
                "useMarkerColorForValues": true,
                "equalWidths": true,
                "enabled": true,
                "maxColumns": 4,
                "valueAlign": "left"

            },
            "dataProvider": dataset,
            "valueAxes": [{
                "stackType": "regular",
                "gridAlpha": 0.07,
                "position": "left",

            }],
            "valueScrollbar": {
                "oppositeAxis": false,
                //"offset": 50,
                "scrollbarHeight": 5
            },
            "graphs": graphs,
            "plotAreaBorderAlpha": 0,
            "marginTop": 10,
            "marginLeft": 0,
            "marginBottom": 0,
            "chartScrollbar": {},
            "chartCursor": {
                "cursorAlpha": 0
            },
            "categoryField": categoryField,
            "categoryAxis": {
                //"startOnAxis": true,
                "axisColor": "#DADADA",
                "gridAlpha": 0.07,
                "labelRotation": 90,
                "guides": guides != undefined && guides.length > 0 ? guides : []
            },
            "export": {
                "enabled": true,
                "libs": {
                    "path": "/Content/Base/plugins/amcharts/plugins/export/libs/"
                },
                position: 'bottom-right',
                "menu": [{
                    "class": "export-main",
                    "menu": [{
                        "label": Language.CHARTS_DOWNLOAD,
                        "menu": ["PNG", "JPG", "CSV", "XLSX", "PDF"]
                    }, {
                        "label": Language.CHARTS_ANNOTATIONS,
                        "action": "draw",
                        "menu": [{
                            "class": "export-drawing",
                            "menu": [{
                                "label": Language.EDIT,
                                "menu": ["UNDO", "REDO", "CANCELAR"]
                            }, {
                                "label": Language.SAVE,
                                "format": "PNG"
                            }]
                        }]
                    }, { "label": Language.PRINT, format: 'PRINT' }]
                }]

            }
        });
        if (handleClick != undefined) {

            chart.addListener("clickGraphItem", handleClick);
            //chart.addListener("clickGraph", function (event) { alert("clicked") });
        }
    };

    var createGenericBarChart = function (targetElement, categoryField, graphs, dataset, axisTitle, handleClick, labelRotation, hasZoomHandle, guides) {
        if (graphs.length == 0)
            return;
        var graphSelected = graphs[0].id;
        var labelRot = 0;
        if (labelRotation != undefined && labelRotation) {
            labelRot = 90;
        }
        var chart = AmCharts.makeChart(targetElement,
                   {
                       "type": "serial",
                       "categoryField": categoryField,
                       "pathToImages": "/Content/Base/plugins/amcharts/images/",
                       "startDuration": 0,
                       "addClassNames": false,
                       "theme": "light",
                       "decimalSeparator": ",",
                       "precision": 2,
                       "thousandsSeparator": ".",
                       "colors": [
		                    "#E43A45",
		                    "#4B77BE",
		                    "#F4D03F",
		                    "#26C281",
		                    "#8877A9",
		                    "#F2784B",
		                    "#BFBFBF",
		                    "#E26A6A",
		                    "#2AB4C0",
		                    "#C5B96B",
		                    "#BF55EC",
		                    "#5E738B",
		                    "#D91E18",
		                    "#E87E04",
		                    "#555555"
                       ],
                       "categoryAxis": {
                           //"startOnAxis": true,
                           "axisColor": "#DADADA",
                           "gridAlpha": 0.07,
                           "labelRotation": labelRot,
                           "guides": guides != undefined && guides.length > 0 ? guides : []
                       },
                       "chartCursor": {
                           "bulletsEnabled": true
                       },
                       "zoomOutOnDataUpdate": false,
                       "chartScrollbar": {
                           //"backgroundColor": "#EFEFEF",
                           //"color": "#000000",
                           //"dragIconHeight": 32,
                           //"dragIconWidth": 37,
                           //"graph": graphSelected,
                           //"graphFillColor": "#EFEFEF",
                           //"graphLineColor": "#FFFFFF",
                           //"graphType": "column",
                           //"gridCount": 2,
                           //"selectedGraphFillAlpha": 0,
                           //"selectedGraphFillColor": "#FFFFFF",
                           //"selectedGraphLineColor": "#FFFFFF",
                           //"autoGridCount": true,
                       },
                       "mouseWheelZoomEnabled": true,
                       "trendLines": [],
                       "graphs": graphs,
                       "guides": guides != undefined && guides.length > 0 ? guides : [],
                       "allLabels": [],
                       "balloon": {},
                       "legend": {
                           "markerBorderThickness": 0,
                           "useGraphSettings": true,
                           "useMarkerColorForLabels": true,
                           "useMarkerColorForValues": true,
                           "equalWidths": true,
                           "enabled": true,
                           "maxColumns": 4,
                           "valueAlign": "left"

                       },
                       "dataProvider": dataset,
                       "export": {
                           "enabled": true,
                           "libs": {
                               "path": "/Content/Base/plugins/amcharts/plugins/export/libs/"
                           },
                           position: 'bottom-right',
                           "menu": [{
                               "class": "export-main",
                               "menu": [{
                                   "label": Language.CHARTS_DOWNLOAD,
                                   "menu": ["PNG", "JPG", "CSV", "XLSX", "PDF"]
                               }, {
                                   "label": Language.CHARTS_ANNOTATIONS,
                                   "action": "draw",
                                   "menu": [{
                                       "class": "export-drawing",
                                       "menu": [{
                                           "label": Language.EDIT,
                                           "menu": ["UNDO", "REDO", "CANCELAR"]
                                       }, {
                                           "label": Language.SAVE,
                                           "format": "PNG"
                                       }]
                                   }]
                               }, { "label": Language.PRINT, format: 'PRINT' }]
                           }]

                       },

                   }


               );
        if (handleClick != undefined) {
            chart.addListener("clickGraphItem", handleClick);
        }
        if (hasZoomHandle != undefined && hasZoomHandle != false) {

            chart.addListener("rendered", function () {
                chart.ignoreZoomEvent = true;
                chart.addListener("zoomed", zoomChart);
            });

        }

        // this method is called when chart is first inited as we listen for "rendered" event

    }

    var createMultipleAxisChart = function (targetElement, categoryField, graphs, dataset, axis, handleClick) {
        if (graphs.length == 0)
            return;

        if (IsNullOrEmpty(window.__chartsinithandler)) {
            // Hack para que os eixos do xx comecem no minimo valor  ou entao 0.
            //SRC: http://stackoverflow.com/questions/30362230/amcharts-replace-0-with-1-in-vertical-valueaxis
            AmCharts.addInitHandler(function (chart) {

                // check if there are any value axes defined
                if (typeof chart.valueAxes !== "object" || !chart.valueAxes.length)
                    return;

                // check if chart's value axis has "tentativeMinimum" set
                // For the sake of simplicity we're just gonna take the first 
                // value axis and first graph.
                // Normally we would want to check all value axes and their attached
                // graphs to check for their respective values.
                var min = 0;
                var axis = chart.valueAxes[0];
                var axis2 = null;
                if (chart.valueAxes.length > 1 && chart.valueAxes[1].position == "right") {
                    axis2 = chart.valueAxes[1];
                }

                axis.tentativeMinimum = 0;
                // if (!IsNullOrEmpty(axis2))
                //  axis.tentativeMinimum = 0;
                //axis.minimum = 0;

                for (var i = 0; i < chart.graphs.length; i++) {


                    var field = chart.graphs[i].valueField;

                    for (var x = 0; x < chart.dataProvider.length; x++) {
                        if (min === undefined || (chart.dataProvider[x][field] < min))
                            min = chart.dataProvider[x][field];
                    }

                }

                //if (min <= (axis.tentativeMinimum))
                if (min >= 0) {
                    axis.minimum = 0;
                    axis.strictMinMax = true;
                    if (axis2 != null) {
                        axis2.minimum = 0;
                        axis2.strictMinMax = true;
                    }
                }
                
                if (axis2 != null) {
                    var max = 0;
                    for (var i = 0; i < chart.graphs.length; i++) {

                        if (chart.graphs[i].valueAxis == axis2.id) {
                            var field = chart.graphs[i].valueField;

                            for (var x = 0; x < chart.dataProvider.length; x++) {
                                if (max === undefined || (parseInt(chart.dataProvider[x][field]) > max))
                                    max = parseInt(chart.dataProvider[x][field]);
                            }
                        }
                    }
                    
                    axis2.maximum = (max * 0.1) + max;
                }

                //if (axis.tentativeMinimum === undefined || axis.minimumThreshold === undefined)
                //    return;
                //axis.tentativeMinimum = 0;
                //axis.minimum = 0;
                //// get first charts valueField to check agains
                //var field = chart.graphs[0].valueField;

                //// cycle through the data

                //for (var x = 0; x < chart.dataProvider.length; x++) {
                //    if (min === undefined || (chart.dataProvider[x][field] < min))
                //        min = chart.dataProvider[x][field];
                //}

                // check if min is within the threshold
                //if (min <= (axis.tentativeMinimum ))
                //    axis.minimum = axis.tentativeMinimum;

            }, ["serial"]);

            window.__chartsinithandler = true;
        }

        var graphSelected = graphs[0].id;
        var chart = AmCharts.makeChart(targetElement, {
            "type": "serial",
            "categoryField": categoryField,// a categoria das barras (nos relatorios está sempre definido 'date')
            "pathToImages": "/Content/Base/plugins/amcharts/images/",
            "startDuration": 0,
            "addClassNames": false,
            "theme": "light",
            "decimalSeparator": ",",
            "precision": 2,
            "thousandsSeparator": ".",
            "dataProvider": dataset,
            "valueAxes": axis,
            "graphs": graphs,
            "zoomOutOnDataUpdate": false,
            "colors": [
		        "#E43A45",
		        "#4B77BE",
		        "#F4D03F",
		        "#26C281",
		        "#8877A9",
		        "#F2784B",
		        "#BFBFBF",
		        "#E26A6A",
		        "#2AB4C0",
		        "#C5B96B",
		        "#BF55EC",
		        "#5E738B",
		        "#D91E18",
		        "#E87E04",
		        "#555555"
            ],
            "chartCursor": {
                "bulletsEnabled": true
            },

            "chartScrollbar": {
                "backgroundColor": "#EFEFEF",
                "color": "#000000",
                "dragIconHeight": 32,
                "dragIconWidth": 37,
                "graph": graphSelected,
                "graphFillColor": "#EFEFEF",
                "graphLineColor": "#FFFFFF",
                "graphType": "column",
                "gridCount": 2,
                "selectedGraphFillAlpha": 0,
                "selectedGraphFillColor": "#FFFFFF",
                "selectedGraphLineColor": "#FFFFFF",
                "autoGridCount": true,
                "ignoreCustomColors": true,
                "updateOnReleaseOnly": true
            },
            "valueScrollbar": {
                "oppositeAxis": false,
                //"offset": 50,
                "scrollbarHeight": 5
            },
            "mouseWheelZoomEnabled": false,
            "legend": {
                "markerBorderThickness": 0,
                "useGraphSettings": true,
                //"useMarkerColorForLabels": true,
                //"useMarkerColorForValues": true,
                "equalWidths": true,
                "enabled": true,
                "maxColumns": 4,
                "valueAlign": "left"
            },
            "categoryField": categoryField,
            "categoryAxis": {
                //"parseDates": false,
                "minorGridEnabled": true,
                "gridPosition": "start",
                "gridThickness": 1,
            },
            "export": {
                "enabled": true,
                "libs": {
                    "path": "/Content/Base/plugins/amcharts/plugins/export/libs/"
                },
                position: 'bottom-right',
                "menu": [{
                    "class": "export-main",
                    "menu": [{
                        "label": "Download",
                        "menu": ["PNG", "JPG", "CSV", "XLSX", "PDF"]
                    }, {
                        "label": "Anotações",
                        "action": "draw",
                        "menu": [{
                            "class": "export-drawing",
                            "menu": [{
                                "label": "Editar",
                                "menu": ["UNDO", "REDO", "CANCELAR"]
                            }, {
                                "label": "Guardar",
                                "format": "PNG"
                            }]
                        }]
                    }, { "label": "Imprimir", format: 'PRINT' }]
                }]

            }
        });

        if (handleClick != undefined) {
            chart.addListener("clickGraphItem", handleClick);
        }
        // verificar se tras medias
        var hasZoom = false;
        $.each(graphs, function (i, graph) {
            if (graph.valueField == 'media') {
                hasZoom = true;
                return;
            }
        });
        if (hasZoom) {
            chart.ignoreZoomEvemt = true;
            chart.addListener("zoomed", zoomChart);
        }




    }

    function createSerialChart(targetElement, categoryField, graphs, dataSet, axisTitle, handleClick) {
        var graphSelected = graphs[0].id;

        var chart = AmCharts.makeChart(targetElement,
                   {
                       "type": "serial",
                       "categoryField": categoryField,
                       "pathToImages": "/Content/Base/plugins/amcharts/images/",
                       //"pathToImages": "http://www.amcharts.com/lib/3/plugins/images/libs/",
                       //"mouseWheelScrollEnabled": true,
                       //"mouseWheelZoomEnabled": true,
                       //"zoomOutText": "Ver Tudo",
                       "startDuration": 0,
                       "addClassNames": false,
                       "decimalSeparator": ",",
                       "precision": 2,
                       "thousandsSeparator": ".",
                       "theme": "none",
                       "colors": [
                       "#E43A45",
                       "#4B77BE",
                       "#F4D03F",
                       "#26C281",
                       "#8877A9",
                       "#F2784B",
                       "#BFBFBF",
                       "#E26A6A",
                       "#2AB4C0",
                       "#C5B96B",
                       "#BF55EC",
                       "#5E738B",
                       "#D91E18",
                       "#E87E04",
                       "#555555"
                       ],
                       "zoomOutOnDataUpdate": false,
                       "categoryAxis": {
                           "gridPosition": "start",
                           "gridThickness": 0,
                           "labelRotation": 90
                       },
                       "chartCursor": {
                           "bulletsEnabled": true
                       },
                       "chartScrollbar": {
                           "backgroundColor": "#EFEFEF",
                           "color": "#000000",
                           "dragIconHeight": 32,
                           "dragIconWidth": 37,
                           "graph": graphSelected,
                           "graphFillColor": "#EFEFEF",
                           "graphLineColor": "#FFFFFF",
                           "graphType": "line",
                           "gridCount": 2,
                           "selectedGraphFillAlpha": 0,
                           "selectedGraphFillColor": "#FFFFFF",
                           "selectedGraphLineColor": "#FFFFFF"
                       },
                       "trendLines": [],
                       "graphs": graphs,
                       "guides": [],
                       "valueAxes": [
                           {
                               "id": "ValueAxis-1",
                               "title": axisTitle + ""
                           }
                       ],
                       "allLabels": [],
                       "balloon": {},
                       "legend": {
                           "markerBorderThickness": 0,
                           "useGraphSettings": true,
                           "useMarkerColorForLabels": true,
                           "useMarkerColorForValues": true,
                           "equalWidths": false
                       },
                       "mouseWheelZoomEnabled": true,
                       "dataProvider": dataSet,
                       "export": {
                           "enabled": true,
                           "libs": {
                               "path": "/Content/Base/plugins/amcharts/plugins/export/libs/"
                           },
                           position: 'bottom-right',
                           "menu": [{
                               "class": "export-main",
                               "menu": [{
                                   "label": Language.CHARTS_DOWNLOAD,
                                   "menu": ["PNG", "JPG", "CSV", "XLSX", "PDF"]
                               }, {
                                   "label": Language.CHARTS_ANNOTATIONS,
                                   "action": "draw",
                                   "menu": [{
                                       "class": "export-drawing",
                                       "menu": [{
                                           "label": Language.EDIT,
                                           "menu": ["UNDO", "REDO", "CANCELAR"]
                                       }, {
                                           "label": Language.SAVE,
                                           "format": "PNG"
                                       }]
                                   }]
                               }, { "label": Language.PRINT, format: 'PRINT' }]
                           }]

                       },

                   }


               );
        if (handleClick == undefined) {
            chart.addListener("clickGraphItem", handleClick);
        }
        // verificar se tras medias
        var hasZoom = false;
        $.each(graphs, function (i, graph) {
            if (graph.valueField == 'media') {
                hasZoom = true;
                return;
            }
        });
        if (hasZoom) {
            chart.addListener("rendered", function () {
                chart.ignoreZoomEvent = true;
                chart.addListener("zoomed", zoomChart);
            });
        }





        //chart.legend.addListener("showItem", function (e) {
        //    // TODO resolver isto depois 
        //});

        function handleClick(event) {
            var chartId = event.chart.div.id;
            var category = event.item.category;
            var dateFormat = event.item.dataContext.dateFormat;
            var elem = new Array();

            // verificar se já existe data no array de categorias
            var arr = $.map(Reporting.BarCharts[chartId], function (cat, idx) {
                return cat.category;
            });
            var categoryExist = $.inArray(category, arr);
            var title = event.target.title;
            var clickedElm = event.target.valueField;
            if (categoryExist != -1) {
                // já existe verifica só se já existe o id do element clicado

                var a = $.map(Reporting.BarCharts[chartId][categoryExist].bars, function (bar, idx) {
                    return bar.id;
                });
                var id_exist = $.inArray(clickedElm, a);
                if (id_exist != -1) {
                    // remove e muda a cor
                    var newColor = Reporting.BarCharts[chartId][categoryExist].bars[id_exist].color;
                    event.item.columnGraphics.node.firstChild.setAttribute("stroke", newColor);
                    event.item.columnGraphics.node.firstChild.setAttribute("fill", newColor);
                    Reporting.BarCharts[chartId][categoryExist].bars.splice(id_exist, 1);
                    if (Reporting.BarCharts[chartId][categoryExist].bars.length == 0) {
                        Reporting.BarCharts[chartId].splice(categoryExist, 1);
                    }

                } else {
                    // adiciona e muda a cor para uma nova
                    var oldColor = event.item.columnGraphics.node.firstChild.getAttribute("fill");
                    var newColor = Utils.ColorLuminance(oldColor, -0.5);
                    event.item.columnGraphics.node.firstChild.setAttribute("stroke", newColor);
                    event.item.columnGraphics.node.firstChild.setAttribute("fill", newColor);

                    Reporting.BarCharts[chartId][categoryExist].bars.push({ id: clickedElm, color: oldColor, title: title });
                }
            } else {

                var oldColor = event.item.columnGraphics.node.firstChild.getAttribute("fill");
                Reporting.BarCharts[chartId].push({ category: category, bars: [{ id: clickedElm, color: oldColor, title: title }], dateFormat: dateFormat });
                var newColor = Utils.ColorLuminance(oldColor, -0.5);
                event.item.columnGraphics.node.firstChild.setAttribute("stroke", newColor);
                event.item.columnGraphics.node.firstChild.setAttribute("fill", newColor);
            }

            var drillBtn = $("#" + chartId).closest(".portlet").find(".drillChartBtn");
            if (Reporting.BarCharts[chartId].length == 0) {
                $(drillBtn).attr("disabled", true);
            } else {
                $(drillBtn).removeAttr("disabled");
            }

            var categories = $.map(Reporting.BarCharts[chartId], function (cat, idx) {
                return cat.category;
            });

            var elements = [];
            $.each(Reporting.BarCharts[chartId], function (i, elm) {
                $.map(elm.bars, function (bar, idx) {
                    if ($.inArray(bar.title, elements) == -1) {
                        elements.push(bar.title);
                        return bar.title;
                    }
                });
            });

            // criar div com os dados

            if (categories.length == 0) {
                $("#" + chartId).find(".options-selected").html("");
            } else {
                $("#" + chartId).find(".options-selected").html("<strong>" + Language.CHARTS_SELECTED_TYPES + " </strong>" + elements.join(", ") + "<br /><strong>" + Language.CHARTS_SELECTED_PERIODS + " </strong>" + categories.join(", "));
            }



        }
    }

    return {
        piechart: function (targetElem, dataset, titleField, legend, clickHandler) {
            pieChart(targetElem, dataset, titleField, legend, clickHandler);
        },
        barchart: function (targetElem, graphType, categoryField, graphs, dataSet, axisTitle, isClickable) {
            barChartCreate(targetElem, graphType, categoryField, graphs, dataSet, axisTitle, isClickable);
        },
        reportPieChartClickHandler: function (sliceIndex) {
            reportPieChartClickHandler(sliceIndex);
        },
        createGenericBarChart: function (targetElement, categoryField, graphs, dataset, axisTitle, handleClick, labelRotation, hasZoom, guides) {
            createGenericBarChart(targetElement, categoryField, graphs, dataset, axisTitle, handleClick, labelRotation, hasZoom, guides);
        },
        createMultipleAxisChart: function (targetElement, categoryField, graphs, dataset, axis, isClickable) {
            createMultipleAxisChart(targetElement, categoryField, graphs, dataset, axis, isClickable);
        },
        createNewPiechart: function (targetElement, dataset) {
            pieChartNewVersion(targetElement, dataset);
        },
        createStackChart: function (targetElement, categoryField, graphs, dataset, axisTitle, guides, handleClick) {
            createStackableChart(targetElement, categoryField, graphs, dataset, axisTitle, guides, handleClick);
        }
    };
}();




