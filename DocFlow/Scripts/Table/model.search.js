﻿var ModelSearch = function () {

    var getFormData = function () {

        var formdata = $("#bs-SingleSearchForm").serialize();
        //var date = $("#searchAdvancedForm #EventDate input").datepicker('getDate');
        //if(date!=null)
        //var autocomplete = $('#Customer').ejAutocomplete('instance');
        //if (autocomplete != null && autocomplete._selectedItems.length > 0 && !IsNullOrEmpty(autocomplete._selectedItems[0][0].Value))
        //    formdata += '&CustomerId=' + autocomplete._selectedItems[0][0].Value;
        return formdata;
    }

    var processResults = function () {

    }

    var initPageControls = function () {

    }

    return {
        init: function () {

            $("#bs-submit-btn").on('click', function () {
                Loading();
                $.ajax({
                    type: "GET",
                    url: $("#bs-SingleSearchForm").attr("action"),
                    data: ModelSearch.getData(),
                    success: function (data) {
                        $("#div-results").html(data);
                        ej.widget.init();
                        var grid = $('#FlatGrid').ejGrid('instance');
                        grid.refreshContent();
                        StopLoading();
                    },
                    error: function (error) {
                        StopLoading();
                    }
                });
            });
        },
        getData: function () {
            return getFormData();
        }
    };
}();

