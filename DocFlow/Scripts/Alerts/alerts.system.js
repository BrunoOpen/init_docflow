﻿var AlertSystem = function () {

	var checkHasAlerts = function () {
		if ($("#alertTableInfo").length) {
			//var rows = $("#alertTableInfo").find("tbody tr");
			//$.each(rows, function (i, row) {
			//	var cols = $(row).find('td');
			//	var id = $(cols[0]).data("id");
			//	AlertSystem.table[id] = { action: $(cols[2]).find("select").val(), note: $(cols[2]).find("input").val() }

			//});
			AlertSystem.table = getTableValues();
		}
	};

	var getTableValues = function () {
		var rows = $("#alertTableInfo").find("tbody tr");
		var table = [];
		$.each(rows, function (i, row) {
			var cols = $(row).find('td');
			var id = $(cols[0]).data("id");
			table.push({ id: id, action: cols.filter(".actionsCol").find("select").val(), note: cols.filter(".notesCol").find("input").val() });
		}); 
		return table;
	};

	var saveAlerts = function () {
		$(document).on("click", "#saveAlertBtn", function () {
			var tableResults = getTableValues();
			
			// compare data
			var saveModel = [];
			for (var i = 0; i < tableResults.length; i++) {
				if (tableResults[i]["action"] != AlertSystem.table[i]["action"] || tableResults[i]["note"] != AlertSystem.table[i]["note"]) {
					saveModel.push({ AlertId: tableResults[i]["id"], ActionId: tableResults[i]["action"], Note: tableResults[i]["note"] });
				}
			}
			debugger;
			$.ajax({
				url: $(this).data("action"),
				type: 'post',
				data: {
					saveModel: saveModel,
					id: AlertSystem.Id
				},
				beforeSend: function () {
				    Loading();
				},
				success: function (data) {
				    $("#alertContainer").html(data);
				    Geral.bsDropDowns();
				    AlertSystem.table = new Object();
				    checkHasAlerts();
				}
			}).done(function (data) { StopLoading(); });
		});
	};

	var getAlertPanel = function (callback) {
	    $.get("/Alert/GetAlertPanel", function (data) {
	        $("#alertContainer").html(data);
	    }).done(callback);
	};

	var executeRecalcAlert = function (dataType) {
	    $.ajax({
	        url: '/Alert/RecalcAlert',
	        type: 'post',
	        data: {
	            typeData: AlertSystem.dataType,
	            id: AlertSystem.Id
	        },
	        beforeSend: function () {
	            // mudar o titulo
	            $("#alertTitle").html("Alertas em re-calculo...");
	            //ativar loading
	            LoadingElement($("#alertPanelBox"), true);
	            //bloquear botao save
	            $("#saveAlertBtn").attr("disabled", "disabled");
	        },
	        success: function (data) {
	            if (data.hasOwnProperty('msg')) {
	                var successPanel = $("<div class=\"alert alert-success\"><strong>" + data.msg + "</strong></div>");
	                $("#alertContainer").html("");
	                $("#alertContainer").append(successPanel);
	            } else {
	                $("#alertContainer").html(data);
	                Geral.bsDropDowns();
	            }
	            
	        }
	    }).done(function () {
	        LoadingElement($("#alertPanelBox"), false);
	        $("#saveAlertBtn").removeAttr("disabled");
	    });
	}
	var recalcAlerts = function (id) {
	    $(document).on("click", "#recalcAlert", function () {
	        var dataType = $(this).data("type");
	        AlertSystem.dataType = dataType;
	        var title = $("#alertTitle").html();
	        if (!$("#alertBox").length) {
	            getAlertPanel(executeRecalcAlert);
	        } else {
	            executeRecalcAlert(dataType)
	        }
	        
	        
	    });
	};
    
	return {
		init: function (id) {
			AlertSystem.table = new Object();
			checkHasAlerts();
			saveAlerts();
			AlertSystem.Id = id;
			AlertSystem.dataType = 0;
			recalcAlerts(id);
		}
	}
}();