﻿var ServicesSearch = function () {

    var getFormData = function () {

        var formdata = $("#bs-SingleSearchForm").serialize();
        return formdata;
    }

    //var processResults = function () {

    //}

    //var initPageControls = function () {

    //}

    //var initSearch = function () {
    //    if (window.InitSearch == 1)
    //        $("#bs-submit-btn").trigger('click');
    //}

    return {
        init: function () {

            $("#bs-submit-btn").on('click', function () {
                Loading();
                $.ajax({
                    type: "GET",
                    url: $("#bs-SingleSearchForm").attr("action"),
                    data: ServicesSearch.getData(),
                    success: function (data) {
                        $("#div-results").html(data);
                        ej.widget.init();
                        StopLoading();
                    },
                    error: function (error) {
                        StopLoading();
                    }
                });
            });

            initSearch();
        },
        getData: function () {
            return getFormData();
        }
    };
}();
