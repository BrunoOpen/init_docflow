﻿var ServicesNew = function () {

    var getFormData = function () {

        var formdata = $("#bs-SingleSearchForm").serialize();
        return formdata;
    }

    return {
        init: function () {

            $("#bs-submit-btn").on('click', function () {
                Loading();
                $.ajax({
                    type: "POST",
                    url: $("#bs-SingleSearchForm").attr("action"),
                    data: ServicesNew.getData(),
                    success: function (data) {
                        $("#Id").val(data);
                        StopLoading();
                    },
                    error: function (error) {
                        StopLoading();
                    }
                });
            });

            initSearch();
        },
        getData: function () {
            return getFormData();
        }
    };
}();
