﻿
function seleccustomer(args) {
    // debugger;
    Loading();

    if (IsNullOrEmpty(args.data))
        args.data = args.item;
    $("#CustomerId").val(args.data.ID);
    $("#CustomerCode").val(args.data.Code);
    $("#CustomerName").val(args.data.Name);
    $("#Brand").val(args.data.Brand);
    $("#CustomerContactName").val(args.data.ContactName);
    $("#CustomerPhone").val(args.data.Phone);
    $("#CustomerMobile").val(args.data.MobilePhone);
    $("#CustomerEmail").val(args.data.Email);
    $("#CustomerVatNumber").val(args.data.FiscalNumber);
    $("#Commercial_Value").selectpicker('val',args.data.CommercialId);
   


    var address = '';
    $("#form-customer-address #CustomerAddress1").val(args.data.Address);
    address += !IsNullOrEmpty(args.data.Address) ? (args.data.Address + '<br/>') : "";
    $("#form-customer-address #CustomerAddress2").val(args.data.Locality);
    address += !IsNullOrEmpty(args.data.Locality) ? (args.data.Locality + '<br/>') : "";
    $("#form-customer-address #CustomerZipCode").val(args.data.ZipCode);
    address += !IsNullOrEmpty(args.data.ZipCode) ? (args.data.ZipCode + ' ') : "";
    $("#form-customer-address #CustomerZipCodeDescription").val(args.data.ZipCodeDesc);
    address += !IsNullOrEmpty(args.data.ZipCodeDesc) ? args.data.ZipCodeDesc : "";


    var $pc = $("#PaymentConditions_Value");
    if ($pc.length > 0 && $pc.val() == "" && !IsNullOrEmpty(args.data.PaymentConditionId)) {
        $pc.val(args.data.PaymentConditionId).selectpicker('refresh');
    }

    var $cz = $("#CustomerZone_Value");
    if ($cz.length > 0 && $cz.val() == "" && !IsNullOrEmpty(args.data.ZoneId)) {
        $pc.val(args.data.ZoneId).selectpicker('refresh');
    }

    //var $cd = $("#CustomerCode_display");
    //if ($cd.length > 0) {
    //    $cd.html("<a target='_blank' ></a>")
    //}


    $("#address-box").html(address);
    $("#modal-customer-select").modal('hide');

    if ($("#CustomerBrands_Value").length > 0) {
        $.ajax({
            type: "GET",
            url: '/Customer/GetBrands',
            data: { Id: args.data.ID },
            success: function (data2) {
                if (data2 != null) {
                    var $select = $("#CustomerBrands_Value");
                    $select.empty();
                    $select.append('<option value=""></option>');
                    for (var i = 0; i < data2.length; i++) {
                        $select.append('<option value="' + data2[i].Value + '">' + data2[i].Text + '</option>');
                    }
                    $select.selectpicker('refresh');
                }
                StopLoading();
            },
            error: function (error) {
                StopLoading();
            }
        });
    } else {
        StopLoading();
    }


}

(function () {

    $search = $(".customer-code-search");
    $modal = $("#modal-customer-select");

    //customer 
    if ($search.length > 0) {
        $search.closest("div.input-group").find("#CustomerCode").on('keypress', function (e) {
            if (e.which == 13) {
                $(this).trigger('blur');
            }
        })
        $search.closest("div.input-group").find("#CustomerCode").on('blur', function () {

            if ($(this).val() != $("#CustomerId").data("code")) {
                Loading();
                $.ajax({
                    type: "GET",
                    url: '/Base/GetCustomerByCode',
                    data: { code: $(this).val() },
                    success: function (data) {

                        if (data != null) {
                            $("#CustomerId").val(data.ID);
                            $("#CustomerId").data("code", data.Code)
                            $("#CustomerCode").val(data.Code);
                            $("#CustomerName").val(data.Name);
                            $("#Brand").val(data.Brand);
                            $("#CustomerContactName").val(data.ContactName);
                            $("#CustomerPhone").val(data.Phone);
                            $("#CustomerMobile").val(data.MobilePhone);
                            $("#CustomerEmail").val(data.Email);
                            $("#CustomerVatNumber").val(data.FiscalNumber);

                            var $pc = $("#PaymentConditions_Value");
                            if ($pc.length > 0 && $pc.val() == "" && !IsNullOrEmpty(data.PaymentConditionId)) {
                                $pc.val(data.PaymentConditionId).selectpicker('refresh');
                            }

                            var $cz = $("#CustomerZone_Value");
                            if ($cz.length > 0 && $cz.val() == "" && !IsNullOrEmpty(data.ZoneId)) {
                                $pc.val(data.ZoneId).selectpicker('refresh');
                            }

                            var address = '';
                            $("#form-customer-address #CustomerAddress1").val(data.Address);
                            address += !IsNullOrEmpty(data.Address) ? (data.Address + '<br/>') : "";
                            $("#form-customer-address #CustomerAddress2").val(data.Locality);
                            address += !IsNullOrEmpty(data.Locality) ? (data.Locality + '<br/>') : "";
                            $("#form-customer-address #CustomerZipCode").val(data.ZipCode);
                            address += !IsNullOrEmpty(data.ZipCode) ? (data.ZipCode + ' ') : "";
                            $("#form-customer-address #CustomerZipCodeDescription").val(data.ZipCodeDesc);
                            address += !IsNullOrEmpty(data.ZipCodeDesc) ? data.ZipCodeDesc : "";

                            $("#address-box").html(address);

                            //start
                            if ($("#CustomerBrands_Value").length > 0) {
                                $.ajax({
                                    type: "GET",
                                    url: '/Customer/GetBrands',
                                    data: { id: data.ID },
                                    success: function (data2) {
                                        if (data2 != null) {
                                            var $select = $("#CustomerBrands_Value");
                                            $select.empty();
                                            for (var i = 0; i < data2.length; i++) {
                                                $select.append('<option value="' + data2[i].Value + '">' + data2[i].Text + '</option>');
                                            }
                                            $select.selectpicker('refresh');
                                        }
                                        StopLoading();
                                    },
                                    error: function (error) {
                                        StopLoading();
                                    }
                                });
                            }
                            else {
                                StopLoading();
                            }
                            //end
                        }
                        else {
                            //clean
                            $("#CustomerId").val('');
                            $("#CustomerId").data("code", null)
                           // $("#CustomerCode").val('');
                            $("#CustomerName").val('');
                            $("#Brand").val('');
                            $("#CustomerContactName").val('');
                            $("#CustomerPhone").val('');
                            $("#CustomerMobile").val('');
                            $("#CustomerEmail").val('');
                            $("#CustomerVatNumber").val('');


                            $("#form-customer-address #CustomerAddress1").val('');
                            $("#form-customer-address #CustomerAddress2").val('');
                            $("#form-customer-address #CustomerZipCode").val('');
                            $("#form-customer-address #CustomerZipCodeDescription").val('');
                            $("#address-box").html('');
                            var $select = $("#CustomerBrands_Value");
                            $select.empty();
                            $select.selectpicker('refresh');

                            StopLoading();
                        }

                    },
                    error: function (error) {
                        StopLoading();
                    }
                });
            }
        });


        if ($modal.length > 0) {
            $search.on('click', function () {

                Loading();
                $.ajax({
                    type: "GET",
                    url: '/Base/GetCustomersGrid',

                    success: function (data) {

                        $("#gridCustomer").ejGrid("dataSource", data);
                        //var grid = $('#gridCustomer').ejGrid('instance');
                        //grid.model.dataSource.push(data);
                        //grid.refreshContent();
                        $modal.modal('show');
                        StopLoading();
                    },
                    error: function (error) {
                        StopLoading();
                    }
                });


                //$("#form-customer-address #CustomerAddress1").val($c_temp.CustomerAddress1);
                //$("#form-customer-address #CustomerAddress2").val($c_temp.CustomerAddress2);
                //$("#form-customer-address #CustomerZipCode").val($c_temp.CustomerZipCode);
                //$("#form-customer-address #CustomerZipCodeDescription").val($c_temp.CustomerZipCodeDescription);
                //$c_modal.modal('hide');
            });
        }
    }



}());


function selectLocal(args) {
    // debugger;
    Loading();

    if (IsNullOrEmpty(args.data))
        args.data = args.item;
  
    $("#Company").val(args.data.Company);
    $("#Zone_Value").val(args.data.ZoneID).selectpicker('refresh');
    $("#LocalContact").val(args.data.LocalContact);
    $("#LocalPhone").val(args.data.LocalPhone);
    $("#LocalMobile").val(args.data.LocalMobile);
    $("#LocalEmail").val(args.data.LocalEmail);




    var address = '';
    $("#form-local-address #LocalAddress1").val(args.data.LocalAddress1);
    address += !IsNullOrEmpty(args.data.LocalAddress1) ? (args.data.LocalAddress1 + '<br/>') : "";
    $("#form-local-address #LocalAddress2").val(args.data.LocalAddress2);
    address += !IsNullOrEmpty(args.data.LocalAddress2) ? (args.data.LocalAddress2 + '<br/>') : "";
    $("#form-local-address #LocalZipCode").val(args.data.LocalZipCode);
    address += !IsNullOrEmpty(args.data.LocalZipCode) ? (args.data.LocalZipCode + ' ') : "";
    $("#form-local-address #LocalZipCodeDescription").val(args.data.LocalZipCodeDescription);
    address += !IsNullOrEmpty(args.data.LocalZipCodeDescription) ? args.data.LocalZipCodeDescription : "";

    $("#address-box-local").html(address);
   
    StopLoading();
}