﻿var Search = function () {

    var _initSubmit = function () {
        $(Search.SubmitBtn + "," + Search.FormSingleName + " " + Search.QuickSearchParamsContainer + " " + Search.QuickSearchSubmitBtn).on('click', _submit);
        $(document).on('keypress', function (event) {
            if (event.which == 13)
                _submit(event);
        });
    };

    var _submit = function () {
        Alert.dismissAll();
        Loading();
        if (!Search.IsAdvancedSearch) {
            var formdata = Search.GetSingleData();
            $.ajax({
                type: "POST",
                url: $(Search.FormSingleName).attr("action"),
                data: formdata,
                success: function (data) {
                    if (data.hasOwnProperty("Status") && data.Status == false) {
                        Alert.Errors(data.Errors, 0);
                        StopLoading();
                        return false;
                    } else {
                        $(".ol-hide").show();
                        Utils.openPortlet("#search-results-pnl");
                        Utils.closePortlet("#search-inputs-pnl");
                        Search.ProcessResults(data);
                    }
                    StopLoading();
                },
                error: function (error) {
                    Alert.Errors(error, 0);
                }
            });
        }
        else {
            var formdata = Search.GetAdvancedData();
            $.ajax({
                type: "POST",
                url: $(Search.FormAdvancedName).attr("action"),
                data: formdata,
                success: function (data) {
                    if (data.hasOwnProperty("Status") && data.Status == false) {
                        Alert.Errors(data.Errors, 0);
                        StopLoading();
                        return false;
                    } else {
                        $(".ol-hide").show();
                        Utils.openPortlet("#search-results-pnl");
                        Utils.closePortlet("#search-inputs-pnl");
                        Search.ProcessResults(data);
                    }
                    StopLoading();
                },
                error: function (error) {
                    Alert.Errors(error, 0);
                }
            });
        }
        Utils.StopEvent(event);
        return false;
    };

    var _initClean = function () {
        $(Search.CleanBtn).on('click', function () {
            if ($(Search.FormAdvancedName + " .bs-select").length > 0)
                $(Search.FormAdvancedName + " .bs-select").val('').trigger('change');

            if ($(Search.FormAdvancedName + ".bs-select[multiple]").length > 0)
                $(Search.FormAdvancedName + ".bs-select[multiple]").parent().find(".selected").removeClass("selected");

            $(Search.FormAdvancedName + " input").val('');

            if ($(Search.FormAdvancedName + " input[type=checkbox]").length > 0)
                $.uniform.update($(Search.FormAdvancedName + " input[type=checkbox]").attr("checked", false));

            if (!IsNullOrEmpty(Search.CleanCallBack))
                Search.CleanCallBack();
        });
    }

    var _initQuickSearch = function () {

        $(Search.FormSingleName + " " + Search.QuickSearchAdvancedSearchToggle).on('click', function () {
            $(Search.FormSingleName + " " + Search.QuickSearchParamsContainer).slideUp();
            $(Search.FormAdvancedName + " " + Search.AdvancedSearchParamsContainer).slideDown();
            Search.IsAdvancedSearch = true;
        });

        $(Search.FormAdvancedName + " " + Search.AdvancedSearchQuickSearchToggle).on('click', function () {
            Search.IsAdvancedSearch = false;
            $(Search.FormSingleName + " " + Search.QuickSearchParamsContainer).slideDown();
            $(Search.FormAdvancedName + " " + Search.AdvancedSearchParamsContainer).slideUp();
            //$(Search.FormAdvancedName + " #quicksearch").focus();
        });

    }

    return {

        init: function (formSingleId, formAdvancedId, getSingleData, getAdvancedData, processresults, clean) {
            Search.FormSingleName = formSingleId;
            Search.FormAdvancedName = formAdvancedId;
            Search.GetSingleData = getSingleData;
            Search.GetAdvancedData = getAdvancedData;
            Search.ProcessResults = processresults;
            Search.CleanCallBack = clean;
            Search.IsAdvancedSearch = false;

            Search.QuickSearchParamsContainer = "#bs-quicksearch-params-container"; //#search-header
            Search.QuickSearchSubmitBtn = "#bs-quick-search-submit-btn"; // btnSingleSubmit
            Search.QuickSearchAdvancedSearchToggle = "#bs-quick-search-toggle"; // #advanced-search
            Search.AdvancedSearchParamsContainer = "#bs-advanced-search-params-container"; // ol-search-body
            Search.SubmitBtn = "#bs-submit-btn"; // btnSubmitSearch
            Search.CleanBtn = "#bs-clean-btn"; //btnClean

            Search.SearchResultsContainer = "#bs-search-results-container";//search-results-pnl
            Search.AdvancedSearchQuickSearchToggle = "#bs-advanced-search-toggle"; // #advanced-search

            _initSubmit();
            _initClean();
            _initQuickSearch();
        }
    };
}();

