﻿var Profile = function () {

    var _init = function () {
        $("#edit-data").on('click', function () {
            $(this).hide();
            $(".ol-show").hide();
            $(".ol-hide").show();
        });


        $("#btnSave").on('click', function () {
            Loading();

            $("#personal-data").submit();
            //$.ajax({
            //    type: "POST",
            //    url: "/Profile/SavePersonalData",
            //    data: { Name: $("#Name.ol-hide").val() },
            //    success: function () {
            //        window.location = "../Profile/PersonalData";
            //    },
            //    error: function () {
            //        StopLoading();
            //    }
            //});
        });


        $("#btnPwdChange").on('click', function () {
            $("#personal-data").hide();
            $("#password-change").show();
        });

        $("#btnCancelPwd").on('click', function () {
            $("#password-change input").val('');
            $("#personal-data").show();
            $("#password-change").hide();
            $("#err-msg").hide();
        });

        $("#changePic").on('click', function () {
            $("#file").trigger('click');
        });

        $("#file").on('change', function () {
            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#avatar').attr('src', e.target.result);
                }

                reader.readAsDataURL(this.files[0]);
            }
        });

        $("#btnSavePwd").on('click', function (e) {



            //WARNING !! WARNING !!
            // Se alterar o mecanismo de pass, alterar tambem no resetpassword.js (form de alteracao de password)
            var message = '';


            if ($("#PasswordOriginal").val() < 1 || $("#NewPassword").val() < 1 || $("#PasswordConfirm").val() < 1) {
                message = '<div class="alert alert-danger">  <strong>' + Language.ACCOUNTS_RESET_PASSWORD_ERRORDATA + '</strong><br>' + Language.ACCOUNTS_MESSAGE_ALL_FIELDS_REQUIRED + '</div>';
            }
            else {


                var symbol = new RegExp(/[\@\#\$\%\^\&\*\(\)\_\+\!]/);
                var lowerCase = new RegExp(/[a-z]/);
                var upperCase = new RegExp(/[A-Z]/);
                var digits = new RegExp(/[0-9]/);
                var minLength = 6;
                var value =$("#NewPassword").val();
                if (value.length < minLength || !symbol.test(value) || !lowerCase.test(value) || !upperCase.test(value) || !digits.test(value))
                    var message = ' <div class="alert alert-danger">  <strong>' + Language.ACCOUNTS_MESSAGE_PASSWORD_INVALID + '</strong><br>' + Language.ACCOUNTS_MESSAGES_NEW_PASSWORD_RULES + ' </div>';

                if ($("#NewPassword").val() != $("#PasswordConfirm").val())
                    var message = ' <div class="alert alert-danger">  <strong>' + Language.ACCOUNTS_MESSAGE_PASSWORD_INVALID + '</strong><p></p>'+Language.ACCOUNTS_MESSAGE_PASSWORDS_DONT_MATCH+' </div>';

            }

            if (message.length > 0) {
                $("#err-msg").html(message).show();
                e.preventDefault();
                if (e.stopPropagation) {
                    e.stopPropagation();
                }
                else if (window.event) {
                    window.event.cancelBubble = true;
                }
                return false;

            }
            else {
                $("#err-msg").hide();

                $.ajax({
                    type: "POST",
                    url: "/Users/ChangePassword",
                    data: { oldPassword: $("#PasswordOriginal").val(), newPassword: $("#NewPassword").val(), ConfirmPassword: $("#PasswordConfirm").val(),oldPassword: $("#OldPassword").val() },
                    success: function (message) {
                        if (message == 'success')
                            window.location = "/Users/PersonalData";
                        else {
                            var msg = '';
                            $.each(message, function (i, val) {
                                msg = msg + val + '<br>';
                            });
                            $("#err-msg").html('<div class="alert alert-danger">  <strong>' + Language.ACCOUNTS_MESSAGE_PASSWORD_INVALID + '</strong><br>' + msg + '</div>').show();
                        }
                    },
                    error: function () {
                        StopLoading();
                    }
                });
            }

        });
    }




    return {

        //main function to initiate the moduledata
        init: function () {


            _init();

        }

    };

}();