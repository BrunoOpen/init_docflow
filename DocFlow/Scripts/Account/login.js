﻿var Login = function () {

    var _init = function () {
        $("form").on('submit', function (e) {
            var valid = true;
            var message = '';
            var header = '';
            if ($(this).find("#Email").val().length < 1 || $(this).find("#Password").val().length < 1) {
                valid = false;
                header = "Dados inválidos!";
                message = "Introduza o Utilizador e a Senha";
            }

            if (valid == true && ($(this).find("#Email").val().length < 3 || $(this).find("#Password").val().length < 3)) {
                valid = false;
                header = "Dados inválidos!";
                message = "Utilizador ou Password inválidos";
            }

            if (!valid) {
                $("#err-msg").remove();
                $("#append-div").after('<div class="form-group" id="err-msg"> <div class="alert alert-danger"> <strong>' + header + '</strong><p></p>' + message + ' </div> </div>');

                e.preventDefault();
                if (e.stopPropagation) {
                    e.stopPropagation();
                }
                else if (window.event) {
                    window.event.cancelBubble = true;
                }
                return false;
            }
        });
    }

    return {
        init: function () {
            _init();
            $("#RememberMe").uniform();
        }
    };
}();

