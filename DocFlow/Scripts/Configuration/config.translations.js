﻿var Config = function () {

    this.CreateTableWithColumns = function (name, columns, order, columnDefs) {
        var mytable = $('#' + name).DataTable({

            "language": Language.DATATABLE_LANGUAGE,
            columns: columns,
            "columnDefs": columnDefs,
            "order": [order],
            "lengthMenu": [
                [5, 10, 15, 20, 40, 50, 100, -1],
                [5, 10, 15, 20, 40, 50, 100, Language.DATATABLE_ALL] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            buttons: {
                dom: {
                    button: { tag: 'li' },
                    container: { tag: 'ul', className: 'dropdown-menu pull-right' },

                    buttonLiner: {
                        tag: 'a' //a volta do texto
                    }
                },
                buttons: [
                        { extend: 'excelHtml5', text: ' <i class="fa fa-file-excel-o"></i> ' + Language.EXPORT_EXCEL },
                        { extend: 'pdfHtml5', text: '  <i class="fa  fa-file-pdf-o"></i></i> ' + Language.EXPORT_PDF },
                        { extend: 'print', text: ' <i class="fa fa-print"></i> ' + Language.PRINT },
                ]
            }
        });

        mytable.buttons().container().appendTo($('#export-' + name));
        return mytable;
    }

    this.CreateTableOneColumn = function (name, index, canEdit) {
        var columnDefs = [{ "width": "40%", "targets": [0, 1] }, { "orderable": false, "targets": [2] }, {
            "targets": 2,
            "data": null,
            "defaultContent": '<a   data-toggle="modal" href="#modal-translations-1"  class="btn default btn-xs red-flamingo-stripe edit"  data-index="' + index + '">  <i class="fa fa-plus"></i> '+Language.EDIT+' </a>'
        }];

        if (!canEdit)
            columnDefs = [];




        var mytable = $('#' + name).DataTable({

            "language": Language.DATATABLE_LANGUAGE,
            columns: [{ data: 'Name' }, { data: 'TranslatedName' }],
            columnDefs: columnDefs,
            "order": [[0, 'asc']],
            "lengthMenu": [
                [5, 10, 15, 20, 40, 50, 100, -1],
                [5, 10, 15, 20, 40, 50, 100, Language.DATATABLE_ALL] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            buttons: {
                dom: {
                    button: { tag: 'li' },
                    container: { tag: 'ul', className: 'dropdown-menu pull-right' },

                    buttonLiner: {
                        tag: 'a' //a volta do texto
                    }
                },
                buttons: [
                        { extend: 'excelHtml5', text: ' <i class="fa fa-file-excel-o"></i> ' + Language.EXPORT_EXCEL },
                        { extend: 'pdfHtml5', text: '  <i class="fa  fa-file-pdf-o"></i></i> ' + Language.EXPORT_PDF },
                        { extend: 'print', text: ' <i class="fa fa-print"></i> ' + Language.PRINT },
                ]
            }
        });

        mytable.buttons().container().appendTo($('#export-' + name));
        return mytable;
    }

    this.CreateTableTwoColumns = function (name, index, canEdit) {
        var columnsdefs = [{ "orderable": false, "targets": [2] }, {
            "targets": 4,
            "data": null,
            "defaultContent": '<a   data-toggle="modal" href="#modal-translations-2"  class="btn default btn-xs red-flamingo-stripe edit" data-index="' + index + '">  <i class="fa fa-plus"></i> '+Language.EDIT+' </a>'
        }]
        ;
        if (!canEdit)
            columnsdefs = [];


        var mytable = $('#' + name).DataTable({

            "language": Language.DATATABLE_LANGUAGE,
            columns: [{ data: 'Name' }, { data: 'Description' }, { data: 'TranslatedName' }, { data: 'TranslatedDescription' }],
            "columnDefs": columnsdefs,
            "order": [[1, 'asc']],
            "lengthMenu": [
                [5, 10, 15, 20, 40, 50, 100, -1],
                [5, 10, 15, 20, 40, 50, 100, Language.DATATABLE_ALL] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            buttons: {
                dom: {
                    button: { tag: 'li' },
                    container: { tag: 'ul', className: 'dropdown-menu pull-right' },

                    buttonLiner: {
                        tag: 'a' //a volta do texto
                    }
                },
                buttons: [
                       { extend: 'excelHtml5', text: ' <i class="fa fa-file-excel-o"></i> ' + Language.EXPORT_EXCEL },
                        { extend: 'pdfHtml5', text: '  <i class="fa  fa-file-pdf-o"></i></i> ' + Language.EXPORT_PDF },
                        { extend: 'print', text: ' <i class="fa fa-print"></i> ' + Language.PRINT },
                ]
            }
        });

        mytable.buttons().container().appendTo($('#export-' + name));
        return mytable;
    }

    this.InitTranslationsEdition = function () {

        for (var i = 0; i < Config.Tables.length; i++) {
            if (Config.Tables[i].OneColum == true) {

                Config.Tables[i].Table.on('click', '.edit', function (e) {
                    // Config.Tables[Number($(this).data('index'))];
                    Config.EditTable = Config.Tables[Number($(this).data('index'))];
                    Config.Edit = true;
                    Config.EditRow = Config.Tables[Number($(this).data('index'))].Table.row($(this).parents('tr'));
                    Config.EditData = Config.EditRow.data();
                    $("#translarions-name-1-default").val(Config.EditData.Name);
                    //  $("#translarions-desc-2-default").val(Config.EditData.Description);
                    $("#translarions-name-1-translated").val(Config.EditData.TranslatedName);
                    //  $("#translarions-desc-2-translated").val(Config.EditData.TranslatedDescription);
                });
            }
            else {
                Config.Tables[i].Table.on('click', '.edit', function (e) {
                    Config.Edit = true;
                    Config.EditTable = Config.Tables[Number($(this).data('index'))];
                    Config.EditRow = Config.Tables[Number($(this).data('index'))].Table.row($(this).parents('tr'));
                    Config.EditData = Config.EditRow.data();
                    $("#translarions-name-2-default").val(Config.EditData.Name);
                    $("#translarions-desc-2-default").val(Config.EditData.Description);
                    $("#translarions-name-2-translated").val(Config.EditData.TranslatedName);
                    $("#translarions-desc-2-translated").val(Config.EditData.TranslatedDescription);
                });
            }
        }

        $('#btn-translations-2-submit').on('click', function (e) {
            $.ajax({
                url: Config.EditTable.SavePath,
                data: {
                    Name: $("#translarions-name-2-default").val(),
                    Description: $("#translarions-desc-2-default").val(),
                    TranslatedName: $("#translarions-name-2-translated").val(),
                    TranslatedDescription: $("#translarions-desc-2-translated").val(),
                    LanguageId: Config.CurrentLanguage,
                    EntityId: Config.EditData.EntityId
                },
                type: "POST",
                dataType: "json",
                success: function (response) {

                    if (Config.Edit) {
                        Config.EditRow.data(response);
                        Config.EditRow.invalidate();
                    }
                    $('#modal-translations-2').modal('hide');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('error');
                }
            });
        });

        $('#btn-translations-1-submit').on('click', function (e) {
            $.ajax({
                url: Config.EditTable.SavePath,
                data: {
                    TranslatedName: $("#translarions-name-1-translated").val(),
                    Name: $("#translarions-name-1-default").val(),
                    LanguageId: Config.CurrentLanguage,
                    EntityId: Config.EditData.EntityId
                },
                type: "POST",
                dataType: "json",
                success: function (response) {

                    if (Config.Edit) {
                        Config.EditRow.data(response);
                        Config.EditRow.invalidate();
                    }
                    $('#modal-translations-1').modal('hide');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('error');
                }
            });
        });

    }

    this.UpdateLanguage = function (langid, active, callback) {
        Loading();
        $.ajax({
            url: "/Configuration/UpdateLanguage",
            data: { langId: langid, active: active },
            type: "POST",
            dataType: "json",
            success: callback,
            error: function (jqXHR, textStatus, errorThrown) {
                alert('error');
                StopLoading();
            },
            complete: function () { StopLoading(); }
        });
    }

    this.InitLanguagesEdition = function () {


        Config.CurrentLanguage = $("#default-language").data('id');
        Config.Languages.on('click', '.delete', function (e) {
            Config.Languages.Row = Config.Languages.row($(this).parents('tr'));
            UpdateLanguage(Config.Languages.Row.data().Id, false);
            Config.Languages.Row.remove().sort().draw();
        });

        Config.Languages.on('click', '.translate', function (e) {
            Config.Languages.Row = Config.Languages.row($(this).parents('tr'));
            Config.CurrentLanguage = Config.Languages.Row.data().Id;
            $(".currentLanguage").attr('src', '/assets/global/img/flags/' + Config.Languages.Row.data().Image);
            $("#pnl-translations").show();


            ReloadTranslations();
        });


        $('#btn-languages-submit').on('click', function (e) {

            UpdateLanguage($("#languages-select option:selected").data('id'), true, ReloadLanguages);
            $('#modal-languages').modal('hide');
        });
    }

    this.ReloadLanguages = function () {

        Loading();
        $.ajax({
            url: "/Configuration/GetActiveLanguages",
            type: "GET",
            dataType: "json",
            success: function (response) {
                $.each(response, function (i, val) {
                    val.FlagAndName = '<img class="flag" src="../../assets/global/img/flags/' + val.Image + '" alt=""> ' + val.Name;
                    val.Actions = !val.Default? '<a  href="#" class="btn default btn-xs red-flamingo-stripe delete" >  <i class="fa fa-plus"></i>' + Language.REMOVE + ' </a> <a  href="#" class="btn default btn-xs red-flamingo-stripe translate" >  <i class="fa fa-plus"></i>' + Language.TRANSLATE + ' </a>':"";
                });
                Config.Languages.clear().rows.add(response).columns.adjust().sort().draw();

            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('error');
                StopLoading();
            },
            complete: function () { StopLoading(); }
        });
    }

    this.ReloadTranslations = function () {
        Loading();
        for (var i = 0; i < Config.Tables.length; i++) {
            if (Config.Tables[i].Path != '')
                $.ajax({
                    url: Config.Tables[i].Path,
                    type: "GET",
                    data: { langId: Config.CurrentLanguage, index: i },
                    dataType: "json",
                    success: function (response) {
                        Config.Tables[Utils.GetQueryStringParam2('index', this.url)].Table.clear().rows.add(response).columns.adjust().sort().draw();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert('error');
                    },
                    complete: function () { StopLoading(); }
                });
        }
    }

    return {
        init: function () {
            // Cria Tabela de Linguas

            Config.Languages = CreateTableWithColumns("languages",
                [{ data: 'FlagAndName' }, { data: 'Actions' }],
                [1, 'asc'],
                [{ "width": "70%", "targets": [0] }, { "orderable": false, "targets": [1] }, {
                    "targets": 1,
                    "data": null,
                    //"defaultContent":'<a  href="#" class="btn default btn-xs red-flamingo-stripe delete" >  <i class="fa fa-plus"></i>' + Language.REMOVE + ' </a> <a  href="#" class="btn default btn-xs red-flamingo-stripe translate" >  <i class="fa fa-plus"></i>' + Language.TRANSLATE + ' </a>'
                }]);

            var canEdit = false;
            if ($("#canEdit").data("id") == "True")
                canEdit = true;
            // Cria todas as tabelas de traducoes
            for (var i = 0; i < Config.Tables.length; i++) {
                if (Config.Tables[i].OneColum == true) {
                    Config.Tables[i].Table = CreateTableOneColumn(Config.Tables[i].Name, i, canEdit);
                }
                else {
                    Config.Tables[i].Table = CreateTableTwoColumns(Config.Tables[i].Name, i, canEdit);
                }
            }

            // Init Edicao das linguas
            InitLanguagesEdition();

            // Init Edicao das Traducoes
            InitTranslationsEdition();

            ReloadLanguages();
            ReloadTranslations();

        }
    };
}();


Config.Tables = [
                    { Name: 'vats', OneColum: true, tblName: 'TableVats', Path: '/Configuration/GetVatTypeTranslations', SavePath: '/Configuration/TranslateVatType', Table: null },
                    { Name: 'readingtypes', OneColum: true, tblName: 'TableReadings', Path: '/Configuration/GetReadingTypeTranslations', SavePath: '/Configuration/TranslateReadingType', Table: null },
                    { Name: 'services', OneColum: true, tblName: 'TableServices', Path: '/Configuration/GetServiceTranslations', SavePath: '/Configuration/TranslateService', Table: null },
                    { Name: 'menus', OneColum: false, tblName: 'TableMenus', Path: '/Configuration/GetMenuTranslations', SavePath: '/Configuration/TranslateMenu', Table: null },
                    { Name: 'localtypes', OneColum: true, tblName: 'TableLocTypes', Path: '/Configuration/GetLocalTypesTranslations', SavePath: '/Configuration/TranslateLocalType', Table: null },
                    { Name: 'localstates', OneColum: true, tblName: 'TableLocStates', Path: '/Configuration/GetLocalStatesTranslations', SavePath: '/Configuration/TranslateLocalState', Table: null },
                    { Name: 'localizationtypes', OneColum: true, tblName: 'TableLoalizations', Path: '/Configuration/GetLocalizationTypesTranslations', SavePath: '/Configuration/TranslateLocalizationType', Table: null },
                    { Name: 'localgroups', OneColum: false, tblName: 'TableLocGroups', Path: '/Configuration/GetLocalGroupsTranslations', SavePath: '/Configuration/TranslateLocalGroup', Table: null },
                    { Name: 'filestates', OneColum: true, tblName: 'TableFileStates', Path: '/Configuration/GetFileStatesTranslations', SavePath: '/Configuration/TranslateFileState', Table: null },
                    { Name: 'energysupplytypes', OneColum: true, tblName: 'TableEnergySupply', Path: '/Configuration/GetEnergySupplyTypesTranslations', SavePath: '/Configuration/TranslateEnergySupplyType', Table: null },
                    { Name: 'energyproductiontypes', OneColum: false, tblName: 'TableEnergyProd', Path: '/Configuration/GetEnergyProductionTypesTranslations', SavePath: '/Configuration/TranslateEnergyProductionType', Table: null },
                    { Name: 'documenttypes', OneColum: true, tblName: 'TableDocTypes', Path: '/Configuration/GetDocumentTypesTranslations', SavePath: '/Configuration/TranslateDocumentType', Table: null },
                    { Name: 'connectiontypes', OneColum: false, tblName: 'TableConnTypes', Path: '/Configuration/GetConnectionTypesTranslations', SavePath: '/Configuration/TranslateConnectionType', Table: null },
                    { Name: 'classificationtypes', OneColum: true, tblName: 'TableClassTypes', Path: '/Configuration/GetClassificationTypesTranslations', SavePath: '/Configuration/TranslateClassificationType', Table: null },
                    { Name: 'classifications', OneColum: true, tblName: 'TableClass', Path: '/Configuration/GetClassificationsTranslations', SavePath: '/Configuration/TranslateClassification', Table: null },
                    { Name: 'roles', OneColum: false, tblName: 'TableRoles', Path: '/Configuration/GetRolesTranslations', SavePath: '/Configuration/TranslateRole', Table: null },
                    { Name: 'actiongroups', OneColum: false, tblName: 'TableActionGroups', Path: '/Configuration/GetActionGroupTranslations', SavePath: '/Configuration/TranslateActionGroup', Table: null }
];

