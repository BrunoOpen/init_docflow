﻿var Config = function () {

    this.CreateTableWithColumns = function (name, columns, order, columnDefs) {
        var mytable = $('#' + name).DataTable({

            "language": Language.DATATABLE_LANGUAGE,
            columns: columns,
            "columnDefs": columnDefs,
            "order": [order],
            "lengthMenu": [
                [5, 10, 15, 20, 40, 50, 100, -1],
                [5, 10, 15, 20, 40, 50, 100, Language.DATATABLE_ALL] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            //buttons: {
            //    dom: {
            //        button: { tag: 'li' },
            //        container: { tag: 'ul', className: 'dropdown-menu pull-right' },

            //        buttonLiner: {
            //            tag: 'a' //a volta do texto
            //        }
            //    },
            //    buttons: [
            //            { extend: 'excelHtml5', text: ' <i class="fa fa-file-excel-o"></i> Exportar para Excel' },
            //            { extend: 'pdfHtml5', text: '  <i class="fa  fa-file-pdf-o"></i></i> Exportar para PDF' },
            //            { extend: 'print', text: ' <i class="fa fa-print"></i> Imprimir' },
            //    ]
            //}
        });

        //mytable.buttons().container().appendTo($('#export-' + name));
        return mytable;
    }

    this.ReloadTable = function () {
            $.ajax({
                url: "/Configuration/GetPeriods",
                type: "GET",
                dataType: "json",
                success: function (response) {

                    $.each(response, function (i, o) {
                        o.publish = null;
                        o.displayDate = '<span style="display:none;">'+ o.Period.replace('/Date(','').replace(')/','')+'</span>'+ Utils.JsonDateToMMYYYY(o.Period);
                        if(o.IsPublished==true){
                            o.checked = ' <i class="fa fa-check"></i>';
                            o.publish = '';
                        }
                        else
                            o.checked = '<i class="fa fa-times"></i>';
                    });
                    Config.Periods.clear().rows.add(response).sort().draw();

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('error');
                }
            });
    }

    this.initEdition = function () {
        Config.Periods.on('click', '.publish', function (e) {
            Config.Periods.Row = Config.Periods.row($(this).parents('tr'));
            Config.Periods.RowData = Config.Periods.Row.data();
            $(".modal-body").html(Language.CONFIG_MESSAGE_PUBLISH + ' ' + Config.Periods.RowData.displayDate);
        });

        $('#realPublish').on('click', function (e) {
            if (IsNullOrEmpty(Config.Periods.Row)) {
                return false;
            }
            else {
                $.ajax({
                    url: "/Configuration/PublishPeriods",
                    data: { periodId: Config.Periods.RowData.Id },
                    type: "POST",
                    dataType: "json",
                    success: function (response) {
                        ReloadTable();
                        $("#confirm").modal('hide');
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert('error');
                    }
                });
            }
        });

    }

    return {
        init: function () {
            var columns = [{ data: 'displayDate' }, { data: 'checked' }, { data: 'publish' }];

            if ($("#tbl-periods tr:first th").length == 3) {
                Config.Periods = CreateTableWithColumns("tbl-periods", columns, [0, 'desc'], [{ "width": "70%", "targets": [0] }, { "orderable": false, "targets": [1] },
                    {
                        "targets": 2,
                        "data": 'publish',
                        "defaultContent": '<a data-toggle="modal" href="#confirm" class="btn default btn-xs red-flamingo-stripe publish" >  <i class="fa fa-plus"></i>' + Language.PUBLISH + ' </a>'
                    }]);
            } else {
                Config.Periods = CreateTableWithColumns("tbl-periods", columns, [0, 'desc'], [{ "width": "70%", "targets": [0] }, { "orderable": false, "targets": [1] }]);
            }

           
            ReloadTable();
            initEdition();
        }
    }
}();



