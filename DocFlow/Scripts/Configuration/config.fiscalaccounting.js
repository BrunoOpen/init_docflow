﻿var Config = function () {

    this.CreateTable = function (name, order, columnDefs) {
        var mytable = $('#' + name).DataTable({

            "language": Language.DATATABLE_LANGUAGE,

            "columnDefs": columnDefs,
            "order": [order],
            "lengthMenu": [
                [5, 10, 15, 20, 40, 50, 100, -1],
                [5, 10, 15, 20, 40, 50, 100, Language.DATATABLE_ALL] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            buttons: {
                dom: {
                    button: { tag: 'li' },
                    container: { tag: 'ul', className: 'dropdown-menu pull-right' },

                    buttonLiner: {
                        tag: 'a' //a volta do texto
                    }
                },
                buttons: [
                        { extend: 'excelHtml5', text: ' <i class="fa fa-file-excel-o"></i> ' + Language.EXPORT_EXCEL },
                        { extend: 'pdfHtml5', text: '  <i class="fa  fa-file-pdf-o"></i></i> ' + Language.EXPORT_PDF },
                        { extend: 'print', text: ' <i class="fa fa-print"></i> ' + Language.PRINT },
                ]
            }
        });

        mytable.buttons().container().appendTo($('#export-' + name));
        return mytable;
    }

    this.InitYearTableEdit = function () {

        $("#fiscalyear-startdate,#fiscalyear-enddate").datepicker({
            rtl: false,
            orientation: 'right',
            cancelClass: 'default',
            format: 'MM yyyy',
            viewMode: "months",
            minViewMode: "months",
            language: "pt",
            autoclose: "true"
        });

        Config.YearTable.on('click', '.edit', function (e) {
            var $tr = $(this).closest('tr');
            $("#fiscalyear-startdate").datepicker('setDate', new Date($tr.data('start-date')));
            $("#fiscalyear-enddate").datepicker('setDate', new Date($tr.data('end-date')));
            $("#btn-fiscalyear-submit").data("id", $tr.data('id'));
            Config.YearTable.Tr = $tr;
        });
        Config.YearTable.on('click', '.new', function (e) {
            $("#fiscalyear-startdate").datepicker('setDate', null);
            $("#fiscalyear-enddate").datepicker('setDate', null);
            $("#btn-fiscalyear-submit").data("id", "");
            Config.YearTable.Tr = null;
        });

        $('#btn-fiscalyear-submit').on('click', function (e) {
            $.ajax({
                url: "/Configuration/SaveFiscalYear",
                data: { BeginDate: $("#fiscalyear-startdate").datepicker('getDate').toJSON(), EndDate: $("#fiscalyear-enddate").datepicker('getDate').toJSON(), Id: $("#btn-fiscalyear-submit").data("id") },
                type: "POST",
                dataType: "json",
                success: function (response) {
                    if (!IsNullOrEmpty(Config.YearTable.Tr)) {
                        Config.YearTable.Tr.data('id', response.Id);
                        Config.YearTable.Tr.data('start-date', response.BeginDate);
                        Config.YearTable.Tr.data('end-date', response.EndDate);
                        Config.YearTable.Tr.find('td:eq(0)').html(response.DisplayBeginDate)
                        Config.YearTable.Tr.find('td:eq(1)').html(response.DisplayEndDate)
                        Config.YearTable.Tr.find('td:eq(2)').html('<a data-toggle="modal" href="#modal-fiscalyears" class="btn default btn-xs red-flamingo-stripe edit" id="fiscalyears_edit">  <i class="fa fa-plus"></i>' + Language.EDIT + ' </a>');
                    } else {
                        var $clone = $("#fiscalyears tr:eq(1)").clone();
                        $clone.data('id', response.Id);
                        $clone.data('start-date', response.BeginDate);
                        $clone.data('end-date', response.EndDate);
                        $clone.find('td:eq(0)').html(response.DisplayBeginDate)
                        $clone.find('td:eq(1)').html(response.DisplayEndDate)
                        $clone.find('td:eq(2)').html('<a data-toggle="modal" href="#modal-fiscalyears" class="btn default btn-xs red-flamingo-stripe edit" id="fiscalyears_edit">  <i class="fa fa-plus"></i>' + Language.EDIT + ' </a>');
                        Config.YearTable.rows.add($clone).draw();
                    }
                    $('#modal-fiscalyears').modal('hide');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('error');
                }
            });
        });

    }

    this.InitVatTableEdit = function () {


        $("#vats-startdate,#vats-enddate").datepicker({
            rtl: false,
            orientation: 'right',
            cancelClass: 'default',
            format: 'dd/mm/yyyy',

            language: "pt",
            autoclose: "true"
        });



        Config.VatTable.on('click', '.edit', function (e) {
            //   <tr data-id="@item.Id" data-value="@item.Value.Replace(" ","").Replace('%','')" data-type-id="@item.TypeId" data-code="@item.SAPCode">

            var $tr = $(this).closest('tr');
            $("#vats-value").val((Number($tr.data('value').replace(",", ".")) * 100).toFixed(2));
            $("#vats-type").val($tr.data('type-id')).selectpicker('refresh');
            $("#vats-zone").val($tr.data('zone-id')).selectpicker('refresh');
            $("#vats-startdate").datepicker('setDate', Utils.StringddmmyyyyToDate($tr.data('start-date')));
            if (!IsNullOrEmpty($tr.data('end-date')))
                $("#vats-enddate").datepicker('setDate', Utils.StringddmmyyyyToDate($tr.data('end-date')));
            else
                $("#vats-enddate").datepicker('setDate', null);
            $("#vats-sap-code").val($tr.data('sap-code'));
            $("#btn-vats-submit").data("id", $tr.data('id'));
            Config.VatTable.Tr = $tr;
        });

        Config.VatTable.on('click', '.new', function (e) {
            $("#vats-value").val('');
            $("#vats-type").val('').selectpicker('refresh');
            $("#vats-sap-code").val('');
            $("#vats-zone").val('').selectpicker('refresh');
            $("#btn-vats-submit").data("id", '');
            $("#vats-startdate").datepicker('setDate', new Date());
            $("#vats-enddate").datepicker('setDate', null);
            Config.VatTable.Tr = null;
        });

        $('#btn-vats-submit').on('click', function (e) {
            $.ajax({
                url: "/Configuration/SaveVat",
                data: {

                    Percentage: (Number($("#vats-value").val()) / 100).toString().replace('.', ','),
                    SAPCode: $("#vats-sap-code").val(),
                    Id: $("#btn-vats-submit").data("id"),
                    TypeId: $("#vats-type").val(),
                    Type: $("#vats-type option:selected").html(),
                    ZoneId: $("#vats-zone").val(),
                    Zone: $("#vats-zone option:selected").html(),
                    StartDate: $("#vats-startdate").datepicker('getDate').toJSON(),
                    EndDate: $("#vats-enddate").datepicker('getDate').toJSON()
                },
                type: "POST",
                dataType: "json",
                success: function (response) {
                    if (!IsNullOrEmpty(Config.VatTable.Tr)) {
                        Config.VatTable.Tr.data('value', response.Percentage);
                        Config.VatTable.Tr.data('type-id', response.TypeId);
                        Config.VatTable.Tr.data('zone-id', response.ZoneId);
                        Config.VatTable.Tr.data('sap-code', response.SAPCode);
                        Config.VatTable.Tr.data('start-date', response.DisplayStartDate);
                        Config.VatTable.Tr.data('end-date', response.DisplayEndDate);
                        Config.VatTable.Tr.data('id', response.Id);

                        Config.VatTable.Tr.find('td:eq(0)').html(response.Zone);
                        Config.VatTable.Tr.find('td:eq(1)').html(response.Type);
                        Config.VatTable.Tr.find('td:eq(2)').html(Utils.FormatNumber(response.Percentage * 100,2));
                        Config.VatTable.Tr.find('td:eq(3)').html(response.DisplayStartDate)
                        Config.VatTable.Tr.find('td:eq(4)').html(response.DisplayEndDate)
                        Config.VatTable.Tr.find('td:eq(5)').html(response.SAPCode)
                        Config.VatTable.Tr.find('td:eq(6)').html('<a data-toggle="modal" href="#modal-vats" class="btn default btn-xs red-flamingo-stripe edit" id="vats_edit">  <i class="fa fa-plus"></i>' + Language.EDIT + ' </a>');
                        Config.VatTable.sort().draw();
                    } else {

                        var $clone = $("#vats tr:eq(1)").clone();
                        $clone.data('value', response.Percentage,2);
                        $clone.data('type-id', response.TypeId);
                        $clone.data('zone-id', response.ZoneId);
                        $clone.data('start-date', response.DisplayStartDate);
                        $clone.data('end-date', response.DisplayEndDate);
                        $clone.data('sap-code', response.SAPCode);
                        $clone.data('id', response.Id);

                        $clone.find('td:eq(0)').html(response.Zone);
                        $clone.find('td:eq(1)').html(response.Type);
                        $clone.find('td:eq(2)').html(Utils.FormatNumber(response.Percentage * 100, 2));
                        $clone.find('td:eq(3)').html(response.DisplayStartDate);
                        $clone.find('td:eq(4)').html(response.DisplayEndDate);
                        $clone.find('td:eq(5)').html(response.SAPCode);
                        $clone.find('td:eq(6)').html('<a data-toggle="modal" href="#modal-vats" class="btn default btn-xs red-flamingo-stripe edit" id="vats_edit">  <i class="fa fa-plus"></i>' + Language.EDIT + ' </a>');


                        Config.VatTable.rows.add($clone).sort().draw();
                    }
                    $('#modal-vats').modal('hide');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('error');
                }
            });
        });

    }

    this.InitDocsTableEdit = function () {

        Config.DocsTable.on('click', '.edit', function (e) {
            var $tr = $(this).closest('tr');
            $("#doctypes-type").val($tr.data('doc-type'));
            $("#doctypes-sap-code").val($tr.data('sap-code'));
            $("#doctypes-transaction-code-credit").val($tr.data('transaction-code-credit'));
            $("#doctypes-transaction-code-debit").val($tr.data('transaction-code-debit'));
            $("#btn-doctypes-submit").data("id", $tr.data('id'));
            $("#doctypes-color").val($tr.data('color'));
            $("#doctypes-color").trigger('change');
            Config.DocsTable.Tr = $tr;
        });

        $('#btn-doctypes-submit').on('click', function (e) {
            $.ajax({
                url: "/Configuration/SaveDocTypes",
                data: {
                    Id: $("#btn-doctypes-submit").data("id"),
                    Code: $("#doctypes-sap-code").val(),
                    TransactionCodeCredit: $("#doctypes-transaction-code-credit").val(),
                    TransactionCodeDebit: $("#doctypes-transaction-code-debit").val(),
                    Color: $("#doctypes-color").val()
                },
                type: "POST",
                dataType: "json",
                success: function (response) {
                    Config.DocsTable.Tr.data('sap-code', response.Code);
                    Config.DocsTable.Tr.data('transaction-code-debit', response.TransactionCodeDebit);
                    Config.DocsTable.Tr.data('transaction-code-credit', response.TransactionCodeCredit);
                    Config.DocsTable.Tr.find('td:eq(2)').html(response.Code);
                    Config.DocsTable.Tr.find('td:eq(3)').html(response.TransactionCodeCredit);
                    Config.DocsTable.Tr.find('td:eq(4)').html(response.TransactionCodeDebit);
                    Config.DocsTable.Tr.data('color', response.Color);
                    Config.DocsTable.Tr.find('td:eq(0)').css('background-color', response.Color);
                    $('#modal-doctypes').modal('hide');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('error');
                }
            });
        });

    }

    this.InitDocsNameTableEdit = function () {

        Config.DocsNamesTable.on('click', '.edit', function (e) {
            var $tr = $(this).closest('tr');
            $("#docnames-type").val($tr.data('doc-name'));
            $("#btn-docnames-submit").data("id", $tr.data('id'));
            $("#docnames-sysdoctype_Value").val($tr.data('system-document-type'));
            $("#docnames-sysdoctype_Value").trigger('change');
            Config.DocsNamesTable.Tr = $tr;
        });

        $('#btn-docnames-submit').on('click', function (e) {
            $.ajax({
                url: "/Configuration/SaveDocNames",
                data: {
                    Id: $("#btn-docnames-submit").data("id"),
                    SystemDocumentTypeID: $("#docnames-sysdoctype_Value").val()
                },
                type: "POST",
                dataType: "json",
                success: function (response) {
                    Config.DocsNamesTable.Tr.data('system-document-type', response.SystemDocumentTypeID);
                    Config.DocsNamesTable.Tr.children(':eq(1)').text(response.SystemDocumentType)
                    $('#modal-docnames').modal('hide');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('error');
                }
            });
        });

    }

    this.EditAll = function ($tr) {

        $("#loctypes-localtype").val($tr.data('loctype'));
        $("#loctypes-costcenter").val($tr.data('costcenter'));
        $("#loctypes-glaccount").val($tr.data('glaccount'));
        $("#loctypes-internalorder").val($tr.data('internalorder'));
        if ($tr.data('networking') == true || $tr.data('networking') == "True" || $tr.data('networking') == "true") {
            $("#loctypes-networking").attr("checked", "checked");
        }
        else {
            $("#loctypes-networking").removeAttr("checked");
        }
        $.uniform.update()

        $("#btn-loctypes-submit").data("id", $tr.data('id'));
        $("#btn-loctypes-submit-all").data("id", $tr.data('id'));

        Config.LocalsTable.Tr = $tr;
    }

    this.EditCostCenter = function ($tr) {

        $("#costCenter-loctypes-localtype").val($tr.data('loctype'));
        $("#costCenter-loctypes-costcenter").val($tr.data('costcenter'));

        $("#all-costCenter-btn-loctypes-submit").data("id", $tr.data('id'));
        $("#costCenter-btn-loctypes-submit").data("id", $tr.data('id'));
        Config.LocalsTable.Tr = $tr;
    }
    this.EditGLAccount = function ($tr) {

        $("#glAccount-loctypes-localtype").val($tr.data('loctype'));
        $("#glAccount-loctypes-glaccount").val($tr.data('glaccount'));
        $("#glAccount-btn-loctypes-submit").data("id", $tr.data('id'));
        $("#all-glAccount-btn-loctypes-submit").data("id", $tr.data('id'));
        Config.LocalsTable.Tr = $tr;
    }
    this.EditInternalOrder = function ($tr) {

        $("#internalOrder-loctypes-localtype").val($tr.data('loctype'));

        $("#internalOrder-loctypes-internalorder").val($tr.data('internalorder'));

        $("#all-internalOrder-btn-loctypes-submit").data("id", $tr.data('id'));
        $("#internalOrder-btn-loctypes-submit").data("id", $tr.data('id'));
        Config.LocalsTable.Tr = $tr;
    }
    this.InitLocalsTableEdit = function () {

        $(document.body).on('click', ".saps-edit", function () {
            var $tr = $("tr[data-id=" + $(this).data('row-id') + "][data-row-type=saps]");
            switch ($(this).data('type')) {
                case 'all':
                    $("#btn-loctypes-submit").html(Language.UPDATE);
                    $("#btn-loctypes-submit-all").show();

                    EditAll($tr);
                    $('#modal-loctypes').modal('show');
                    break;
                case 'nameNetworking':
                    EditNameNetwork($tr);
                    $('#modal-loctypes-nameNetworking').modal('show');
                    break;
                case 'costCenter':
                    EditCostCenter($tr);
                    $('#modal-loctypes-costCenter').modal('show');
                    break;
                case 'glAccount':
                    EditGLAccount($tr);
                    $('#modal-loctypes-glAccount').modal('show');
                    break;
                case 'internalOrder':
                    EditInternalOrder($tr);
                    $('#modal-loctypes-internalOrder').modal('show');
                    break;
                default:

            }
        });



        Config.LocalsTable.on('click', '.new', function (e) {
            $("#btn-loctypes-submit-all").hide();
            $("#btn-loctypes-submit").html(Language.SAVE);

            $("#loctypes-localtype").val('');
            $("#loctypes-costcenter").val('');
            $("#loctypes-glaccount").val('');
            $("#loctypes-internalorder").val('');
            $("#loctypes-networking").removeAttr("checked");
            $.uniform.update()
            $("#btn-loctypes-submit").data("id", '');
            Config.LocalsTable.Tr = null;
        });

        $('[id^="btn-loctypes-submit"]').on('click', function (e) {

            var updateAll = $(this).attr('id').indexOf("-all") > 0;
            $.ajax({
                url: "/Configuration/SaveLocalTypes",
                data: {
                    Id: $("#btn-loctypes-submit").data('id'),
                    LocalType: $("#loctypes-localtype").val(),
                    CostCenter: $("#loctypes-costcenter").val(),
                    GLAccount: $("#loctypes-glaccount").val(),
                    InternalOrder: $("#loctypes-internalorder").val(),
                    Network: $("#loctypes-networking").attr("checked") == "checked" ? true : false,
                    ServiceId: $("#model-service-id").val(),
                    UpdateAll: updateAll
                },
                type: "POST",
                dataType: "json",
                success: function (response) {
                    if (!IsNullOrEmpty(Config.LocalsTable.Tr)) {
                        Config.LocalsTable.Tr.data('loctype', response.LocalType);
                        Config.LocalsTable.Tr.data('costcenter', response.CostCenter);
                        Config.LocalsTable.Tr.data('glaccount', response.GLAccount);
                        Config.LocalsTable.Tr.data('internalorder', response.InternalOrder);
                        Config.LocalsTable.Tr.data('networking', response.Network);
                        Config.LocalsTable.Tr.data('id', response.Id);

                        Config.LocalsTable.Tr.find('td:eq(0)').html(response.LocalType)
                        Config.LocalsTable.Tr.find('td:eq(1)').html(response.CostCenter)
                        Config.LocalsTable.Tr.find('td:eq(2)').html(response.GLAccount)
                        Config.LocalsTable.Tr.find('td:eq(3)').html(response.InternalOrder)

                        if (response.Network == true || response.Network == "True" || response.Network == "true") {
                            Config.LocalsTable.Tr.find('td:eq(4)').html('<div class="checker disabled" id="uniform-item_Network"> <span class="checked"> <input checked="checked" disabled="disabled" id="item_Network" name="item.Network" type="checkbox" value="true"></span></div>');
                        } else {
                            Config.LocalsTable.Tr.find('td:eq(4)').html('<div class="checker disabled" id="uniform-item_Network"> <span> <input disabled="disabled" id="item_Network" name="item.Network" type="checkbox" value="true"> </span></div>');
                        }
                        //                                                                <li class="saps-edit" data-row-id="'+ response.Id + '" data-type="nameNetworking"> <a href="javascript:;"> '+Language.NAME_AND_NETWORKING+'</a></li> \
                        Config.LocalsTable.Tr.find('td:eq(5)').html('   <div class="btn-group fixed-dropdown"> \
                                                            <button class="btn btn-sm purple dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> \
                                                                '+ Language.EDIT + ' <i class="fa fa-angle-down"></i> \
                                                            </button> \
                                                            <ul class="dropdown-menu" role="menu" style="left: -100px;"> \
                                                                <li class="saps-edit" data-row-id="' + response.Id + '" data-type="all"> <a href="javascript:;"> ' + Language.ALL + ' </a> </li> \
                                                                <li class="saps-edit" data-row-id="' + response.Id + '" data-type="costCenter"> <a href="javascript:;"> ' + Language.COST_CENTER + ' </a></li> \
                                                                <li class="saps-edit" data-row-id="' + response.Id + '" data-type="glAccount"> <a href="javascript:;"> ' + Language.GL_ACCOUNT + ' </a></li> \
                                                                <li class="saps-edit" data-row-id="' + response.Id + '" data-type="internalOrder"> <a href="javascript:;"> ' + Language.INTERNAL_ORDER + ' </a></li> \
                                                            </ul> \
                                                        </div> ');

                    } else {
                        var $clone = $("#loctypes tr:eq(1)").clone();
                        $clone.data('loctype', response.LocalType);
                        $clone.data('costcenter', response.CostCenter);
                        $clone.data('glaccount', response.GLAccount);
                        $clone.data('internalorder', response.InternalOrder);
                        $clone.data('networking', response.Network);
                        $clone.data('id', response.Id);

                        $clone.find('td:eq(0)').html(response.LocalType)
                        $clone.find('td:eq(1)').html(response.CostCenter)
                        $clone.find('td:eq(2)').html(response.GLAccount)
                        $clone.find('td:eq(3)').html(response.InternalOrder)

                        if (response.Network == true || response.Network == "true" || response.Network == "True") {
                            $clone.find('td:eq(4)').html('<div class="checker disabled" id="uniform-item_Network"> <span class="checked"> <input checked="checked" disabled="disabled" id="item_Network" name="item.Network" type="checkbox" value="true"></span></div>');
                        } else {
                            $clone.find('td:eq(4)').html('<div class="checker disabled" id="uniform-item_Network"> <span> <input disabled="disabled" id="item_Network" name="item.Network" type="checkbox" value="true"> </span></div>');
                        }
                        //$clone.find('td:eq(5)').html('<a data-toggle="modal" href="#modal-loctypes" class="btn default btn-xs red-flamingo-stripe edit" id="loctypes_edit">  <i class="fa fa-plus"></i>Editar </a>');

                        $clone.find('td:eq(5)').html('   <div class="btn-group fixed-dropdown"> \
                                                            <button class="btn btn-sm purple dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> \
                                                                '+ Language.EDIT + ' <i class="fa fa-angle-down"></i> \
                                                            </button> \
                                                            <ul class="dropdown-menu" role="menu" style="left: -100px;"> \
                                                                <li class="saps-edit" data-row-id="' + response.Id + '" data-type="all"> <a href="javascript:;"> ' + Language.ALL + ' </a> </li> \
                                                                <li class="saps-edit" data-row-id="' + response.Id + '" data-type="nameNetworking"> <a href="javascript:;"> ' + Language.NAME_AND_NETWORKING + '</a></li> \
                                                                <li class="saps-edit" data-row-id="' + response.Id + '" data-type="costCenter"> <a href="javascript:;"> ' + Language.COST_CENTER + 'o </a></li> \
                                                                <li class="saps-edit" data-row-id="' + response.Id + '" data-type="glAccount"> <a href="javascript:;"> ' + Language.GL_ACCOUNT + ' </a></li> \
                                                                <li class="saps-edit" data-row-id="' + response.Id + '" data-type="internalOrder"> <a href="javascript:;"> ' + Language.INTERNAL_ORDER + ' </a></li> \
                                                            </ul> \
                                                        </div> ');

                        Config.LocalsTable.rows.add($clone).draw();
                    }
                    $('#modal-loctypes').modal('hide');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('error');
                }
            });
        });

    }
    this.InitLocalTypesSubmits = function () {

        $("[id$=btn-loctypes-submit]").on('click', function () {

            var path = '';
            var data = { Id: $(this).data('id'), ServiceId: $("#model-service-id").val(), UpdateAll: false };
            switch ($(this).attr('id')) {
                case 'nameNetworking-btn-loctypes-submit':
                    data.LocalType = $("#nameNetworking-loctypes-localtype").val();
                    data.Network = $("#nameNetworking-loctypes-networking").attr("checked") == "checked" ? true : false;
                    path = "/Configuration/SaveLocalTypesNameNetwork";
                    $('#modal-loctypes-nameNetworking').modal('hide');
                    break;

                case 'all-costCenter-btn-loctypes-submit':
                    data.UpdateAll = true;
                case 'costCenter-btn-loctypes-submit':
                    path = "/Configuration/SaveLocalTypesCostCenter";
                    data.CostCenter = $("#costCenter-loctypes-costcenter").val();;
                    $('#modal-loctypes-costCenter').modal('hide');
                    break;

                case 'all-glAccount-btn-loctypes-submit':
                    data.UpdateAll = true;
                case 'glAccount-btn-loctypes-submit':
                    path = "/Configuration/SaveLocalTypesGLAccount";
                    data.GLAccount = $("#glAccount-loctypes-glaccount").val();;
                    $('#modal-loctypes-glAccount').modal('hide');
                    break;

                case 'all-internalOrder-btn-loctypes-submit':
                    data.UpdateAll = true;
                case 'internalOrder-btn-loctypes-submit':
                    path = "/Configuration/SaveLocalTypesInternalOrder";
                    data.InternalOrder = $("#internalOrder-loctypes-internalorder").val();;
                    $('#modal-loctypes-internalOrder').modal('hide');
                    break;

                default:
                    return;
            }

            $.ajax({
                url: path,
                data: data,
                type: "POST",
                dataType: "json",
                success: function (response) {

                    Config.LocalsTable.Tr.data('loctype', response.LocalType);
                    Config.LocalsTable.Tr.data('costcenter', response.CostCenter);
                    Config.LocalsTable.Tr.data('glaccount', response.GLAccount);
                    Config.LocalsTable.Tr.data('internalorder', response.InternalOrder);
                    Config.LocalsTable.Tr.data('networking', response.Network);
                    Config.LocalsTable.Tr.data('id', response.Id);

                    Config.LocalsTable.Tr.find('td:eq(0)').html(response.LocalType)
                    Config.LocalsTable.Tr.find('td:eq(1)').html(response.CostCenter)
                    Config.LocalsTable.Tr.find('td:eq(2)').html(response.GLAccount)
                    Config.LocalsTable.Tr.find('td:eq(3)').html(response.InternalOrder)

                    if (response.Network == true || response.Network == "True" || response.Network == "true") {
                        Config.LocalsTable.Tr.find('td:eq(4)').html('<div class="checker disabled" id="uniform-item_Network"> <span class="checked"> <input checked="checked" disabled="disabled" id="item_Network" name="item.Network" type="checkbox" value="true"></span></div>');
                    } else {
                        Config.LocalsTable.Tr.find('td:eq(4)').html('<div class="checker disabled" id="uniform-item_Network"> <span> <input disabled="disabled" id="item_Network" name="item.Network" type="checkbox" value="true"> </span></div>');
                    }
                    //Config.LocalsTable.Tr.find('td:eq(5)').html('<ul class="dropdown-menu" role="menu"> \
                    //                                            <li class="saps-edit" data-row-id="'+ response.Id + '" data-type="all"> <a href="javascript:;"> Tudo </a> </li>\
                    //                                            <li class="saps-edit" data-row-id="'+ response.Id + '" data-type="nameNetworking"> <a href="javascript:;"> Nome e Networking</a></li> \
                    //                                            <li class="saps-edit" data-row-id="'+ response.Id + '" data-type="costCenter"> <a href="javascript:;"> Centro de Custo </a></li> \
                    //                                            <li class="saps-edit" data-row-id="'+ response.Id + '" data-type="glAccount"> <a href="javascript:;"> Conta GL </a></li> \
                    //                                            <li class="saps-edit" data-row-id="'+ response.Id + '" data-type="internalOrder"> <a href="javascript:;"> Ordem Interna </a></li> \
                    //                                        </ul>');   <li class="saps-edit" data-row-id="' + response.Id + '" data-type="nameNetworking"> <a href="javascript:;">' + Language.NAME_AND_NETWORKING + '</a></li> \


                    Config.LocalsTable.Tr.find('td:eq(5)').html('   <div class="btn-group fixed-dropdown"> \
                                                            <button class="btn btn-sm purple dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> \
                                                                ' + Language.EDIT + ' <i class="fa fa-angle-down"></i> \
                                                            </button> \
                                                            <ul class="dropdown-menu" role="menu" style="left: -100px;"> \
                                                                <li class="saps-edit" data-row-id="' + response.Id + '" data-type="all"> <a href="javascript:;"> ' + Language.ALL + ' </a> </li> \
                                                                <li class="saps-edit" data-row-id="' + response.Id + '" data-type="costCenter"> <a href="javascript:;"> ' + Language.COST_CENTER + ' </a></li> \
                                                                <li class="saps-edit" data-row-id="' + response.Id + '" data-type="glAccount"> <a href="javascript:;"> ' + Language.GL_ACCOUNT + ' </a></li> \
                                                                <li class="saps-edit" data-row-id="' + response.Id + '" data-type="internalOrder"> <a href="javascript:;"> ' + Language.INTERNAL_ORDER + ' </a></li> \
                                                            </ul> \
                                                        </div> ');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('error');
                }
            });

        });
    }

    return {
        init: function () {

            Config.YearTable = CreateTable("fiscalyears", [1, 'desc'], [{ "width": "15%", "targets": [2] }, { "orderable": false, "targets": [2] }]);
            InitYearTableEdit();
            Config.VatTable = CreateTable("vats", [0, 'asc'], [{ "width": "15%", "targets": [6] }, { "orderable": false, "targets": [6] }]);
            InitVatTableEdit();
            Config.DocsTable = CreateTable("doctypes", [1, 'asc'], [{ "width": "20px", "targets": [0] }, { "width": "20%", "targets": [5] }, { "orderable": false, "targets": [0, 5] }]);
            InitDocsTableEdit();
            Config.DocsNamesTable = CreateTable("docnames", [1, 'asc'], [{ "width": "20px", "targets": [2] }, { "orderable": false, "targets": [2] }]);
            InitDocsNameTableEdit();
            Config.LocalsTable = CreateTable("loctypes", [0, 'asc'], [{ "orderable": false, "targets": [5] }]);
            InitLocalsTableEdit();
            InitLocalTypesSubmits();
            Utils.FixBsDropdown();
        }
    };
}();



