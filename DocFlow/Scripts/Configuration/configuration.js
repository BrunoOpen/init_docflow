﻿var Configuration = new Object();

Configuration.DateFromMonth = function (str) {
    var months_pt = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
    var months_en = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    var splitdate = str.split(" ");

    for (var i = 0; i < months_pt.length; i++) {
        if (months_pt[i] == splitdate[0] || months_en == splitdate[0])
            return new Date(Number(splitdate[1]), i, 1);
    }
    return null;
}

Configuration.DatePicker = function (selector, strDate) {
    selector.datepicker({
        //rtl: Metronic.isRTL(),
        //orientation: (Metronic.isRTL() ? 'left' : 'right'),
        //buttonClasses: ['btna'],
        //applyClass: ' blue',
        cancelClass: 'default',
        format: 'MM yyyy',
        viewMode: "months",
        minViewMode: "months",
        language: "pt",
        autoclose: "true"
        //,
        //endDate: new Date()
    });

    selector.datepicker("setDate", Configuration.DateFromMonth(strDate));
}

Configuration.GetDate = function (selector) {
    selector.datepicker("getDate");
}

Configuration.SaveFiscalYear = function (dateIni, dateEnd, id) {
    $.ajax({
        url: '/Configuration/SaveFiscalYear',
        data: JSON.stringify({ 'BeginDate': dateIni.toJSON(), 'EndDate': dateEnd.toJSON(), 'Id': id }),
        contentType: 'application/json',
        type: 'Post',
        dataType: 'application/json'
    }).success(function (data) {

    }).error(function () { });
}

Configuration.SpanToInput = function (span) {
    var $input = $('<input>').attr({
        type: 'text',
        name: span.attr("name"),
        style: 'width:30px;',
        value: span.text()
    });
    span.replaceWith($input);
}

Configuration.InputToSpan = function (input) {
    var $span = $('<span>').attr({
        name: input.attr("name")
    });
    $span.html(input.val());
    input.replaceWith($span);

}

jQuery(document).ready(function () {

    TableFiscalYear();
    TableLineTypes();
    TableLineClassification();
    TableLineLines();
    TableTechnologies();
    TablePowers();
    TableIVA();
    TableDesvios();
    TableDocuments();
    TableInstalations();
    TableTranslations();
});


function TableFiscalYear() {

    function restoreRow(oTable, nRow) {
        var aData = oTable.fnGetData(nRow);
        var jqTds = $('>td', nRow);

        for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
            oTable.fnUpdate(aData[i], nRow, i, false);
        }

        oTable.fnDraw();
    }

    function editRow(oTable, nRow) {
        var aData = oTable.fnGetData(nRow);
        var jqTds = $('>td', nRow);
        jqTds[0].innerHTML = '<input type="text" class="form-control input-small" id="date-start" ">';
        jqTds[1].innerHTML = '<input type="text" class="form-control input-small" id="date-end" ">';
        //jqTds[2].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[2] + '">';
        jqTds[2].innerHTML = '<a class="btn default btn-xs red-flamingo-stripe edit" href="">' + Language.OK + '</a> <a class="btn default btn-xs red-flamingo-stripe cancel" href="">' + Language.CANCEL + '</a>';
        Configuration.DatePicker($("#date-start"), aData[0]);
        Configuration.DatePicker($("#date-end"), aData[1]);

    }

    function saveRow(oTable, nRow) {
        var jqInputs = $('input', nRow);
        oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
        oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
        //oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
        oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i> ' + Language.EDIT + ' </a>', nRow, 2, false);

        var eRow = $(nRow);
        var id = eRow.attr("data-row-id");//enviar este Id
        Configuration.SaveFiscalYear(Configuration.DateFromMonth(jqInputs[0].value), Configuration.DateFromMonth(jqInputs[1].value, null));

        oTable.fnDraw();
    }

    function cancelEditRow(oTable, nRow) {
        var jqInputs = $('input', nRow);
        oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
        oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
        //oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
        oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i>' + Language.EDIT + ' </a>', nRow, 2, false);
        oTable.fnDraw();
    }

    var table = $('#table-fiscal');

    var oTable = table.dataTable({

        // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
        // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
        // So when dropdowns used the scrollable div should be removed.
        "dom": "t",
        "columnDefs": [
                    { "width": "20%", "targets": [0, 1] },
                    { "width": "60%", "targets": 2 },
                    { "className": "dt-right", "targets": 2 }

        ],

        "orderCellsTop": false,
        // Or you can use remote translation file
        //"language": {
        //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
        //},

    });



    var nEditing = null;
    var nNew = false;

    $('#table-fiscal_new').click(function (e) {
        e.preventDefault();

        if (nNew && nEditing) {
            if (confirm("Previose row not saved. Do you want to save it ?")) {
                saveRow(oTable, nEditing); // save
                $(nEditing).find("td:first").html("Untitled");
                nEditing = null;
                nNew = false;

            } else {
                oTable.fnDeleteRow(nEditing); // cancel
                nEditing = null;
                nNew = false;
                return;
            }
        }

        var aiNew = oTable.fnAddData(['', '', '']);
        var nRow = oTable.fnGetNodes(aiNew[0]);
        editRow(oTable, nRow);
        nEditing = nRow;
        nNew = true;
    });

    table.on('click', '.delete', function (e) {
        e.preventDefault();

        if (confirm("Are you sure to delete this row ?") == false) {
            return;
        }

        var nRow = $(this).parents('tr')[0];
        oTable.fnDeleteRow(nRow);
        //alert("Deleted! Do not forget to do some ajax to sync with backend :)");
    });

    table.on('click', '.cancel', function (e) {
        e.preventDefault();
        if (nNew) {
            oTable.fnDeleteRow(nEditing);
            nEditing = null;
            nNew = false;
        } else {
            restoreRow(oTable, nEditing);
            nEditing = null;
        }
    });

    table.on('click', '.edit', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var nRow = $(this).parents('tr')[0];

        if (nEditing !== null && nEditing != nRow) {
            /* Currently editing - but not this row - restore the old before continuing to edit mode */
            restoreRow(oTable, nEditing);
            editRow(oTable, nRow);
            nEditing = nRow;
        } else if (nEditing == nRow && this.innerHTML == "OK") {
            /* Editing this row and want to save it */
            saveRow(oTable, nEditing);
            nEditing = null;
            //alert("Updated! Do not forget to do some ajax to sync with backend :)");
        } else {
            /* No edit in progress - let's start one */
            editRow(oTable, nRow);
            nEditing = nRow;
        }
    });
}

function TableLineTypes() {

    function restoreRow(oTable, nRow) {
        var aData = oTable.fnGetData(nRow);
        var jqTds = $('>td', nRow);

        for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
            oTable.fnUpdate(aData[i], nRow, i, false);
        }

        oTable.fnDraw();
    }

    function editRow(oTable, nRow) {
        var aData = oTable.fnGetData(nRow);
        var jqTds = $('>td', nRow);
        jqTds[0].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[0] + '">';
        //jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[1] + '">';
        jqTds[1].innerHTML = '<a class="btn default btn-xs red-flamingo-stripe edit" href="">OK</a> <a class="btn default btn-xs red-flamingo-stripe cancel" href="">Cancelar</a>';
    }

    function saveRow(oTable, nRow) {
        var jqInputs = $('input', nRow);
        oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
        //oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
        oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i> Editar </a><a href="javascript:;" class="btn default btn-xs red-flamingo-stripe delete"> <i class="fa fa-pencil"></i>Eliminar </a>', nRow, 1, false);
        oTable.fnDraw();
    }

    function cancelEditRow(oTable, nRow) {
        var jqInputs = $('input', nRow);
        oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
        //oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
        oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i>Editar </a> <a href="javascript:;" class="btn default btn-xs red-flamingo-stripe delete"> <i class="fa fa-pencil"></i>Eliminar </a>', nRow, 1, false);
        oTable.fnDraw();
    }

    var table = $('#table-line-types');

    var oTable = table.dataTable({

        // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
        // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
        // So when dropdowns used the scrollable div should be removed.
        "dom": "t",
        "columnDefs": [
                    { "width": "50%", "targets": [0, 1] },
                    { "className": "dt-right", "targets": 1 }

        ],
        "orderCellsTop": false,
        // Or you can use remote translation file
        //"language": {
        //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
        //},

    });



    var nEditing = null;
    var nNew = false;

    $('#table-line-types_new').click(function (e) {
        e.preventDefault();

        if (nNew && nEditing) {
            if (confirm("Previose row not saved. Do you want to save it ?")) {
                saveRow(oTable, nEditing); // save
                $(nEditing).find("td:first").html("Untitled");
                nEditing = null;
                nNew = false;

            } else {
                oTable.fnDeleteRow(nEditing); // cancel
                nEditing = null;
                nNew = false;
                return;
            }
        }

        var aiNew = oTable.fnAddData(['', '']);
        var nRow = oTable.fnGetNodes(aiNew[0]);
        editRow(oTable, nRow);
        nEditing = nRow;
        nNew = true;
    });

    table.on('click', '.delete', function (e) {
        e.preventDefault();

        if (confirm("Are you sure to delete this row ?") == false) {
            return;
        }

        var nRow = $(this).parents('tr')[0];
        oTable.fnDeleteRow(nRow);
        //alert("Deleted! Do not forget to do some ajax to sync with backend :)");
    });

    table.on('click', '.cancel', function (e) {
        e.preventDefault();
        if (nNew) {
            oTable.fnDeleteRow(nEditing);
            nEditing = null;
            nNew = false;
        } else {
            restoreRow(oTable, nEditing);
            nEditing = null;
        }
    });

    table.on('click', '.edit', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var nRow = $(this).parents('tr')[0];

        if (nEditing !== null && nEditing != nRow) {
            /* Currently editing - but not this row - restore the old before continuing to edit mode */
            restoreRow(oTable, nEditing);
            editRow(oTable, nRow);
            nEditing = nRow;
        } else if (nEditing == nRow && this.innerHTML == "OK") {
            /* Editing this row and want to save it */
            saveRow(oTable, nEditing);
            nEditing = null;
            //alert("Updated! Do not forget to do some ajax to sync with backend :)");
        } else {
            /* No edit in progress - let's start one */
            editRow(oTable, nRow);
            nEditing = nRow;
        }
    });
}

function TableLineClassification() {

    function restoreRow(oTable, nRow) {
        var aData = oTable.fnGetData(nRow);
        var jqTds = $('>td', nRow);

        for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
            oTable.fnUpdate(aData[i], nRow, i, false);
        }

        oTable.fnDraw();
    }

    function editRow(oTable, nRow) {
        var aData = oTable.fnGetData(nRow);
        var jqTds = $('>td', nRow);
        jqTds[0].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[0] + '">';

        jqTds[1].innerHTML = '<select class="form-control"> <option selected="selected" value="Consumo">Consumo</option><option value="Iva">Iva</option><option value="Leitura">Leitura</option><option value="Taxa">Taxa</option></select>';
        // jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[1] + '">';
        jqTds[2].innerHTML = '<a class="btn default btn-xs red-flamingo-stripe edit" href="">OK</a> <a class="btn default btn-xs red-flamingo-stripe cancel" href="">Cancelar</a>';
    }

    function saveRow(oTable, nRow) {
        var jqInputs = $('input', nRow);
        var jqSelects = $('select', nRow);
        oTable.fnUpdate(jqSelects[0].value, nRow, 1, false);
        oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
        oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i> Editar </a>', nRow, 2, false);
        oTable.fnDraw();
    }

    function cancelEditRow(oTable, nRow) {
        var jqInputs = $('input', nRow);
        oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
        oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
        oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i>Editar </a>', nRow, 2, false);
        oTable.fnDraw();
    }

    var table = $('#table-line-class');

    var oTable = table.dataTable({

        // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
        // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
        // So when dropdowns used the scrollable div should be removed.
        "dom": "t",
        "columnDefs": [
                    { "width": "33%", "targets": [0, 1, 2] },
                    { "className": "dt-right", "targets": 2 }

        ],
        "orderCellsTop": false,
        // Or you can use remote translation file
        //"language": {
        //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
        //},

    });



    var nEditing = null;
    var nNew = false;

    $('#table-line-class_new').click(function (e) {
        e.preventDefault();

        if (nNew && nEditing) {
            if (confirm("Previose row not saved. Do you want to save it ?")) {
                saveRow(oTable, nEditing); // save
                $(nEditing).find("td:first").html("Untitled");
                nEditing = null;
                nNew = false;

            } else {
                oTable.fnDeleteRow(nEditing); // cancel
                nEditing = null;
                nNew = false;
                return;
            }
        }

        var aiNew = oTable.fnAddData(['', '', '']);
        var nRow = oTable.fnGetNodes(aiNew[0]);
        editRow(oTable, nRow);
        nEditing = nRow;
        nNew = true;
    });

    table.on('click', '.delete', function (e) {
        e.preventDefault();

        if (confirm("Are you sure to delete this row ?") == false) {
            return;
        }

        var nRow = $(this).parents('tr')[0];
        oTable.fnDeleteRow(nRow);
        //alert("Deleted! Do not forget to do some ajax to sync with backend :)");
    });

    table.on('click', '.cancel', function (e) {
        e.preventDefault();
        if (nNew) {
            oTable.fnDeleteRow(nEditing);
            nEditing = null;
            nNew = false;
        } else {
            restoreRow(oTable, nEditing);
            nEditing = null;
        }
    });

    table.on('click', '.edit', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var nRow = $(this).parents('tr')[0];

        if (nEditing !== null && nEditing != nRow) {
            /* Currently editing - but not this row - restore the old before continuing to edit mode */
            restoreRow(oTable, nEditing);
            editRow(oTable, nRow);
            nEditing = nRow;
        } else if (nEditing == nRow && this.innerHTML == "OK") {
            /* Editing this row and want to save it */
            saveRow(oTable, nEditing);
            nEditing = null;
            //alert("Updated! Do not forget to do some ajax to sync with backend :)");
        } else {
            /* No edit in progress - let's start one */
            editRow(oTable, nRow);
            nEditing = nRow;
        }
    });
}

function TableLineLines() {

    function restoreRow(oTable, nRow) {
        var aData = oTable.fnGetData(nRow);
        var jqTds = $('>td', nRow);

        for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
            oTable.fnUpdate(aData[i], nRow, i, false);
        }

        oTable.fnDraw();
    }

    function editRow(oTable, nRow) {
        var aData = oTable.fnGetData(nRow);
        var jqTds = $('>td', nRow);
        jqTds[0].innerHTML = aData[0]

        jqTds[1].innerHTML = '<select class="form-control"> <option selected="selected" value="Enegia Ativa Simples">Enegia Ativa Simples</option><option value="Enegia Ativa Ponta">Enegia Ativa Ponta</option><option value="Energia Ativa Cheia">Energia Ativa Cheia</option></select>';
        // jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[1] + '">';
        jqTds[2].innerHTML = '<a class="btn default btn-xs red-flamingo-stripe edit" href="">OK</a> <a class="btn default btn-xs red-flamingo-stripe cancel" href="">Cancelar</a>';
    }

    function saveRow(oTable, nRow) {
        var jqInputs = $('input', nRow);
        var jqSelects = $('select', nRow);
        oTable.fnUpdate(jqSelects[0].value, nRow, 1, false);
        // oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
        oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i> Editar </a>', nRow, 2, false);
        oTable.fnDraw();
    }

    function cancelEditRow(oTable, nRow) {
        var jqInputs = $('input', nRow);
        oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
        oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
        oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i>Editar </a>', nRow, 2, false);
        oTable.fnDraw();
    }

    var table = $('#table-line-lines');

    var oTable = table.dataTable({

        // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
        // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
        // So when dropdowns used the scrollable div should be removed.
        "dom": "t",
        "columnDefs": [
                    { "width": "33%", "targets": [0, 1, 2] },
                    { "className": "dt-right", "targets": 2 }

        ],
        "orderCellsTop": false,
        // Or you can use remote translation file
        //"language": {
        //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
        //},

    });



    var nEditing = null;
    var nNew = false;

    $('#table-line-lines_new').click(function (e) {
        e.preventDefault();

        if (nNew && nEditing) {
            if (confirm("Previose row not saved. Do you want to save it ?")) {
                saveRow(oTable, nEditing); // save
                $(nEditing).find("td:first").html("Untitled");
                nEditing = null;
                nNew = false;

            } else {
                oTable.fnDeleteRow(nEditing); // cancel
                nEditing = null;
                nNew = false;
                return;
            }
        }

        var aiNew = oTable.fnAddData(['', '', '']);
        var nRow = oTable.fnGetNodes(aiNew[0]);
        editRow(oTable, nRow);
        nEditing = nRow;
        nNew = true;
    });

    table.on('click', '.delete', function (e) {
        e.preventDefault();

        if (confirm("Are you sure to delete this row ?") == false) {
            return;
        }

        var nRow = $(this).parents('tr')[0];
        oTable.fnDeleteRow(nRow);
        //alert("Deleted! Do not forget to do some ajax to sync with backend :)");
    });

    table.on('click', '.cancel', function (e) {
        e.preventDefault();
        if (nNew) {
            oTable.fnDeleteRow(nEditing);
            nEditing = null;
            nNew = false;
        } else {
            restoreRow(oTable, nEditing);
            nEditing = null;
        }
    });

    table.on('click', '.edit', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var nRow = $(this).parents('tr')[0];

        if (nEditing !== null && nEditing != nRow) {
            /* Currently editing - but not this row - restore the old before continuing to edit mode */
            restoreRow(oTable, nEditing);
            editRow(oTable, nRow);
            nEditing = nRow;
        } else if (nEditing == nRow && this.innerHTML == "OK") {
            /* Editing this row and want to save it */
            saveRow(oTable, nEditing);
            nEditing = null;
            //alert("Updated! Do not forget to do some ajax to sync with backend :)");
        } else {
            /* No edit in progress - let's start one */
            editRow(oTable, nRow);
            nEditing = nRow;
        }
    });
}


function TableDocuments() {

    function restoreRow(oTable, nRow) {
        var aData = oTable.fnGetData(nRow);
        var jqTds = $('>td', nRow);

        for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
            oTable.fnUpdate(aData[i], nRow, i, false);
        }

        oTable.fnDraw();
    }

    function editRow(oTable, nRow) {
        var aData = oTable.fnGetData(nRow);
        var jqTds = $('>td', nRow);
        //jqTds[0].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[0] + '">';
        jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[1] + '">';
        //jqTds[2].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[2] + '">';
        //jqTds[3].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[3] + '">';
        //jqTds[4].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[4] + '">';
        jqTds[2].innerHTML = '<a class="btn default btn-xs red-flamingo-stripe edit" href="">OK</a> <a class="btn default btn-xs red-flamingo-stripe cancel" href="">Cancelar</a>';
    }

    function saveRow(oTable, nRow) {
        var jqInputs = $('input', nRow);

        oTable.fnUpdate(jqInputs[0].value, nRow, 1, false);
        //oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
        //oTable.fnUpdate(jqInputs[3].value, nRow, 2, false);
        //oTable.fnUpdate(jqInputs[4].value, nRow, 0, false);
        oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i> Editar </a>', nRow, 2, false);
        oTable.fnDraw();
    }

    function cancelEditRow(oTable, nRow) {
        var jqInputs = $('input', nRow);
        oTable.fnUpdate(jqInputs[0].value, nRow, 1, false);
        //oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
        //oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
        //oTable.fnUpdate(jqInputs[4].value, nRow, 4, false);
        oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i>Editar </a>', nRow, 2, false);
        oTable.fnDraw();
    }

    var table = $('#table-documento');

    var oTable = table.dataTable({

        // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
        // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
        // So when dropdowns used the scrollable div should be removed.
        "dom": "t",
        "columnDefs": [
            { "width": "30%", "targets": [0, 1] },
            { "className": "dt-right", "targets": 2 }

        ],
        "orderCellsTop": false,
        // Or you can use remote translation file
        //"language": {
        //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
        //},

    });



    var nEditing = null;
    var nNew = false;

    $('#table-documento_new').click(function (e) {
        e.preventDefault();

        if (nNew && nEditing) {
            if (confirm("Previose row not saved. Do you want to save it ?")) {
                saveRow(oTable, nEditing); // save
                $(nEditing).find("td:first").html("Untitled");
                nEditing = null;
                nNew = false;

            } else {
                oTable.fnDeleteRow(nEditing); // cancel
                nEditing = null;
                nNew = false;
                return;
            }
        }

        var aiNew = oTable.fnAddData(['', '']);
        var nRow = oTable.fnGetNodes(aiNew[0]);
        editRow(oTable, nRow);
        nEditing = nRow;
        nNew = true;
    });

    table.on('click', '.delete', function (e) {
        e.preventDefault();

        if (confirm("Are you sure to delete this row ?") == false) {
            return;
        }

        var nRow = $(this).parents('tr')[0];
        oTable.fnDeleteRow(nRow);
        //alert("Deleted! Do not forget to do some ajax to sync with backend :)");
    });

    table.on('click', '.cancel', function (e) {
        e.preventDefault();
        if (nNew) {
            oTable.fnDeleteRow(nEditing);
            nEditing = null;
            nNew = false;
        } else {
            restoreRow(oTable, nEditing);
            nEditing = null;
        }
    });

    table.on('click', '.edit', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var nRow = $(this).parents('tr')[0];

        if (nEditing !== null && nEditing != nRow) {
            /* Currently editing - but not this row - restore the old before continuing to edit mode */
            restoreRow(oTable, nEditing);
            editRow(oTable, nRow);
            nEditing = nRow;
        } else if (nEditing == nRow && this.innerHTML == "OK") {
            /* Editing this row and want to save it */
            saveRow(oTable, nEditing);
            nEditing = null;
            //alert("Updated! Do not forget to do some ajax to sync with backend :)");
        } else {
            /* No edit in progress - let's start one */
            editRow(oTable, nRow);
            nEditing = nRow;
        }
    });
}

function TableTechnologies() {

    function restoreRow(oTable, nRow) {
        var aData = oTable.fnGetData(nRow);
        var jqTds = $('>td', nRow);

        for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
            oTable.fnUpdate(aData[i], nRow, i, false);
        }

        oTable.fnDraw();
    }

    function editRow(oTable, nRow) {
        var aData = oTable.fnGetData(nRow);
        var jqTds = $('>td', nRow);
        jqTds[0].innerHTML = '<input type="text" class="form-control " value="' + aData[0] + '">';
        jqTds[1].innerHTML = '<input type="text" class="form-control " value="' + aData[1] + '">';
        jqTds[2].innerHTML = '<a class="btn default btn-xs red-flamingo-stripe edit" href="">OK</a> <a class="btn default btn-xs red-flamingo-stripe cancel" href="">Cancelar</a>';
    }

    function saveRow(oTable, nRow) {
        var jqInputs = $('input', nRow);
        oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
        oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
        oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i> Editar </a><a href="javascript:;" class="btn default btn-xs red-flamingo-stripe delete"> <i class="fa fa-pencil"></i>Eliminar </a>', nRow, 2, false);
        oTable.fnDraw();
    }

    function cancelEditRow(oTable, nRow) {
        var jqInputs = $('input', nRow);
        oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
        oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
        oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i>Editar </a><a href="javascript:;" class="btn default btn-xs red-flamingo-stripe delete"> <i class="fa fa-pencil"></i>Eliminar </a>', nRow, 2, false);
        oTable.fnDraw();
    }

    var table = $('#table-tecnologias');

    var oTable = table.dataTable({

        // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
        // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
        // So when dropdowns used the scrollable div should be removed.
        "dom": "t",
        "columnDefs": [
                    { "width": "20%", "targets": [0, 2] },
                    { "width": "60%", "targets": 1 },
                    { "className": "dt-right", "targets": 2 }

        ],
        "orderCellsTop": false,
        // Or you can use remote translation file
        //"language": {
        //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
        //},

    });



    var nEditing = null;
    var nNew = false;

    $('#table-tecnologias_new').click(function (e) {
        e.preventDefault();

        if (nNew && nEditing) {
            if (confirm("Previose row not saved. Do you want to save it ?")) {
                saveRow(oTable, nEditing); // save
                $(nEditing).find("td:first").html("Untitled");
                nEditing = null;
                nNew = false;

            } else {
                oTable.fnDeleteRow(nEditing); // cancel
                nEditing = null;
                nNew = false;
                return;
            }
        }

        var aiNew = oTable.fnAddData(['', '', '']);
        var nRow = oTable.fnGetNodes(aiNew[0]);
        editRow(oTable, nRow);
        nEditing = nRow;
        nNew = true;
    });

    table.on('click', '.delete', function (e) {
        e.preventDefault();

        if (confirm("Are you sure to delete this row ?") == false) {
            return;
        }

        var nRow = $(this).parents('tr')[0];
        oTable.fnDeleteRow(nRow);
        //alert("Deleted! Do not forget to do some ajax to sync with backend :)");
    });

    table.on('click', '.cancel', function (e) {
        e.preventDefault();
        if (nNew) {
            oTable.fnDeleteRow(nEditing);
            nEditing = null;
            nNew = false;
        } else {
            restoreRow(oTable, nEditing);
            nEditing = null;
        }
    });

    table.on('click', '.edit', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var nRow = $(this).parents('tr')[0];

        if (nEditing !== null && nEditing != nRow) {
            /* Currently editing - but not this row - restore the old before continuing to edit mode */
            restoreRow(oTable, nEditing);
            editRow(oTable, nRow);
            nEditing = nRow;
        } else if (nEditing == nRow && this.innerHTML == "OK") {
            /* Editing this row and want to save it */
            saveRow(oTable, nEditing);
            nEditing = null;
            //alert("Updated! Do not forget to do some ajax to sync with backend :)");
        } else {
            /* No edit in progress - let's start one */
            editRow(oTable, nRow);
            nEditing = nRow;
        }
    });
}

function TablePowers() {

    function restoreRow(oTable, nRow) {
        var aData = oTable.fnGetData(nRow);
        var jqTds = $('>td', nRow);

        for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
            oTable.fnUpdate(aData[i], nRow, i, false);
        }

        oTable.fnDraw();
    }

    function editRow(oTable, nRow) {
        var aData = oTable.fnGetData(nRow);
        var jqTds = $('>td', nRow);
        jqTds[0].innerHTML = '<input type="text" class="form-control " value="' + aData[0] + '">';
        //jqTds[1].innerHTML = '<input type="text" class="form-control " value="' + aData[1] + '">';
        jqTds[2].innerHTML = '<a class="btn default btn-xs red-flamingo-stripe edit" href="">OK</a> <a class="btn default btn-xs red-flamingo-stripe cancel" href="">Cancelar</a>';
    }

    function saveRow(oTable, nRow) {
        var jqInputs = $('input', nRow);
        oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
        //oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
        oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i> Editar </a><a href="javascript:;" class="btn default btn-xs red-flamingo-stripe delete"> <i class="fa fa-pencil"></i>Eliminar </a>', nRow, 2, false);
        oTable.fnDraw();
    }

    function cancelEditRow(oTable, nRow) {
        var jqInputs = $('input', nRow);
        oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
        //oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
        oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i>Editar </a><a href="javascript:;" class="btn default btn-xs red-flamingo-stripe delete"> <i class="fa fa-pencil"></i>Eliminar </a>', nRow, 2, false);
        oTable.fnDraw();
    }

    var table = $('#table-potencias');

    var oTable = table.dataTable({

        // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
        // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
        // So when dropdowns used the scrollable div should be removed.
        "dom": "t",
        "columnDefs": [
                    { "width": "10%", "targets": [0, 1] },
                    { "className": "dt-right", "targets": 2 }

        ],
        "orderCellsTop": false,
        // Or you can use remote translation file
        //"language": {
        //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
        //},

    });



    var nEditing = null;
    var nNew = false;

    $('#table-potencias_new').click(function (e) {
        e.preventDefault();

        if (nNew && nEditing) {
            if (confirm("Previose row not saved. Do you want to save it ?")) {
                saveRow(oTable, nEditing); // save
                $(nEditing).find("td:first").html("Untitled");
                nEditing = null;
                nNew = false;

            } else {
                oTable.fnDeleteRow(nEditing); // cancel
                nEditing = null;
                nNew = false;
                return;
            }
        }

        var aiNew = oTable.fnAddData(['', 'kVA', '']);
        var nRow = oTable.fnGetNodes(aiNew[0]);
        editRow(oTable, nRow);
        nEditing = nRow;
        nNew = true;
    });

    table.on('click', '.delete', function (e) {
        e.preventDefault();

        if (confirm("Are you sure to delete this row ?") == false) {
            return;
        }

        var nRow = $(this).parents('tr')[0];
        oTable.fnDeleteRow(nRow);
        //alert("Deleted! Do not forget to do some ajax to sync with backend :)");
    });

    table.on('click', '.cancel', function (e) {
        e.preventDefault();
        if (nNew) {
            oTable.fnDeleteRow(nEditing);
            nEditing = null;
            nNew = false;
        } else {
            restoreRow(oTable, nEditing);
            nEditing = null;
        }
    });

    table.on('click', '.edit', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var nRow = $(this).parents('tr')[0];

        if (nEditing !== null && nEditing != nRow) {
            /* Currently editing - but not this row - restore the old before continuing to edit mode */
            restoreRow(oTable, nEditing);
            editRow(oTable, nRow);
            nEditing = nRow;
        } else if (nEditing == nRow && this.innerHTML == "OK") {
            /* Editing this row and want to save it */
            saveRow(oTable, nEditing);
            nEditing = null;
            //alert("Updated! Do not forget to do some ajax to sync with backend :)");
        } else {
            /* No edit in progress - let's start one */
            editRow(oTable, nRow);
            nEditing = nRow;
        }
    });
}

function TableIVA() {

    function restoreRow(oTable, nRow) {
        var aData = oTable.fnGetData(nRow);
        var jqTds = $('>td', nRow);

        for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
            oTable.fnUpdate(aData[i], nRow, i, false);
        }

        oTable.fnDraw();
    }

    function editRow(oTable, nRow) {
        var aData = oTable.fnGetData(nRow);
        var jqTds = $('>td', nRow);
        jqTds[0].innerHTML = '<input type="text" class="form-control " value="' + aData[0] + '">';
        jqTds[2].innerHTML = '<select class="form-control"> <option selected="selected" value="Isento">Isento</option><option value="Taxa Reduzida">Taxa Reduzida</option><option value="Taxa Maxima">Taxa Maxima</option></select>';
        jqTds[3].innerHTML = '<a class="btn default btn-xs red-flamingo-stripe edit" href="">OK</a> <a class="btn default btn-xs red-flamingo-stripe cancel" href="">Cancelar</a>';
    }

    function saveRow(oTable, nRow) {
        var jqInputs = $('input', nRow);
        var jqSelects = $('select', nRow);
        oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
        oTable.fnUpdate(jqSelects[0].value, nRow, 2, false);
        oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i> Editar </a><a href="javascript:;" class="btn default btn-xs red-flamingo-stripe delete"> <i class="fa fa-pencil"></i>Eliminar </a>', nRow, 3, false);
        oTable.fnDraw();
    }

    function cancelEditRow(oTable, nRow) {
        var jqInputs = $('input', nRow);
        oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
        oTable.fnUpdate(jqInputs[2].value, nRow, 0, false);
        oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i>Editar </a><a href="javascript:;" class="btn default btn-xs red-flamingo-stripe delete"> <i class="fa fa-pencil"></i>Eliminar </a>', nRow, 3, false);
        oTable.fnDraw();
    }

    var table = $('#table-ivas');

    var oTable = table.dataTable({

        // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
        // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
        // So when dropdowns used the scrollable div should be removed.
        "dom": "t",
        "columnDefs": [
                   { "width": "10%", "targets": [0, 1] },
                    { "width": "30%", "targets": [2] },
                   { "className": "dt-right", "targets": 3 }

        ],
        "orderCellsTop": false,
        // Or you can use remote translation file
        //"language": {
        //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
        //},

    });



    var nEditing = null;
    var nNew = false;

    $('#table-ivas_new').click(function (e) {
        e.preventDefault();

        if (nNew && nEditing) {
            if (confirm("Previose row not saved. Do you want to save it ?")) {
                saveRow(oTable, nEditing); // save
                $(nEditing).find("td:first").html("Untitled");
                nEditing = null;
                nNew = false;

            } else {
                oTable.fnDeleteRow(nEditing); // cancel
                nEditing = null;
                nNew = false;
                return;
            }
        }

        var aiNew = oTable.fnAddData(['', '%', '']);
        var nRow = oTable.fnGetNodes(aiNew[0]);
        editRow(oTable, nRow);
        nEditing = nRow;
        nNew = true;
    });

    table.on('click', '.delete', function (e) {
        e.preventDefault();

        if (confirm("Are you sure to delete this row ?") == false) {
            return;
        }

        var nRow = $(this).parents('tr')[0];
        oTable.fnDeleteRow(nRow);
        //alert("Deleted! Do not forget to do some ajax to sync with backend :)");
    });

    table.on('click', '.cancel', function (e) {
        e.preventDefault();
        if (nNew) {
            oTable.fnDeleteRow(nEditing);
            nEditing = null;
            nNew = false;
        } else {
            restoreRow(oTable, nEditing);
            nEditing = null;
        }
    });

    table.on('click', '.edit', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var nRow = $(this).parents('tr')[0];

        if (nEditing !== null && nEditing != nRow) {
            /* Currently editing - but not this row - restore the old before continuing to edit mode */
            restoreRow(oTable, nEditing);
            editRow(oTable, nRow);
            nEditing = nRow;
        } else if (nEditing == nRow && this.innerHTML == "OK") {
            /* Editing this row and want to save it */
            saveRow(oTable, nEditing);
            nEditing = null;
            //alert("Updated! Do not forget to do some ajax to sync with backend :)");
        } else {
            /* No edit in progress - let's start one */
            editRow(oTable, nRow);
            nEditing = nRow;
        }
    });
}

function TableDesvios() {

    function restoreRow(oTable, nRow) {
        var aData = oTable.fnGetData(nRow);
        var jqTds = $('>td', nRow);

        for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
            oTable.fnUpdate(aData[i], nRow, i, false);
        }

        oTable.fnDraw();
    }

    function editRow(oTable, nRow) {
        var aData = oTable.fnGetData(nRow);
        var jqTds = $('>td', nRow);
        var jqSpans = $('span', nRow);
        $.each(jqSpans, function (index, value) {
            Configuration.SpanToInput($(value));
        });
        //jqTds[0].innerHTML = '<input type="text" class="form-control " value="' + aData[0] + '">';
        jqTds[1].innerHTML = '<a class="btn default btn-xs red-flamingo-stripe edit" href="">OK</a> <a class="btn default btn-xs red-flamingo-stripe cancel" href="">Cancelar</a>';
    }

    function saveRow(oTable, nRow) {
        var jqInputs = $('input', nRow);
        var jqTds = $('>td', nRow);
        //oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
        $.each(jqInputs, function (index, value) {
            Configuration.InputToSpan($(value));
        });
        oTable.fnUpdate(jqTds[0].innerHTML, nRow, 0, false);
        oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i> Editar </a>', nRow, 1, false);
        oTable.fnDraw();
    }

    function cancelEditRow(oTable, nRow) {
        var jqInputs = $('input', nRow);
        oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
        oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i>Editar </a>', nRow, 1, false);
        oTable.fnDraw();
    }

    var table = $('#table-desvios');

    var oTable = table.dataTable({

        // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
        // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
        // So when dropdowns used the scrollable div should be removed.
        "dom": "t",
        "columnDefs": [
                  { "width": "75%", "targets": 0 },
                  { "className": "dt-right", "targets": 1 }

        ],
        "order": [[1, "desc"]],
        "orderCellsTop": false,
        // Or you can use remote translation file
        //"language": {
        //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
        //},

    });



    var nEditing = null;
    var nNew = false;

    $('#table-desvios_new').click(function (e) {
        e.preventDefault();

        if (nNew && nEditing) {
            if (confirm("Previose row not saved. Do you want to save it ?")) {
                saveRow(oTable, nEditing); // save
                $(nEditing).find("td:first").html("Untitled");
                nEditing = null;
                nNew = false;

            } else {
                oTable.fnDeleteRow(nEditing); // cancel
                nEditing = null;
                nNew = false;
                return;
            }
        }

        var aiNew = oTable.fnAddData(['', '%', '']);
        var nRow = oTable.fnGetNodes(aiNew[0]);
        editRow(oTable, nRow);
        nEditing = nRow;
        nNew = true;
    });

    table.on('click', '.delete', function (e) {
        e.preventDefault();

        if (confirm("Are you sure to delete this row ?") == false) {
            return;
        }

        var nRow = $(this).parents('tr')[0];
        oTable.fnDeleteRow(nRow);
        //alert("Deleted! Do not forget to do some ajax to sync with backend :)");
    });

    table.on('click', '.cancel', function (e) {
        e.preventDefault();
        if (nNew) {
            oTable.fnDeleteRow(nEditing);
            nEditing = null;
            nNew = false;
        } else {
            restoreRow(oTable, nEditing);
            nEditing = null;
        }
    });

    table.on('click', '.edit', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var nRow = $(this).parents('tr')[0];

        if (nEditing !== null && nEditing != nRow) {
            /* Currently editing - but not this row - restore the old before continuing to edit mode */
            restoreRow(oTable, nEditing);
            editRow(oTable, nRow);
            nEditing = nRow;
        } else if (nEditing == nRow && this.innerHTML == "OK") {
            /* Editing this row and want to save it */
            saveRow(oTable, nEditing);
            nEditing = null;
            //alert("Updated! Do not forget to do some ajax to sync with backend :)");
        } else {
            /* No edit in progress - let's start one */
            editRow(oTable, nRow);
            nEditing = nRow;
        }
    });
}

function TableInstalations() {

    function restoreRow(oTable, nRow) {
        var aData = oTable.fnGetData(nRow);
        var jqTds = $('>td', nRow);

        for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
            oTable.fnUpdate(aData[i], nRow, i, false);
        }

        oTable.fnDraw();
    }

    function editRow(oTable, nRow) {
        var aData = oTable.fnGetData(nRow);
        var jqTds = $('>td', nRow);
        jqTds[0].innerHTML = '<input type="text" class="form-control ol-width-125" value="' + aData[0] + '">';
        jqTds[1].innerHTML = '<input type="text" class="form-control ol-width-125" value="' + aData[1] + '">';
        jqTds[2].innerHTML = '<input type="text" class="form-control ol-width-125" value="' + aData[2] + '">';
        jqTds[3].innerHTML = '<input type="text" class="form-control ol-width-125" value="' + aData[3] + '">';
        jqTds[4].innerHTML = '<input type="text" class="form-control ol-width-125" value="' + aData[4] + '">';
        jqTds[5].innerHTML = '<input type="text" class="form-control ol-width-125" value="' + aData[5] + '">';
        jqTds[6].innerHTML = '<input type="text" class="form-control ol-width-125" value="' + aData[6] + '">';
        jqTds[7].innerHTML = '<a class="btn default btn-xs red-flamingo-stripe edit" href="">OK</a> <a class="btn default btn-xs red-flamingo-stripe cancel" href="">Cancelar</a>';
    }

    function saveRow(oTable, nRow) {
        var jqInputs = $('input', nRow);
        oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
        oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
        oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
        oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
        oTable.fnUpdate(jqInputs[4].value, nRow, 4, false);
        oTable.fnUpdate(jqInputs[5].value, nRow, 5, false);
        oTable.fnUpdate(jqInputs[5].value, nRow, 6, false);
        oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i> Editar </a><a href="javascript:;" class="btn default btn-xs red-flamingo-stripe delete"> <i class="fa fa-pencil"></i>Eliminar </a>', nRow, 7, false);
        oTable.fnDraw();
    }

    function cancelEditRow(oTable, nRow) {
        var jqInputs = $('input', nRow);
        oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
        oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
        oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
        oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
        oTable.fnUpdate(jqInputs[4].value, nRow, 4, false);
        oTable.fnUpdate(jqInputs[5].value, nRow, 5, false);
        oTable.fnUpdate(jqInputs[5].value, nRow, 6, false);
        oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i>Editar </a><a href="javascript:;" class="btn default btn-xs red-flamingo-stripe delete"> <i class="fa fa-pencil"></i>Eliminar </a>', nRow, 7, false);
        oTable.fnDraw();
    }

    var table = $('#table-instalacao');

    var oTable = table.dataTable({

        // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
        // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
        // So when dropdowns used the scrollable div should be removed.
        "dom": "t",
        "columnDefs": [
                  { "width": "15%", "targets": [7] },
                  { "className": "dt-right", "targets": 7 }

        ],
        "orderCellsTop": false,
        // Or you can use remote translation file
        //"language": {
        //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
        //},

    });



    var nEditing = null;
    var nNew = false;

    $('#table-instalacao_new').click(function (e) {
        e.preventDefault();

        if (nNew && nEditing) {
            if (confirm("Previose row not saved. Do you want to save it ?")) {
                saveRow(oTable, nEditing); // save
                $(nEditing).find("td:first").html("Untitled");
                nEditing = null;
                nNew = false;

            } else {
                oTable.fnDeleteRow(nEditing); // cancel
                nEditing = null;
                nNew = false;
                return;
            }
        }

        var aiNew = oTable.fnAddData(['', '', '', '', '', '', '', '']);
        var nRow = oTable.fnGetNodes(aiNew[0]);
        editRow(oTable, nRow);
        nEditing = nRow;
        nNew = true;
    });

    table.on('click', '.delete', function (e) {
        e.preventDefault();

        if (confirm("Are you sure to delete this row ?") == false) {
            return;
        }

        var nRow = $(this).parents('tr')[0];
        oTable.fnDeleteRow(nRow);
        //alert("Deleted! Do not forget to do some ajax to sync with backend :)");
    });

    table.on('click', '.cancel', function (e) {
        e.preventDefault();
        if (nNew) {
            oTable.fnDeleteRow(nEditing);
            nEditing = null;
            nNew = false;
        } else {
            restoreRow(oTable, nEditing);
            nEditing = null;
        }
    });

    table.on('click', '.edit', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var nRow = $(this).parents('tr')[0];

        if (nEditing !== null && nEditing != nRow) {
            /* Currently editing - but not this row - restore the old before continuing to edit mode */
            restoreRow(oTable, nEditing);
            editRow(oTable, nRow);
            nEditing = nRow;
        } else if (nEditing == nRow && this.innerHTML == "OK") {
            /* Editing this row and want to save it */
            saveRow(oTable, nEditing);
            nEditing = null;
            //alert("Updated! Do not forget to do some ajax to sync with backend :)");
        } else {
            /* No edit in progress - let's start one */
            editRow(oTable, nRow);
            nEditing = nRow;
        }
    });
}


function TableTranslations() {

    function restoreRow(oTable, nRow) {
        var aData = oTable.fnGetData(nRow);
        var jqTds = $('>td', nRow);

        for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
            oTable.fnUpdate(aData[i], nRow, i, false);
        }

        oTable.fnDraw();
    }

    function editRow(oTable, nRow) {
        var aData = oTable.fnGetData(nRow);
        var jqTds = $('>td', nRow);
        jqTds[0].innerHTML = GetCountries();
        jqTds[1].innerHTML = '';
        jqTds[2].innerHTML = '<a class="btn default btn-xs red-flamingo-stripe edit" href="">OK</a> <a class="btn default btn-xs red-flamingo-stripe cancel" href="">Cancelar</a>';
        enableSelect();
    }

    function saveRow(oTable, nRow) {
        var jqInputs = $('input', nRow);
        var jqTds = $('>td', nRow);
        oTable.fnUpdate(($("#s2id_" + "language-select span")[0]).innerHTML, nRow, 0, false);
        oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe"> <i class="fa fa-pencil"></i> Traduzir </a>', nRow, 1, false); // $("#language-select").val()
        oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe "> <i class="fa fa-pencil"></i> Ativar </a> <a href="javascript:;" class="btn default btn-xs red-flamingo-stripe delete"> <i class="fa fa-pencil"></i>Eliminar </a>', nRow, 2, false);
        oTable.fnDraw();
        
    }

    function cancelEditRow(oTable, nRow) {
        var jqInputs = $('input', nRow);
        oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
        oTable.fnUpdate(jqTds[2].innerHTML, nRow, 2, false);
        //oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
        //oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i>Editar </a><a href="javascript:;" class="btn default btn-xs red-flamingo-stripe delete"> <i class="fa fa-pencil"></i>Eliminar </a>', nRow, 2, false);
        oTable.fnDraw();
    }

    var table = $('#table-translatios');

    var oTable = table.dataTable({

        // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
        // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
        // So when dropdowns used the scrollable div should be removed.
        "dom": "t",
        "columnDefs": [
                    { "width": "40%", "targets": [0] },
                    { "width": "20%", "targets": [1] },
                    { "className": "dt-right", "targets": 2 }

        ],
        "orderCellsTop": false,
        // Or you can use remote translation file
        //"language": {
        //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
        //},

    });



    var nEditing = null;
    var nNew = false;

    $('#table-translatios_new').click(function (e) {
        e.preventDefault();

        if (nNew && nEditing) {
            if (confirm("Previose row not saved. Do you want to save it ?")) {
                saveRow(oTable, nEditing); // save
                $(nEditing).find("td:first").html("Untitled");
                nEditing = null;
                nNew = false;

            } else {
                oTable.fnDeleteRow(nEditing); // cancel
                nEditing = null;
                nNew = false;
                return;
            }
        }

        var aiNew = oTable.fnAddData(['', '','']);
        var nRow = oTable.fnGetNodes(aiNew[0]);
        editRow(oTable, nRow);
        nEditing = nRow;
        nNew = true;
    });

    table.on('click', '.delete', function (e) {
        e.preventDefault();

        if (confirm("Are you sure to delete this row ?") == false) {
            return;
        }

        var nRow = $(this).parents('tr')[0];
        oTable.fnDeleteRow(nRow);
        //alert("Deleted! Do not forget to do some ajax to sync with backend :)");
    });

    table.on('click', '.cancel', function (e) {
        e.preventDefault();
        if (nNew) {
            oTable.fnDeleteRow(nEditing);
            nEditing = null;
            nNew = false;
        } else {
            restoreRow(oTable, nEditing);
            nEditing = null;
        }
    });

    table.on('click', '.edit', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var nRow = $(this).parents('tr')[0];

        if (nEditing !== null && nEditing != nRow) {
            /* Currently editing - but not this row - restore the old before continuing to edit mode */
            restoreRow(oTable, nEditing);
            editRow(oTable, nRow);
            nEditing = nRow;
        } else if (nEditing == nRow && this.innerHTML == "OK") {
            /* Editing this row and want to save it */
            saveRow(oTable, nEditing);
            nEditing = null;
            //alert("Updated! Do not forget to do some ajax to sync with backend :)");
        } else {
            /* No edit in progress - let's start one */
            editRow(oTable, nRow);
            nEditing = nRow;
        }
    });
}



function GetCountries() {
    return "<select name='' id='language-select' class='form-control select2'> <option value='AF'>Afghanistan</option> <option value='AL'>Albania</option> <option value='DZ'>Algeria</option> <option value='AS'>American Samoa</option> <option value='AD'>Andorra</option> <option value='AO'>Angola</option> <option value='AI'>Anguilla</option> <option value='AR'>Argentina</option> <option value='AM'>Armenia</option> <option value='AW'>Aruba</option> <option value='AU'>Australia</option> <option value='AT'>Austria</option> <option value='AZ'>Azerbaijan</option> <option value='BS'>Bahamas</option> <option value='BH'>Bahrain</option> <option value='BD'>Bangladesh</option> <option value='BB'>Barbados</option> <option value='BY'>Belarus</option> <option value='BE'>Belgium</option> <option value='BZ'>Belize</option> <option value='BJ'>Benin</option> <option value='BM'>Bermuda</option> <option value='BT'>Bhutan</option> <option value='BO'>Bolivia</option> <option value='BA'>Bosnia and Herzegowina</option> <option value='BW'>Botswana</option> <option value='BV'>Bouvet Island</option> <option value='BR'>Brazil</option> <option value='IO'>British Indian Ocean Territory</option> <option value='BN'>Brunei Darussalam</option> <option value='BG'>Bulgaria</option> <option value='BF'>Burkina Faso</option> <option value='BI'>Burundi</option> <option value='KH'>Cambodia</option> <option value='CM'>Cameroon</option> <option value='CA'>Canada</option> <option value='CV'>Cape Verde</option> <option value='KY'>Cayman Islands</option> <option value='CF'>Central African Republic</option> <option value='TD'>Chad</option> <option value='CL'>Chile</option> <option value='CN'>China</option> <option value='CX'>Christmas Island</option> <option value='CC'>Cocos (Keeling) Islands</option> <option value='CO'>Colombia</option> <option value='KM'>Comoros</option> <option value='CG'>Congo</option> <option value='CD'>Congo, the Democratic Republic of the</option> <option value='CK'>Cook Islands</option> <option value='CR'>Costa Rica</option> <option value='CI'>Cote d'Ivoire</option> <option value='HR'>Croatia (Hrvatska)</option> <option value='CU'>Cuba</option> <option value='CY'>Cyprus</option> <option value='CZ'>Czech Republic</option> <option value='DK'>Denmark</option> <option value='DJ'>Djibouti</option> <option value='DM'>Dominica</option> <option value='DO'>Dominican Republic</option> <option value='EC'>Ecuador</option> <option value='EG'>Egypt</option> <option value='SV'>El Salvador</option> <option value='GQ'>Equatorial Guinea</option> <option value='ER'>Eritrea</option> <option value='EE'>Estonia</option> <option value='ET'>Ethiopia</option> <option value='FK'>Falkland Islands (Malvinas)</option> <option value='FO'>Faroe Islands</option> <option value='FJ'>Fiji</option> <option value='FI'>Finland</option> <option value='FR'>France</option> <option value='GF'>French Guiana</option> <option value='PF'>French Polynesia</option> <option value='TF'>French Southern Territories</option> <option value='GA'>Gabon</option> <option value='GM'>Gambia</option> <option value='GE'>Georgia</option> <option value='DE'>Germany</option> <option value='GH'>Ghana</option> <option value='GI'>Gibraltar</option> <option value='GR'>Greece</option> <option value='GL'>Greenland</option> <option value='GD'>Grenada</option> <option value='GP'>Guadeloupe</option> <option value='GU'>Guam</option> <option value='GT'>Guatemala</option> <option value='GN'>Guinea</option> <option value='GW'>Guinea-Bissau</option> <option value='GY'>Guyana</option> <option value='HT'>Haiti</option> <option value='HM'>Heard and Mc Donald Islands</option> <option value='VA'>Holy See (Vatican City State)</option> <option value='HN'>Honduras</option> <option value='HK'>Hong Kong</option> <option value='HU'>Hungary</option> <option value='IS'>Iceland</option> <option value='IN'>India</option> <option value='ID'>Indonesia</option> <option value='IR'>Iran (Islamic Republic of)</option> <option value='IQ'>Iraq</option> <option value='IE'>Ireland</option> <option value='IL'>Israel</option> <option value='IT'>Italy</option> <option value='JM'>Jamaica</option> <option value='JP'>Japan</option> <option value='JO'>Jordan</option> <option value='KZ'>Kazakhstan</option> <option value='KE'>Kenya</option> <option value='KI'>Kiribati</option> <option value='KP'>Korea, Democratic People's Republic of</option> <option value='KR'>Korea, Republic of</option> <option value='KW'>Kuwait</option> <option value='KG'>Kyrgyzstan</option> <option value='LA'>Lao People's Democratic Republic</option> <option value='LV'>Latvia</option> <option value='LB'>Lebanon</option> <option value='LS'>Lesotho</option> <option value='LR'>Liberia</option> <option value='LY'>Libyan Arab Jamahiriya</option> <option value='LI'>Liechtenstein</option> <option value='LT'>Lithuania</option> <option value='LU'>Luxembourg</option> <option value='MO'>Macau</option> <option value='MK'>Macedonia, The Former Yugoslav Republic of</option> <option value='MG'>Madagascar</option> <option value='MW'>Malawi</option> <option value='MY'>Malaysia</option> <option value='MV'>Maldives</option> <option value='ML'>Mali</option> <option value='MT'>Malta</option> <option value='MH'>Marshall Islands</option> <option value='MQ'>Martinique</option> <option value='MR'>Mauritania</option> <option value='MU'>Mauritius</option> <option value='YT'>Mayotte</option> <option value='MX'>Mexico</option> <option value='FM'>Micronesia, Federated States of</option> <option value='MD'>Moldova, Republic of</option> <option value='MC'>Monaco</option> <option value='MN'>Mongolia</option> <option value='MS'>Montserrat</option> <option value='MA'>Morocco</option> <option value='MZ'>Mozambique</option> <option value='MM'>Myanmar</option> <option value='NA'>Namibia</option> <option value='NR'>Nauru</option> <option value='NP'>Nepal</option> <option value='NL'>Netherlands</option> <option value='AN'>Netherlands Antilles</option> <option value='NC'>New Caledonia</option> <option value='NZ'>New Zealand</option> <option value='NI'>Nicaragua</option> <option value='NE'>Niger</option> <option value='NG'>Nigeria</option> <option value='NU'>Niue</option> <option value='NF'>Norfolk Island</option> <option value='MP'>Northern Mariana Islands</option> <option value='NO'>Norway</option> <option value='OM'>Oman</option> <option value='PK'>Pakistan</option> <option value='PW'>Palau</option> <option value='PA'>Panama</option> <option value='PG'>Papua New Guinea</option> <option value='PY'>Paraguay</option> <option value='PE'>Peru</option> <option value='PH'>Philippines</option> <option value='PN'>Pitcairn</option> <option value='PL'>Poland</option> <option value='PT'>Portugal</option> <option value='PR'>Puerto Rico</option> <option value='QA'>Qatar</option> <option value='RE'>Reunion</option> <option value='RO'>Romania</option> <option value='RU'>Russian Federation</option> <option value='RW'>Rwanda</option> <option value='KN'>Saint Kitts and Nevis</option> <option value='LC'>Saint LUCIA</option> <option value='VC'>Saint Vincent and the Grenadines</option> <option value='WS'>Samoa</option> <option value='SM'>San Marino</option> <option value='ST'>Sao Tome and Principe</option> <option value='SA'>Saudi Arabia</option> <option value='SN'>Senegal</option> <option value='SC'>Seychelles</option> <option value='SL'>Sierra Leone</option> <option value='SG'>Singapore</option> <option value='SK'>Slovakia (Slovak Republic)</option> <option value='SI'>Slovenia</option> <option value='SB'>Solomon Islands</option> <option value='SO'>Somalia</option> <option value='ZA'>South Africa</option> <option value='GS'>South Georgia and the South Sandwich Islands</option> <option value='ES'>Spain</option> <option value='LK'>Sri Lanka</option> <option value='SH'>St. Helena</option> <option value='PM'>St. Pierre and Miquelon</option> <option value='SD'>Sudan</option> <option value='SR'>Suriname</option> <option value='SJ'>Svalbard and Jan Mayen Islands</option> <option value='SZ'>Swaziland</option> <option value='SE'>Sweden</option> <option value='CH'>Switzerland</option> <option value='SY'>Syrian Arab Republic</option> <option value='TW'>Taiwan, Province of China</option> <option value='TJ'>Tajikistan</option> <option value='TZ'>Tanzania, United Republic of</option> <option value='TH'>Thailand</option> <option value='TG'>Togo</option> <option value='TK'>Tokelau</option> <option value='TO'>Tonga</option> <option value='TT'>Trinidad and Tobago</option> <option value='TN'>Tunisia</option> <option value='TR'>Turkey</option> <option value='TM'>Turkmenistan</option> <option value='TC'>Turks and Caicos Islands</option> <option value='TV'>Tuvalu</option> <option value='UG'>Uganda</option> <option value='UA'>Ukraine</option> <option value='AE'>United Arab Emirates</option> <option value='GB'>United Kingdom</option> <option value='US'>United States</option> <option value='UM'>United States Minor Outlying Islands</option> <option value='UY'>Uruguay</option> <option value='UZ'>Uzbekistan</option> <option value='VU'>Vanuatu</option> <option value='VE'>Venezuela</option> <option value='VN'>Viet Nam</option> <option value='VG'>Virgin Islands (British)</option> <option value='VI'>Virgin Islands (U.S.)</option> <option value='WF'>Wallis and Futuna Islands</option> <option value='EH'>Western Sahara</option> <option value='YE'>Yemen</option> <option value='ZM'>Zambia</option> <option value='ZW'>Zimbabwe</option> </select>";
}


function format(state) {
    if (!state.id) return state.text; // optgroup
    return "<img class='flag' src='" + "assets/global/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
}
function enableSelect() {
    $("#language-select").select2({
        placeholder: "Select a Country",
        allowClear: true,
        formatResult: format,
        formatSelection: format,
        escapeMarkup: function (m) {
            return m;
        }
    });
}


