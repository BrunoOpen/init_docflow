﻿UserDetails = function () {

    var select2_change = function (e) {
        var selected = $('#select2_roles').val();
        if (selected.length > 0) {
            var selector = '';
            $.each(selected, function (i, val) {
                selector = selector + ',[data-role-id=' + val + ']';
            });
            selector = selector.replace(',', '');
            $("#actions_table tbody tr").not($(selector).parents("tr").show()).hide();
        }
    }

    var _init = function () {

        $(".role-item").hide();
        var preselected = new Array();
        $(".pre-sel-rol").each(function (i, val) {
            preselected.push({ id: $(val).data('id'), text: $(val).data('name') });
            $(".role-item[data-role-id=" + $(val).data('id') + "]").show();
        });
        $('#select2_roles').select2({
            allowClear: true
        });
        if (preselected.length > 0) {
            $('#select2_roles').select2('data', preselected);
        }
        //select2_change();

        $('#select2_roles').select2('enable', false);


        UserDetails.Table = $('#actions_table').DataTable({
            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": Language.DATATABLE_LANGUAGE,

            "ordering": false,
            "searching": false,
            "paging": false,
            "info": false
        });

    }





    return {

        //main function to initiate the moduledata
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            _init();
            //   select2_change();

        }

    };

}();