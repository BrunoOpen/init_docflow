﻿UserEdit = function () {


    var _initSubmit = function () {
        $("#btnSaveUser").on('click', _submit);
        $(document).on('keypress', function (event) {
            if (event.which == 13)
                _submit(event);
        });
    };
    var _submit = function (event) {

        Loading();
        var formdata = $("#usersEditForm").serialize();

        var arr = new Array();
        if (!IsNullOrEmpty($('#select2_roles').val()))
            $.each($('#select2_roles').val(), function (i, val) {
                arr.push({ Id: val });
            });

        var actiongroups = new Array();
        $("select.variables[multiple]").each(function (i, o) {
            if ($(this).parent().is(':visible')) {
                var item = { Id: $(this).data('group-id'), Variables: new Array() };
                if (!IsNullOrEmpty($(this).val()))
                    $.each($(this).val(), function (index, obj) {
                        item.Variables.push({ Id: obj, Selected: true });
                    });
                actiongroups.push(item);
            }
        });


        $.ajax({
            type: "POST",
            url: "/Users/Save",
            data: {
                Id: $("#Id").val(),
                Roles: arr,
                Name: $("#Name").val(),
                Email: $("#Email").val(),
                Description: $("#Description").val(),
                Active: $("input[name=Active]").is(":checked"),
                Valid: $("input[name=Valid]").is(":checked"),
                ActionGroups: actiongroups //[{ Id: 1, Variables: [{ Id: 1, Selected: true }, { Id: 2, Selected: true }] }, { Id: 2 }],

            },
            success: function (a) {
                if (isNaN(a)) {
                    var message = '<div class="alert alert-danger">  <strong>' + Language.ACCOUNTS_RESET_PASSWORD_ERRORDATA + '</strong>';
                    for (var i = 0; i < a.length; i++) {
                        message = message + '<p></p>' + a;
                    }
                    message = message + '</div>';
                    $("#err-msg").html(message).show();

                    Utils.ScrolllTo($("#err-msg"));
                    StopLoading();
                }
                else {
                    window.location = '/Users/Details?id=' + a;
                }
            },
            error: function (a) {
                StopLoading();
            }
        });

    };

    var select2_change = function (e) {


        //$('#select2_roles').on('change', function (e) {

        //   $("#actions_table tbody tr").not($("[data-role-id=2]").parents("tr")).show()
        $(".role-item").hide();
        var selected = $('#select2_roles').val();
        if (!IsNullOrEmpty(selected) && selected.length > 0) {
            var selector = '';
            $.each(selected, function (i, val) {
                selector = selector + ',[data-role-id=' + val + ']';
                $(".role-item[data-role-id=" + val + "]").show();
            });
            selector = selector.replace(',', '');
            $("#actions_table tbody tr").not($(selector).parents("tr").show()).hide();

        } else {
            $("#actions_table tbody tr").hide();
        }



        //debugger;
        //var addedroles = e.added;
        //if (addedroles != undefined && addedroles.text == undefined) {

        //}
        //if (e.added != undefined && e.added.text != undefined) {

        //}
        //if (e.removed != undefined) {
        //    $('#div' + e.removed.text).remove();
        //    $('#table' + e.removed.text).remove();
        //}

        //});
    }
    var _init = function () {

        $(".role-item").hide();
        var preselected = new Array();
        $(".pre-sel-rol").each(function (i, val) {
            preselected.push({ id: $(val).data('id'), text: $(val).data('name') });
            $(".role-item[data-role-id=" + $(val).data('id') + "]").show();
        });
        $('#select2_roles').select2({
            allowClear: true
        });
        if (preselected.length > 0) {
            $('#select2_roles').select2('data', preselected);
        }
        select2_change();

        $('#select2_roles').on('change', select2_change);


        $(".row-details").on('click', function () {
            var id = $(this).closest('tr').attr('data-id');
            if ($(this).hasClass('row-details-close')) {
                $(this).removeClass('row-details-close');
                $(this).addClass('row-details-open');
                //expand

                $(".child-" + id).show();
            } else {
                $(this).removeClass('row-details-open');
                $(this).addClass('row-details-close');

                $(".child-" + id).hide();
                //colapse
            }
        });

        $('.check-parent').on('change', function () {
            var id = $(this).closest('tr').attr('data-id');
            if ($(this).is(":checked")) {

                //check-all
                $.uniform.update($(".check-child-" + id).prop('checked', true));

            } else {
                //uncheck-all
                $.uniform.update($(".check-child-" + id).removeProp('checked'));

            }
        });
        $('.check-child').on('change', function () {

            if ($(this).is(":checked")) {
                $.uniform.update($('.check-parent-' + $(this).closest('tr').data('parent-id')).prop('checked', true));
                //  $.uniform.update($(this).closest('tr').find('.check-parent').prop('checked', true));
                ////check-all
                //  $.uniform.update($(".check-child-" + id).prop('checked', true));

            } else {
                //uncheck-all
                //  $.uniform.update($(".check-child-" + id).removeProp('checked'));

            }
        });

        UserEdit.Table = $('#actions_table').DataTable({
            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": Language.DATATABLE_LANGUAGE,

            "ordering": false,
            "searching": false,
            "paging": false,
            "info": false
        });

        _initSubmit();

    }





    return {

        //main function to initiate the moduledata
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            _init();
            //   select2_change();

        }

    };

}();