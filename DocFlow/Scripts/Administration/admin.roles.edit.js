﻿var Roles = function () {

    var _initSubmit = function () {
        $("#btnSaveRole").on('click', _submit);
        $(document).on('keypress', function (event) {
            if (event.which == 13)
                _submit(event);
        });
    };
    var _submit = function (event) {

        Loading();
        var formdata = $("#rolesEditForm").serialize();

        var arr = new Array();
        $(".check:checked").each(function (i, val) {
            if ($(val).is(':checked'))
                arr.push({ Id: $(val).data('id') });
            //  formdata = formdata + '&Actions[' + i + '].Id=' + $(val).data('id');

        });


        $.ajax({
            type: "POST",
            url: "/Roles/Save",
            data: { Id: $("#RoleId").val(), ActionsGroups: arr, Name: $("#Name").val(), Description: $("#Description").val(), Active: $("input[name=Active]").is(":checked") },
            success: function (a) {
                window.location = '../Roles/Details?roleid=' + $("#RoleId").val();
            },
            error: function (a) {
                StopLoading();
            }
        });

    };

    return {


        init: function () {
            if (!jQuery().dataTable) {
                return;
            }
            _initSubmit();

            $(".row-details").on('click', function () {
                var id = $(this).closest('tr').attr('data-id');
                if ($(this).hasClass('row-details-close')) {
                    $(this).removeClass('row-details-close');
                    $(this).addClass('row-details-open');
                    //expand

                    $(".child-" + id).show();
                } else {
                    $(this).removeClass('row-details-open');
                    $(this).addClass('row-details-close');

                    $(".child-" + id).hide();
                    //colapse
                }
            });

            $('.check-parent').on('change', function () {
                var id = $(this).closest('tr').attr('data-id');
                if ($(this).is(":checked")) {

                    //check-all
                    $.uniform.update($(".check-child-" + id).prop('checked', true));

                } else {
                    //uncheck-all
                    $.uniform.update($(".check-child-" + id).removeProp('checked'));

                }
            });
            $('.check-child').on('change', function () {

                if ($(this).is(":checked")) {
                    $.uniform.update($('.check-parent-' + $(this).closest('tr').data('parent-id')).prop('checked', true));
                    //  $.uniform.update($(this).closest('tr').find('.check-parent').prop('checked', true));
                    ////check-all
                    //  $.uniform.update($(".check-child-" + id).prop('checked', true));

                } else {
                    //uncheck-all
                    //  $.uniform.update($(".check-child-" + id).removeProp('checked'));

                }
            });

            Roles.Table = $('#actions_table').DataTable({
                // Internationalisation. For more info refer to http://datatables.net/manual/i18n
                "language": Language.DATATABLE_LANGUAGE,

                "ordering": false,
                "searching": false,
                "paging": false,
                "info": false
            });


        }

    };

}();