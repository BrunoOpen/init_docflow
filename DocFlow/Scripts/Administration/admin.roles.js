﻿var Roles = function () {

    var _init = function () {

         $('#roles_table').DataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": Language.DATATABLE_LANGUAGE,

            "columnDefs": [{
                "orderable": false,
                "targets": [0]
            },
          
            ],
            "order": [
                [2, 'asc']
            ],
            "lengthMenu": [
                [5, 10, 15, 20, 40, 50, 100, -1],
                [5, 10, 15, 20, 40, 50, 100, Language.DATATABLE_ALL] // change per page values here
            ],
            // set the initial value
            "pageLength": 15

        });
    }


  

return {

    //main function to initiate the moduledata
    init: function () {
        if (!jQuery().dataTable) {
            return;
        }

        _init();
            
    }

};

}();