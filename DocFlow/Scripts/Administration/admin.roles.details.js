﻿var Roles = function () {

    var format = function (data) {

        var childs =  jQuery.grep(Roles.DataSource, function (a) {
            return a.ParentId == data.Id;
        });

        var html = '<table class="table  table-bordered table-hover"> ';
        $.each(childs, function (i, val) {
            html = html + '<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + val.Name + '</td>';
            html = html + '<td>' + val.Description + '</td>';
            html = html + '<td><input type="checkbox"   /></td></tr>';
        });
        return html + '</table>';
    }

    var _initTable = function (data) {

        Roles.Parents = new Array();
        $.each(data, function (i, val) {
            val.check = '<input type="checkbox"   />';
            if (val.ParentId == null)
                Roles.Parents.push(val);
        });

        Roles.DataSource = data;

        Roles.Table = $('#actions_table').DataTable({
            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": Language.DATATABLE_LANGUAGE,

            "columnDefs": [{
                "orderable": true,
                "targets": [0, 1, 2]
            },

            ],
            "order": [
                [2, 'asc']
            ],
            "paging": false,
            "lengthMenu": [
                [5, 10, 15, 20, 40, 50, 100, -1],
                [5, 10, 15, 20, 40, 50, 100, Language.DATATABLE_ALL] // change per page values here
            ],
            // set the initial value
            "pageLength": 15,
            data: Roles.Parents,
            columns: [
                { data: "Name" },
                { data: "Description" },
                { data: "check." }
            ],
            "rowCallback": function (row, data, index) {
                var table = this.api();
                table.row(row).child(format(data)).show();
            }

        });

        StopLoading();
    }

    var _getData = function () {
        Loading();
        $.ajax({
            type: "POST",
            url: "/Roles/GetAllActions",
            success: _initTable,
            error: function () { StopLoading(); }
        });

    }



    return {


        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            $(".row-details").on('click', function () {
                var id = $(this).closest('tr').attr('data-id');
                if ($(this).hasClass('row-details-close')) {
                    $(this).removeClass('row-details-close');
                    $(this).addClass('row-details-open');
                    //expand

                    $(".child-" + id).show();
                } else {
                    $(this).removeClass('row-details-open');
                    $(this).addClass('row-details-close');

                    $(".child-" + id).hide();
                    //colapse
                }
            });

            $('.check-parent').on('change', function () {
                var id = $(this).closest('tr').attr('data-id');
                if ($(this).is(":checked")) {
                 
                    //check-all
                    $.uniform.update($(".check-child-" + id).prop('checked', true));
                   
                } else {
                    //uncheck-all
                    $.uniform.update($(".check-child-" + id).removeProp('checked'));

                }
            });
            $('.check-child').on('change', function () {

                if ($(this).is(":checked")) {
                    $.uniform.update($('.check-parent-' + $(this).closest('tr').data('parent-id')).prop('checked', true));
                  //  $.uniform.update($(this).closest('tr').find('.check-parent').prop('checked', true));
                    ////check-all
                  //  $.uniform.update($(".check-child-" + id).prop('checked', true));

                } else {
                    //uncheck-all
                  //  $.uniform.update($(".check-child-" + id).removeProp('checked'));

                }
            });

            Roles.Table = $('#actions_table').DataTable({
                // Internationalisation. For more info refer to http://datatables.net/manual/i18n
                "language": Language.DATATABLE_LANGUAGE,

                "ordering": false,
               
                "paging": false,
                "lengthMenu": [
                    [5, 10, 15, 20, 40, 50, 100, -1],
                    [5, 10, 15, 20, 40, 50, 100, Language.DATATABLE_ALL] // change per page values here
                ],
                // set the initial value
                "pageLength": 15
               

            });


            Roles.UsersTable = $('#user_table').dataTable({

                // Internationalisation. For more info refer to http://datatables.net/manual/i18n
                "language": Language.DATATABLE_LANGUAGE,
                "order": [
                    [0, 'asc']
                ],
                "lengthMenu": [
                    [5, 10, 15, 20, 40, 50, 100, -1],
                    [5, 10, 15, 20, 40, 50, 100, Language.DATATABLE_ALL] // change per page values here
                ],
                // set the initial value
                "pageLength": 5
            });
        }

    };

}();