﻿var Utils = new Object();
Utils.FullMonths = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
Utils.AbvrMonths = ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"];
/**
 * Stop an event.
 * @param {event} the event to be stoped.
 * @return {void} doesnt have return.
 */
Utils.StopEvent = function (event) {
    event.preventDefault();
    if (event.stopPropagation) {
        event.stopPropagation();
    }
    else if (window.event) {
        window.event.cancelBubble = true;
    }
}

Utils.GetQueryStringParam = function (name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

Utils.GetQueryStringParam2 = function (name, querystring) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(IsNullOrEmpty(querystring) ? location.search : querystring);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
//teste

Utils.RemoveQueryStringParam = function (url, parameter) {
    return url
        .replace(new RegExp('[?&]' + parameter + '=[^&#]*(#.*)?$'), '$1')
        .replace(new RegExp('([?&])' + parameter + '=[^&]*&'), '$1');
}

//Valida endereços
Utils.Regex = function (code, value) {

    switch (code) {
        case 'mail':
            //from http://emailregex.com/
            var regex = new RegExp("^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$");
            return regex.test(value);

        default:
            return false;
    }
}

// /Date(123424235325)/ -> Agosto 2015
Utils.JsonDateToMMYYYY = function (strDate) {

    var dt = Utils.JsonToDateTime(strDate);
    return Utils.FullMonths[dt.getMonth()] + ' ' + dt.getFullYear().toString();
}

Utils.DateToMMYYYY = function (date) {
    if (IsNullOrEmpty(date))
        return "";
    return Utils.FullMonths[date.getMonth()] + ' ' + date.getFullYear().toString();
}

Utils.JsonDateToShortMMYYYY = function (strDate) {
    var dt = Utils.JsonToDateTime(strDate);
    return Utils.AbvrMonths[dt.getMonth()] + ' ' + dt.getFullYear().toString();
}

Utils.StringddmmyyyyToDate = function (str) {
    if (IsNullOrEmpty(str))
        return null;
    var arr = str.split('/');
    if (arr.length == 3) {
        return new Date(arr[2], Number(arr[1]) - 1, arr[0]);
    }
    return null;
}

Date.prototype.getWeek = function () {
    var onejan = new Date(this.getFullYear(), 0, 1);
    return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
} 

Date.prototype.toLocalJSON = (function () {

    function addZ(n) {
        return (n < 10 ? '0' : '') + n;
    }
    return function () {
        if (this == 'Invalid Date' || this == null)
            return '';

        return this.getFullYear() + '-' +
            addZ(this.getMonth() + 1) + '-' +
            addZ(this.getDate()) + 'T' +
            addZ(this.getHours()) + ':' +
            addZ(this.getMinutes()) + ':' +
            addZ(this.getSeconds()) + '.000Z';
    };
}())

// credits: http://stackoverflow.com/questions/563406/add-days-to-datetime
Date.prototype.addDays = function (days) {
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
}
Date.prototype.addMinutes = function (minutes) {
    var date = new Date(this.valueOf());
    return new Date(date.getTime() + minutes * 60000);
}


Utils.GetDatePickerDate = function ($obj) {
    if (!IsNullOrEmpty($obj) && $obj.val() != "") {
        return $obj.datepicker("getDate")
        // return $obj.data('datepicker').date;
    }
    return null;
}
Utils.GetJsonDate = function ($obj) {
    var date = this.GetDatePickerDate($obj);
    if (date != null)
        return date.toJSON();
    else
        return null;
}
Utils.JsonToDateTime = function (date) {
    if (!IsNullOrEmpty(date)) {
        return new Date(date.match(/\d+/)[0] * 1);
    }
    return null;
}
Utils.JsonToDateTimeFormat = function (date) {
    if (!IsNullOrEmpty(date)) {
        return new Date(date.match(/\d+/)[0] * 1).toLocaleDateString();
    }
    return null;
}

Utils.JsonToPeriodDateFormat = function (date) {
    if (!IsNullOrEmpty(date)) {
        return new Date(date.match(/\d+/)[0] * 1).getMonthPeriod();
    }
    return null;
}

/**
 * Format a Number to -> ###.###.###,##
 * @param {number} the number to be formatted.
 * @return {string} formatted number as string.
 */
Utils.FormatNumber = function (number, numberDecimals) {
    if (numberDecimals == undefined) {
        numberDecimals = 2;
    }
    if (IsNullOrEmpty(number))
        number = 0;

    if (isNaN(number) && !isNaN(number.toString().replace(',', '.')))
        number = Number(number.toString().replace(',', '.'));

    var number = Number(number).toFixed(numberDecimals) + '';
    var x = number.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? ',' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}

Utils.UnFormatNumber = function (number) {
    if (IsNullOrEmpty(number))
        return 0;

    number = number.replaceAll('.', '');
    number = number.replace(',', '.');
    return Number(number);
}

// o server recebe virgulas em vez de pontos
Utils.DecimalToServerFormat = function (val) {
    if (!IsNullOrEmpty(val)) {
        return val.toString().replace(".", ",")
    }
    return '';
}
//var privateUnformat = function (val) {
//    if (!IsNullOrEmpty(val)) {
//        var t = Utils.UnFormatNumber(val);
//        if (t != NaN) {
//            return t.toString().replace(".", ",");
//        }
//    }
//    return '';
//}


Utils.NewTab = function (url) {
    var win = window.open(url, '_blank');
    win.focus();
}

Date.prototype.getMonthPeriod = function () {
    var month = this.getMonth() + 1;
    return this.getFullYear() + "/" + (month < 10 ? '0' + month : '' + month); // ('' + month) for string result
}

String.prototype.replaceAll = function (value, repaceTo) {
    var me = this.toString()
    while (me.indexOf(value) > -1) {
        me = me.replace(value, repaceTo);
    }
    return me;
}

//Syncfusion
String.format = function () {
    for (var t = arguments[0], n = 0; n < arguments.length - 1; n++)
        t = t.replace(new RegExp("\\{" + n + "\\}", "gm"), arguments[n + 1]);
    return t.replace(/\{[0-9]\}/g, "")
}

String.prototype.format = String.prototype.f = function () {
    var s = this,
        i = arguments.length;

    while (i--) {
        s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
    }
    return s;
};

String.prototype.removeHtml = function () {
    return this.replace(new RegExp('<[^>]*>', 'gm'), '');
}

String.prototype.endsWith = function (suffix) {
    return (this.substr(this.length - suffix.length) === suffix);
}

String.prototype.startsWith = function (prefix) {
    return (this.substr(0, prefix.length) === prefix);
}

Utils.ToFixed = function (val, n, append) {
    if (IsNullOrEmpty(n))
        n = 2;

    if (!isNaN(Number(val))) {
        if (!IsNullOrEmpty(append)) {
            return Number(val).toFixed(n) + append
        }
        return Number(val).toFixed(n);
    }
    return val;
}


Utils.StrDateToDate = function (strDate) {
    //strDate="dd/MM/yyyy"
    var parts = strDate.split('/');
    return new Date(parts[2], parts[1] - 1, parts[0]);

    //new Date(parts[2].toString()+"-"+05-02T00:00:00Z")
}

Utils.Separators = function (value, separator) {
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, separator);
}

Utils.ScrolllTo = function (el, offeset) {
    var pos = (el && el.size() > 0) ? el.offset().top : 0;

    if (el) {
        if ($('body').hasClass('page-header-fixed')) {
            pos = pos - $('.page-header').height();
        }
        pos = pos + (offeset ? offeset : -1 * el.height());
    }

    $('html,body').animate({
        scrollTop: pos
    }, 'slow');
}

//ESTOU A USAR NO FILE WIZARD
Utils.GenericTable = function (tableName) {

    if (!$().dataTable) {
        return;
    }

    var table = $('#' + tableName);
    var oTable = table.dataTable({

        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "Sem registos",
            "info": "_START_ a _END_ de _TOTAL_ registos",
            "infoEmpty": "N&atilde;o foram encontrados registos ",
            "infoFiltered": "(filtrados de _MAX_)",
            "lengthMenu": "Mostrar _MENU_",
            "search": "Procurar:",
            "zeroRecords": "N&atilde;o foram encontrados registos."
        },
        "columnDefs": [{
            "orderable": false,
            "targets": [0]
        }],
        "order": [
            [4, 'desc']
        ],
        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 5,
    });
    var tableWrapper = $('#' + tableName + '_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper
    var tableColumnToggler = $('#' + tableName + '_column_toggler');

    /* modify datatable control inputs */
    tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
    /* Get the DataTables object again - this is not a recreation, just a get of the object */
    $('#' + tableName + '_column_toggler input[type="checkbox"]', tableColumnToggler).change(function () {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        oTable.fnSetColumnVis(iCol, $(this).is(":checked"));
    });

    $('#' + tableName + '_column_toggler  input[type=checkbox]').each(function () {
        var iCol = parseInt($(this).attr("data-column"));
        oTable.fnSetColumnVis(iCol, $(this).is(":checked"));
    });
}

Utils.KeepOneCheckbox = function ($container) {
    $container.find("input[type=checkbox]").on('change', function (ev) {
        if ($container.find("input[type=checkbox]:checked").length == 0)
            $.uniform.update($(this).attr('checked', 'checked'));
    });
}
Utils.getParamByName = function (formData, name) {
    var element = "";
    $.each(formData, function (idx, elm) {
        if (elm.name == name) {
            element = elm;

            return element;
        }
    });
    return element
}

Utils.getParamsByNameSelects = function (formData, name) {
    var elements = [];
    $.each(formData, function (idx, elm) {
        if (elm.name == name) {
            elements.push(elm.value);
        }
    });
    return elements;
}

Utils.BasicTable = function (tableName) {

    if (!$().dataTable) {
        return;
    }

    var table = $('#' + tableName);
    var oTable = table.dataTable({
        "dom": '<"top"><"clear"><"bottom"><"clear">',
        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "Sem registos",
            "info": "_START_ a _END_ de _TOTAL_ registos",
            "infoEmpty": "N&atilde;o foram encontrados registos ",
            "infoFiltered": "(filtrados de _MAX_)",
            "lengthMenu": "Mostrar _MENU_",
            "search": "Procurar:",
            "zeroRecords": "N&atilde;o foram encontrados registos."
        },
        "columnDefs": [{
            "orderable": false,
            "targets": [0]
        }],
        "order": [
            [4, 'desc']
        ],
        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 5,
    });
    var tableWrapper = $('#' + tableName + '_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper
    var tableColumnToggler = $('#' + tableName + '_column_toggler');

    /* modify datatable control inputs */
    tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
    /* Get the DataTables object again - this is not a recreation, just a get of the object */
    $('#' + tableName + '_column_toggler input[type="checkbox"]', tableColumnToggler).change(function () {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        oTable.fnSetColumnVis(iCol, $(this).is(":checked"));
    });

    $('#' + tableName + '_column_toggler  input[type=checkbox]').each(function () {
        var iCol = parseInt($(this).attr("data-column"));
        oTable.fnSetColumnVis(iCol, $(this).is(":checked"));
    });
}

Utils.HtmlError = function (message, title) {
    if (IsNullOrEmpty(title))
        title = "Erro"
    return '<div class="alert alert-danger alert-dismissable"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button> <strong>' + title + '</strong> ' + message + ' </div>';

}

Utils.HtmlSuccess = function (message, title) {
    if (IsNullOrEmpty(title))
        title = "Sucesso"
    return '<div class="alert alert-success alert-dismissable"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button> <strong>' + title + '</strong> ' + message + ' </div>';
}

Utils.ChangeEditForm = function (edit, form) {
    if (edit) {

    } else {

    }
}

Utils.ChecAttr = function (element, attr, val) {
    if (!$(element).hasAttr(attr)) { $(element).attr(attr, val); }
}

Utils.GenHtmlTable = function (colList, tblId, classList) {
    var strClass = '';
    if (IsNullOrEmpty(classList)) {
        strClass = 'table table-striped table-bordered table-hover';
    } else {
        strClass = classList.join(' ');
    }

    var tbl = '<table class="' + strClass + '" id="' + tblId + '"><thead><tr>';
    for (var i = 0; i < colList.length; i++) {
        tbl = tbl + '<th>' + colList[i] + ' </th>';
    }

    return tbl + '</tr> </thead><tbody> </tbody> </table>';
}

Utils.GenHtmlInfoNote = function (id, message, title) {
    if (IsNullOrEmpty(title))
        title = "Informação!"
    if (IsNullOrEmpty(id))
        id = (Math.random() * 10000).toFixed();
    return '<div id=' + id + ' class="note note-info"><strong>' + title + '</strong> &nbsp; &nbsp; ' + message + ' </div>';
}

Utils.GenHtmlErrorNote = function (id, message, title) {
    if (IsNullOrEmpty(title))
        title = "Erro!"
    if (IsNullOrEmpty(id))
        id = (Math.random() * 10000).toFixed();
    return '<div id=' + id + ' class="note note-danger"><strong>' + title + '</strong> &nbsp; &nbsp; ' + message + ' </div>';
}
Utils.GenHtmlSuccessNote = function (id, message, title) {
    if (IsNullOrEmpty(title))
        title = "Sucesso!"
    if (IsNullOrEmpty(id))
        id = (Math.random() * 10000).toFixed();
    return '<div id=' + id + ' class="note note-success"><strong>' + title + '</strong> &nbsp; &nbsp; ' + message + ' </div>';
}

Utils.GenRowPortlet = function (id, title, colorclass) {
    // row-id =id da row
    // body-id = id do body
    if (IsNullOrEmpty(colorclass))
        colorclass = 'red-flamingo'
    var portlet = '';
    portlet = portlet.concat('<div class="row" id="row-' + id + '">');
    portlet = portlet.concat('<div class="col-md-12">');
    portlet = portlet.concat('<div class="portlet box ' + colorclass + '">');
    portlet = portlet.concat('<div class="portlet-title">');
    portlet = portlet.concat('<div class="caption">');
    portlet = portlet.concat('<i class="fa fa-globe"></i>' + title + '');
    portlet = portlet.concat('</div>');
    portlet = portlet.concat('<div class="tools">');
    portlet = portlet.concat('<a href="javascript:;" class="collapse" data-original-title="" title="">');
    portlet = portlet.concat('</a>');
    portlet = portlet.concat('<a href="" class="fullscreen" data-original-title="" title="">');
    portlet = portlet.concat('</a>');
    portlet = portlet.concat('<a href="javascript:;" class="remove" data-original-title="" title="">');
    portlet = portlet.concat('</a>');
    portlet = portlet.concat('</div>');
    portlet = portlet.concat('</div>');
    portlet = portlet.concat('<div class="portlet-body" id="body-' + id + '">');
    portlet = portlet.concat('</div>');
    portlet = portlet.concat('</div>');
    portlet = portlet.concat('</div>');
    portlet = portlet.concat('</div>');
    return portlet;
}

Utils.CreatePortlet = function (id, md_col, portletClass, title, titleIcon, titleClass, contentToAppend, withoutRow, removeClassBtn) {
    var portlet = '';
    if (removeClassBtn === undefined) {
        removeClassBtn = "";
    }
    if (withoutRow == 1) {
        portlet = portlet.concat('<div class="col-md-' + md_col + '">');
        portlet = portlet.concat('<div class="portlet ' + portletClass + '">');
        portlet = portlet.concat('<div class="portlet-title">');
        portlet = portlet.concat('<div class="caption">');
        portlet = portlet.concat('<i class="' + titleIcon + '"></i><span class="' + titleClass + '">' + title + '</span>');
        portlet = portlet.concat('</div>');
        portlet = portlet.concat('<div class="tools">');
        portlet = portlet.concat('<a href="javascript:;" class="collapse" data-original-title="" title="">');
        portlet = portlet.concat('</a>');
        portlet = portlet.concat('<a href="" class="fullscreen" data-original-title="" title="">');
        portlet = portlet.concat('</a>');
        portlet = portlet.concat('<a href="javascript:;" class="remove ' + removeClassBtn + '" data-original-title="" title="">');
        portlet = portlet.concat('</a>');
        portlet = portlet.concat('</div>');
        portlet = portlet.concat('</div>');
        portlet = portlet.concat('<div class="portlet-body" id="' + id + '">');
        portlet = portlet.concat(contentToAppend);
        portlet = portlet.concat('</div>');
        portlet = portlet.concat('</div>');
        portlet = portlet.concat('</div>');
    } else {
        portlet = portlet.concat('<div class="row" id="row' + id + '">');
        portlet = portlet.concat('<div class="col-md-' + md_col + '">');
        portlet = portlet.concat('<div class="portlet ' + portletClass + '">');
        portlet = portlet.concat('<div class="portlet-title">');
        portlet = portlet.concat('<div class="caption">');
        portlet = portlet.concat('<i class="' + titleIcon + '"></i><span class="' + titleClass + '">' + title + '</span>');
        portlet = portlet.concat('</div>');
        portlet = portlet.concat('<div class="tools">');
        portlet = portlet.concat('<a href="javascript:;" class="collapse" data-original-title="" title="">');
        portlet = portlet.concat('</a>');
        portlet = portlet.concat('<a href="" class="fullscreen" data-original-title="" title="">');
        portlet = portlet.concat('</a>');
        portlet = portlet.concat('<a href="javascript:;" class="remove ' + removeClassBtn + '" data-original-title="" title="">');
        portlet = portlet.concat('</a>');
        portlet = portlet.concat('</div>');
        portlet = portlet.concat('</div>');
        portlet = portlet.concat('<div class="portlet-body" id="' + id + '">');
        portlet = portlet.concat(contentToAppend);
        portlet = portlet.concat('</div>');
        portlet = portlet.concat('</div>');
        portlet = portlet.concat('</div>');
        portlet = portlet.concat('</div>');
    }
    return portlet;
}

Utils.downloadFile = function (action, params) {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', action, true);
    xhr.responseType = 'arraybuffer';
    xhr.onload = function () {
        if (this.status === 200) {
            var filename = "";
            var disposition = xhr.getResponseHeader('Content-Disposition');
            if (disposition && disposition.indexOf('attachment') !== -1) {
                var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                var matches = filenameRegex.exec(disposition);
                if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
            }
            var type = xhr.getResponseHeader('Content-Type');

            var blob = new Blob([this.response], { type: type });
            if (typeof window.navigator.msSaveBlob !== 'undefined') {
                // IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
                window.navigator.msSaveBlob(blob, filename);
            } else {
                var URL = window.URL || window.webkitURL;
                var downloadUrl = URL.createObjectURL(blob);

                if (filename) {
                    // use HTML5 a[download] attribute to specify filename
                    var a = document.createElement("a");
                    // safari doesn't support this yet
                    if (typeof a.download === 'undefined') {
                        window.location = downloadUrl;
                    } else {
                        a.href = downloadUrl;
                        a.download = filename;
                        document.body.appendChild(a);
                        a.click();
                    }
                } else {
                    window.location = downloadUrl;
                }

                setTimeout(function () { URL.revokeObjectURL(downloadUrl); }, 100); // cleanup
            }
        }
    };
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.onloadstart = function () {
        Loading();
    }
    xhr.onerror = function () {
        StopLoading();
    }
    xhr.onabort = function () {
        StopLoading();
    }
    xhr.onreadystatechange = function () {
        if (xhr.readyState == XMLHttpRequest.DONE) {
            StopLoading();
        }
    }
    xhr.send($.param(params));
}

Utils.CreateLightPortlet = function (id, md_col, portletClass, fontIconDesc, title, subtitle, titleIcon, titleClass, contentToAppend) {
    var portlet = '';

    portlet = portlet.concat('<div class="row" id="row' + id + '">');
    portlet = portlet.concat('<div class="col-md-' + md_col + '">');
    portlet = portlet.concat('<div class="portlet ' + portletClass + '">');
    portlet = portlet.concat('<div class="portlet-title">');
    portlet = portlet.concat('<div class="caption ' + fontIconDesc + '">');
    portlet = portlet.concat('<i class="' + titleIcon + '"></i><span class="' + titleClass + '">' + title + '</span>' + subtitle);
    portlet = portlet.concat('</div>');
    portlet = portlet.concat('<div class="tools">');
    portlet = portlet.concat('<a href="javascript:;" class="collapse" data-original-title="" title="">');
    portlet = portlet.concat('</a>');
    portlet = portlet.concat('<a href="" class="fullscreen" data-original-title="" title="">');
    portlet = portlet.concat('</a>');
    portlet = portlet.concat('<a href="javascript:;" class="remove" data-original-title="" title="">');
    portlet = portlet.concat('</a>');
    portlet = portlet.concat('</div>');
    portlet = portlet.concat('</div>');
    portlet = portlet.concat('<div class="portlet-body" id="' + id + '">');
    portlet = portlet.concat(contentToAppend);
    portlet = portlet.concat('</div>');
    portlet = portlet.concat('</div>');
    portlet = portlet.concat('</div>');
    portlet = portlet.concat('</div>');

    return portlet;
}

Utils.ColorLuminance = function (hex, lum) {
    // validate hex string
    hex = String(hex).replace(/[^0-9a-f]/gi, '');
    if (hex.length < 6) {
        hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
    }
    lum = lum || 0;
    // convert to decimal and change luminosity
    var rgb = "#", c, i;
    for (i = 0; i < 3; i++) {
        c = parseInt(hex.substr(i * 2, 2), 16);
        c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
        rgb += ("00" + c).substr(c.length);
    }
    return rgb;
}

Utils.initDateRangepicker = function (elementId, leftOrRight) {

    if (!jQuery().daterangepicker) {
        return;
    }

    if (IsNullOrEmpty(leftOrRight)) {
        leftOrRight = 'right';
    }

    $(elementId).daterangepicker({
        opens: leftOrRight,// (Metronic.isRTL() ? 'left' : 'right'),

        separator: ' a ',
        //startDate: moment().subtract('days', 29),
        //endDate: moment(),
        minDate: '01/01/2000',
        //maxDate: moment(),
        showDropdowns: true,
        buttonClasses: ['btn'],
        applyClass: 'green',
        cancelClass: 'default',
        language: "pt",
        locale: {
            applyLabel: 'OK',
            fromLabel: 'De',
            toLabel: 'A',
            daysOfWeek: WeekAbrv, // array declarado no _layout
            monthNames: Months, // array declarado no _layout
            firstDay: 1,
            format: 'DD/MM/YYYY',
        }
    },
        function (start, end) {
            $(elementId + ' input').val('De ' + start.format('DD/MM/YYYY') + ' a ' + end.format('DD/MM/YYYY'));
            $(elementId).attr("data-start-date", start.toJSON());
            $(elementId).attr("data-end-date", end.toJSON());
        }
    );
    $(document).on("change", elementId + ' input', function () {
        if ($(this).val() == '') {
            $(elementId).removeAttr("data-start-date");
            $(elementId).removeAttr("data-end-date");
            var drp = $(elementId).data('daterangepicker');
            drp.setStartDate('01/01/2000');
            var today = new Date();
            drp.setEndDate(today.getDay() + "/" + today.getMonth() + "/" + today.getFullYear());
        }
    });


}

Utils.DateRangepickerSetDates = function (elementSelector, dtIni, dtFim) {

    var dt = $(elementSelector).data('daterangepicker');
    if (!IsNullOrEmpty(dt)) {
        dt.setStartDate(dtIni);
        dt.setEndDate(dtFim);
        dt.notify();
    }
}

Utils.openPortlet = function (portlet) {
    var portletBody = $(portlet).find(".portlet-body")
    $(portletBody).first().removeClass("display-hide");
    $(portletBody).first().slideDown("slow");
    $(portlet).find(".tools a.expand").first().removeClass("expand").addClass("collapse");
}

Utils.closePortlet = function (portlet) {
    var portletBody = $(portlet).find(".portlet-body")
    $(portletBody).first().addClass("display-hide");
    $(portletBody).first().slideUp("slow");
    $(portlet).find(".tools a.collapse").first().removeClass("collapse").addClass("expand");
}

Utils.createPanelDashboard = function (colorClass, id, title, iconClass, value, text) {
    var dashboard = "";
    dashboard = dashboard.concat("<div class='row'>");
    dashboard = dashboard.concat("<div class='col-md-3'>");
    dashboard = dashboard.concat('<div class="dashboard-stat ' + colorClass + ' ol-kpi" id="' + id + '-wrap">');
    dashboard = dashboard.concat('<div class="visual">');
    dashboard = dashboard.concat('<div class="desc">');
    dashboard = dashboard.concat('<small>' + title + '</small>');
    dashboard = dashboard.concat('</div>');
    dashboard = dashboard.concat('<i class="fa ' + iconClass + '"></i>');
    dashboard = dashboard.concat('</div>');
    dashboard = dashboard.concat('<div class="details">');
    dashboard = dashboard.concat('<div class="number" id="' + id + '">');
    dashboard = dashboard.concat(value);
    dashboard = dashboard.concat('</div>');
    dashboard = dashboard.concat('<div class="desc">');
    dashboard = dashboard.concat(text);
    dashboard = dashboard.concat('</div>');
    dashboard = dashboard.concat('</div>');
    dashboard = dashboard.concat('</div>');
    dashboard = dashboard.concat('</div>');
    dashboard = dashboard.concat('</div>');

    return dashboard;
}

//Just add .fixed-dropdown class and call this function - fix overflow bug 
// https://github.com/angular-ui/bootstrap/issues/1030
Utils.FixBsDropdown = function () {
    $('body').on('shown.bs.dropdown.evt-toggle', '.fixed-dropdown', function (evt) {
        // store the dropdown menu for later use
        var $dropdown = $(this),
            $dropdown_menu = $dropdown.find('.dropdown-menu');


        var off = $dropdown_menu.offset();
        $dropdown_menu.css("left", -100);
        // clone this dropdown menu
        var $fixed_dropdown_menu = $dropdown_menu.clone(true);

        // hide this dropdown menu
        $dropdown_menu.addClass('invisible');

        // position the cloned dropdown menu relative to the document body and show it

        //  v1.css('left', v2.offset().left).css('top', v2.offset().top);
        $fixed_dropdown_menu.css({ 'display': 'block' }).css('left', off.left).css('top', off.top - 20).appendTo('body');

        //$fixed_dropdown_menu.css({ 'display': 'block' }).offset(off).appendTo('body');
        //$fixed_dropdown_menu.css('top', $fixed_dropdown_menu.position().top - 20 + 'px');

        // pass-through click events to original dropdown menu
        var i = 0;
        $fixed_dropdown_menu.find('a').each(function () {
            var $related_anchor = $dropdown_menu.find('a').eq(i);

            $(this).on('click', function () {
                //$dropdown_menu.removeClass('invisible');
                $related_anchor.click();
            });

            i++;
        });

        // remove the cloned dropdown
        $dropdown.on('hidden.bs.dropdown', function () {
            $dropdown_menu.removeClass('invisible');
            $fixed_dropdown_menu.remove();
        });
    });
}

Utils.FixBsDropdown20 = function () {
    $('body').on('shown.bs.dropdown.evt-toggle', '.fixed-dropdown', function (evt) {
        // store the dropdown menu for later use
        var $dropdown = $(this);

        // remove the cloned dropdown
        $dropdown.on('hidden.bs.dropdown', function () {
            $dropdown.closest(".table-scrollable").css('overflow-y', 'hidden');
            $dropdown.closest(".table-scrollable").css('overflow-x', 'auto');
        });

        $dropdown.on('shown.bs.dropdown', function () {
            $dropdown.closest(".table-scrollable").css('overflow-y', 'visible');
            $dropdown.closest(".table-scrollable").css('overflow-x', 'visible');
        });

        return false;
    });
}
Utils.checkboxTableActions = function (table) {

    //Geral.checkboxes();

    $(document).on("click", table + ' .checkAll', function () {
        var result = $(this).attr("checked") == undefined ? false : true;
        $(table + " .check").prop("checked", result).trigger('change');
        $.uniform.update(table + " .check");
    });


    $(document).on("click", table + ' .check', function () {
        if ($(table + " .check:checked").length < $(table + " .check").length) {
            $(table + ' .checkAll').attr("checked", false);

        } else {
            $(table + ' .checkAll').attr("checked", true);
        }
        $.uniform.update(table + " .checkAll");
    });

    $(document).on('change', table + " tr>td input[type=checkbox]", function () {
        if ($(this).is(':checked'))
            $(this).closest('tr').addClass('active');
        else
            $(this).closest('tr').removeClass('active');
    });
}

Utils.checkboxTableOptions = function (table, triggerBtn) {

    Geral.checkboxes();

    $(table + ' .checkAll').on("click", function () {
        var result = $(this).attr("checked") == undefined ? false : true;
        $(table + " .check").prop("checked", result).trigger('change');
        $.uniform.update(table + " .check");
        if (result == true) {
            $(triggerBtn).removeAttr("disabled");
        }
        else {
            $(triggerBtn).attr("disabled", true);
        }
    });


    $(table + ' .check').on("click", function () {
        if ($(table + " .check:checked").length < $(table + " .check").length) {
            $(table + ' .checkAll').attr("checked", false);

        } else {
            $(table + ' .checkAll').attr("checked", true);
            $(triggerBtn).removeAttr("disabled");
        }
        if ($(this).is(':checked')) {
            $(triggerBtn).removeAttr("disabled");
        } else if (!$(this).is(':checked') && $(table + " .check:checked").length == 0) {
            $(triggerBtn).attr("disabled", true);
        }
        $.uniform.update(table + " .checkAll");
    });

    $(table + " tr>td input[type=checkbox]").on('change', function () {
        if ($(this).is(':checked'))
            $(this).closest('tr').addClass('active');
        else
            $(this).closest('tr').removeClass('active');
    });


    /* $(table + ' .checkAll').on('ifChecked', function (event) {
         $(table + ' .check').iCheck('check');
         triggeredByChild = false;
         $(triggerBtn).removeAttr("disabled");
     });
 
     $(table + ' .checkAll').on('ifUnchecked', function (event) {
         if (!triggeredByChild) {
             $(table + ' .check').iCheck('uncheck');
             $(triggerBtn).attr("disabled", true);
         }
         triggeredByChild = false;
         
     });
 
         
     $(table+' .check').on('ifUnchecked', function (event) {
         triggeredByChild = true;
         $(table + ' .checkAll').iCheck('uncheck');
 
         if ($(table + ' .check').filter(':checked').length == 0) {
             $(triggerBtn).attr("disabled", true);
         }
     });
 
     $(table+' .check').on('ifChecked', function (event) {
         if ($(table+' .check').filter(':checked').length == $(table+' .check').length) {
             $(table + ' .checkAll').iCheck('check');
         }
         $(triggerBtn).removeAttr("disabled");
     });*/
}

Utils.QueryStringAddDate = function (querystring, name, selector) {
    if (IsNullOrEmpty(querystring))
        querystring = '';
    var $obj = $(selector);
    if ($obj.length > 0 && $obj.datepicker('getDate').toJSON() != null) {
        querystring = querystring + '&' + name + '=' + $obj.datepicker('getDate').toJSON();
    }
    return querystring
}

Utils.QueryStringAddInputVal = function (querystring, name, selector) {
    if (IsNullOrEmpty(querystring))
        querystring = '';
    var $obj = $(selector);
    if ($obj.length > 0) {
        querystring = querystring + '&' + name + '=' + $obj.val()
    }
    return querystring
}

Utils.QueryStringAddInputValDecimal = function (querystring, name, selector) {
    if (IsNullOrEmpty(querystring))
        querystring = '';
    var $obj = $(selector);
    if ($obj.length > 0 && !IsNullOrEmpty($obj.val())) {
        var number = Utils.UnFormatNumber($obj.val());
        if (!isNaN(number))
            querystring = querystring + '&' + name + '=' + number.toString().replace('.', ',');
    }
    return querystring
}

Utils.QueryStringMakeArray = function (querystring, strToFind, strToReplace) {
    if (IsNullOrEmpty(strToReplace))
        strToReplace = strToFind;
    var i = 0;

    while (querystring.indexOf(strToFind) > -1) {
        querystring = querystring.replace(strToFind, strToReplace + '[' + i + ']=');
        i = i + 1;
    }
    return querystring;
}

Utils.QueryStringAddParam = function (querystring, name, value) {
    if (IsNullOrEmpty(querystring))
        querystring = '';
    querystring = querystring + '&' + name + '=' + value;
    return querystring
}

//Based On http://stackoverflow.com/questions/1714786/query-string-encoding-of-a-javascript-object
objectToQueryString = function (obj, prefix) {
    if (obj == null)
        return "";
    return Object.keys(obj).map(function ( objKey,i) {

        if (obj.hasOwnProperty(objKey)) {
           // const key = prefix ? `${prefix}[${objKey}]` : objKey;
            const key = prefix ? String.format('{0}[{1}]', prefix, objKey) : objKey;
            const value = obj[objKey];

            return typeof value === "object" && value != null ?
                this.objectToQueryString(value, key) : String.format('{0}={1}', encodeURIComponent(key), encodeURIComponent(value));//`${encodeURIComponent(key)}=${encodeURIComponent(value)}`;
        }

        return null;
    }).join("&");
}

// Credits: in comments-> http://www.developerdrive.com/2013/08/turning-the-querystring-into-a-json-object-using-javascript/ 
function QueryStringToJSON(str) {
    var pairs = str.split('&');
    var result = {};
    pairs.forEach(function (pair) {
        pair = pair.split('=');
        var name = pair[0]
        var value = pair[1]
        if (name.length)
            if (result[name] !== undefined) {
                if (!result[name].push) {
                    result[name] = [result[name]];
                }
                result[name].push(value || '');
            } else {
                result[name] = value || '';
            }
    });
    return (result);
}


Utils.Clone = function (toClone) {
    return JSON.parse(JSON.stringify(toClone));

    // jQuery.extend(true, {}, instance.getSelectedRecords()[0]);
}


//function editinputs() {
//    var ps = form[0].parentElement.parentElement.parentElement.getElementsByClassName('form-control-static');
//    while (ps.length > 0) {
//        ps[0].parentElement.removeChild(ps[0]);
//    }
//    var inps = form[0].parentElement.parentElement.parentElement.getElementsByTagName('input');
//    for (var i = 0, n = inps.length; i < n; i++) {
//        if (inps[i].type == "text") {
//            inps[i].style.display = '';
//        } else {
//            if (inps[i].type == "checkbox") {
//                inps[i].removeAttribute("disabled");
//            }
//        }
//    }
//}
//$(document).ready(function () {
//    $('.datepicker').datepicker();
//});



function IsNullOrEmpty(s) {
    if (s == null || s === "" || s == undefined)
        return true;
    return false;
}


(function ($) {

    $.fn.hideColumn = function (columnindex) {

        this.each(function () {
            if ($(this).find('td:nth-child(' + columnindex + '),th:nth-child(' + columnindex + ')').length > 0)
                $(this).find('td:nth-child(' + columnindex + '),th:nth-child(' + columnindex + ')').hide();
        });
        return this;
    };

}(jQuery));

(function ($) {

    $.fn.showColumn = function (columnindex) {

        this.each(function () {
            if ($(this).find('td:nth-child(' + columnindex + '),th:nth-child(' + columnindex + ')').length > 0)
                $(this).find('td:nth-child(' + columnindex + '),th:nth-child(' + columnindex + ')').show();
        });
        return this;
    };

}(jQuery));

(function ($) {
$.fn.enterKey = function (fnc) {
    return this.each(function () {
        $(this).keypress(function (ev) {
            var keycode = (ev.keyCode ? ev.keyCode : ev.which);
            if (keycode == '13') {
                fnc.call(this, ev);
            }
        })
    })
}
}(jQuery));

(function ($) {
    $.fn.hasAttr = function (attr) {
        return !IsNullOrEmpty($(this).attr(attr));
    };
}(jQuery));


var serializeTable = function (selector) {
    var dt = selector.dataTable();
    var nodes = dt.fnGetNodes();
    var dataserialized = '';
    $.each(nodes, function (index, value) {
        $(value).find('td').each(function (i, o) {
            if (!IsNullOrEmpty($(this).attr('data-col-name'))) {
                //encodeURIComponent(
                dataserialized += $(this).attr('data-col-name').replace('[X]', '[' + index + ']') + '=' + $(this).text().trim().replace(' ', '+') + '&';
            }
        });
    });
    return dataserialized;
}