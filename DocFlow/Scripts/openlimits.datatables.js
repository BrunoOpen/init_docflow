﻿
function colNumeric(name) {
    return {
        data: name, orderDataType: "custom-numeric", "type": "numeric", render: function (data, type, row) {
            return type === 'export' ?
            Utils.UnFormatNumber(data) : data;
        }
    }
}

function colNumericTitle(name, colTitle) {
    return {
        data: name, title:colTitle , orderDataType: "custom-numeric", "type": "numeric", render: function (data, type, row) {
            return type === 'export' ?
            Utils.UnFormatNumber(data) : data;
        }
    }
}

function colTooltip(name, colTitle) {
    return {
        data: name, title: colTitle, render: function (data, type, row) {
            return type === 'export' ? $.map($(data), function (i, o) { return $(i).data('original-title') }).toString() : data;
        }
    }
}

function colNumeric(name) {
    return {
        data: name, orderDataType: "custom-numeric", "type": "numeric", render: function (data, type, row) {
            return type === 'export' ?
            Utils.UnFormatNumber(data) : data;
        }
    }
}

// $.fn.dataTable.ext.order['custom-numeric'] = function (settings, col) {
//            //return this.api().column(col, { order: 'index' }).nodes().map(function (td, i) {
//            //    return $('input', td).val();
//            //});

//            //return this.api().column(col, { order: 'index' }).nodes().map(function (td, i) {
//            //     return Utils.UnFormatNumber($(td).html());
//            // });
//            return this.api().column(col, { order: 'index' }).data().map(function (td, i) {
//                return Utils.UnFormatNumber(td);
//            });
//        };
//function colDateMMMMYYYY(name) {
//    return {
//        data: name, orderDataType: "custom-numeric", "type": "numeric", render: function (data, type, row) {
//            return type === 'export' ?
//            Utils.UnFormatNumber(data) : data;
//        }
//    }
//}

function htmlEncode(html) {
    return document.createElement('a').appendChild(
        document.createTextNode(html)).parentNode.innerHTML;
}

function exportButtons() {
    return {
        dom: {
            button: { tag: 'li' },
            container: { tag: 'ul', className: 'dropdown-menu pull-right' },

            buttonLiner: {
                tag: 'a' //a volta do texto
            }
        },
        buttons: [
            { extend: 'excelHtml5', text: ' <i class="fa fa-file-excel-o"></i> ' + Language.EXPORT_EXCEL, exportOptions: { orthogonal: 'export', columns: ':visible' } },
            { extend: 'pdfHtml5', text: '  <i class="fa  fa-file-pdf-o"></i></i> ' + Language.EXPORT_PDF, orientation: 'landscape',exportOptions: { orthogonal: 'export', columns: ':visible' } },
            { extend: 'print', text: ' <i class="fa fa-print"></i> ' + Language.PRINT, orientation: 'landscape',exportOptions: { columns: ':visible' } },
        ]
    }
}