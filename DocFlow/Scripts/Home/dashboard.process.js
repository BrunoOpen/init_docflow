﻿
ProcessDashboard = new Object();
ProcessDashboard.Date = null;

ProcessDashboard.Reload = function (jsonMonthDate, $element,url) {
    $.ajax({
        type: 'POST',
        url: url,
        data: { month: jsonMonthDate },
        success: function (data) {

            $element.html(data);
            if (ProcessDashboard.SelfInit)
                ProcessDashboard.InitOwnDatepicker(url);
        }
    });
};


ProcessDashboard.InitOwnDatepicker = function (url) {
    if (!jQuery().datepicker) {
        return;
    }
    ProcessDashboard.SelfInit = true;
    $("#my-process-datepicker").show();

    if ($('#my-process-Period').length) {

        $('#my-process-Period').datepicker({
            rtl: false,
            orientation: 'right',
            //buttonClasses: ['btna'],
            //applyClass: ' blue',
            cancelClass: 'default',
            format: 'MM yyyy',
            viewMode: "months",
            minViewMode: "months",
            language: "pt",
            autoclose: "true",
            endDate: (new Date())
        });

        if (IsNullOrEmpty(ProcessDashboard.Date))
            ProcessDashboard.Date = Utils.StrDateToDate($("#tbMonth").val())
        $('#my-process-Period').datepicker('setDate', ProcessDashboard.Date);


        $('#my-process-Period input')[0].setAttribute("value", Months[ProcessDashboard.Date.getMonth()] + " " + ProcessDashboard.Date.getFullYear());

        $('#my-process-Period').datepicker().on('changeDate', function (ev) {
            $("#tbMonth").val(ev.date.getDate().toString() + "/" + (ev.date.getMonth() + 1).toString() + "/" + ev.date.getFullYear().toString());
            ProcessDashboard.Date = ev.date;
            ProcessDashboard.Reload(ev.date.toJSON(), $("#process-logs"), url);
        });
    }
    
};

