﻿var ExampleSearch = function () {

    var _getSingleFormData = function () {
        var formdata = $(ExampleSearch.QuickSearchFormId).serialize();
        return formdata;
    }

    var _getAdvancedFormData = function () {

        var formdata = $(ExampleSearch.AdvancedSearchFormId).serialize();

        // Date Range
        var dateIni = $('#example-dates-range').attr("data-start-date");
        var dateEnd = $('#example-dates-range').attr("data-end-date");
        if (!IsNullOrEmpty(dateIni)) { formdata += '&StartDate=' + dateIni }
        if (!IsNullOrEmpty(dateEnd)) { formdata += '&EndDate=' + dateEnd }

        // Date Picker
        var date = $('#simple-date input').datepicker('getDate');
        if (date != null)
            formdata += '&SingleDate=' + date.toLocalJSON();

        // Checkboxes
        formdata = formdata.replaceAll("=on", "=true");
        formdata = formdata.replaceAll("=off", "=false");

        return formdata;
    }

    var _processResults = function (data) {


        if (data && data.Data.length > 0) {
            var yes = '<i class="fa fa-check"></i>';
            var no = '<i class="fa fa-times"></i>';
            $.each(data.Data, function (index, obj) {

                obj.Link = '<a target="_blank" href="/Events/Details/id=' + obj.Id + '" > ' + obj.Email + ' </a>';
                obj.Date = Utils.JsonToDateTimeFormat(obj.Date);
                obj.Active = obj.Active == true ? yes : no;
                obj.Decimal = IsNullOrEmpty(obj.Decimal) ? "" : Utils.FormatNumber(obj.Decimal);


            });
            ExampleSearch.Table.clear();
            ExampleSearch.Table.rows.add(data.Data).columns.adjust().draw();

        } else {
            ExampleSearch.Table.clear().draw();

        }
        StopLoading();
    }

    var _initResultsTable = function () {

        ExampleSearch.Table = $(ExampleSearch.TableId).DataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": Language.DATATABLE_LANGUAGE,
            "order": [
                [1, 'desc']
            ],
            "lengthMenu": Language.DATATABLE_PAGE_LENGTHS,
            // set the initial value
            "pageLength": 15,
            columns: [
                    { data: 'Name' },
                    { data: 'Date' },
                    { data: 'Decimal' },
                    { data: 'Active' },
                    { data: 'Link' }
            ],

            buttons: exportButtons(),
        });

        var tableWrapper = $(ExampleSearch.TableId + '_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper
        var tableColumnToggler = $(ExampleSearch.TableId + '_column_toggler');

        /* modify datatable control inputs */
        tableWrapper.find('.dataTables_length select').selectpicker(); // initialize select2 dropdown

        /* handle show/hide columns*/
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        $('input[type="checkbox"]', tableColumnToggler).change(function () {
            /* Get the DataTables object again - this is not a recreation, just a get of the object */
            var iCol = parseInt($(this).attr("data-column"));
            ExampleSearch.Table.column(iCol).visible($(this).is(":checked"));
        });

        $('input[type="checkbox"]', tableColumnToggler).each(function () {
            var iCol = parseInt($(this).attr("data-column"));
            ExampleSearch.Table.column(iCol).visible($(this).is(":checked"));
        });

        ExampleSearch.Table.buttons().container().appendTo($('#ExportDrop'));
        //$("#ExportDrop ul").append('<li><a class="export-excel-meter"  href="javascript:void(0);"> <i class="fa fa-download"></i> Gerar Ficheiro de Leituras</a></li>');
       
    }

    var _initControls = function () {

        Utils.initDateRangepicker('#example-dates-range');

        $('#simple-date input').datepicker({
            rtl: Metronic.isRTL(),
            orientation: "bottom",
            autoclose: true,
            format: 'dd/mm/yyyy',
            language: "pt"
        });

        $('#records-spinner').spinner({ value: 1, step: 1, min: 1, max: 999 });
    }


    return {
        init: function () {
            ExampleSearch.QuickSearchFormId = "#bs-QuickSearchForm"; //manter este id
            ExampleSearch.AdvancedSearchFormId = "#bs-AdvancedSearchForm"; //manter este id
            ExampleSearch.TableId = "#results-tablename";
             _initResultsTable();

            _initControls();

            Search.init(ExampleSearch.QuickSearchFormId, ExampleSearch.AdvancedSearchFormId, _getSingleFormData, _getAdvancedFormData, _processResults);

            //hack para remover 1º item que aparece vazio porque aparecem 2. creio que é porque existem 2 selects com o mesmo id
            //$($("#searchAdvancedForm #EventType_Value option")[0]).remove();
            //$("#searchAdvancedForm #EventType_Value").selectpicker('refresh');

            //if ($("#InitSearch").val() == "1") {
            //    $("#advanced-search").trigger('click');
            //    $("#btnSubmitSearch").trigger('click');
            //}
        }
    };
}();

