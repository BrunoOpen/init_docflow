﻿using DocFlow.Services;
using System.Web.Mvc;

namespace DocFlow.Controllers
{
    [AuthorizeUser]
    public class AdminController : Controller
    {
        //
        // GET: /Permission/

        public ActionResult Index()
        {
            return View();
        }

        [AuthorizeActions("ADM_USR_ROLE_SEARCH")]
        public ActionResult Roles()
        {
            RolesService service = new RolesService();
            return View(service.Search());
        }

        [AuthorizeActions("ADM_USR_SEARCH")]
        public ActionResult Users()
        {
            UserService service = new UserService();
            return View(service.Search());
        }

    }
}
