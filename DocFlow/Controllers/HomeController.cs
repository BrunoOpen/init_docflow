﻿using DocFlow.Core;
using DocFlow.Core.Extensions;
using DocFlow.Core.Objects;
using DocFlow.Data.Dashboards;
using DocFlow.ViewModels;
using DocFlow.ViewModels.Dashboard;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DocFlow.Controllers
{

    [AuthorizeUser]
    public class HomeController : BaseController
    {

        #region Old Methods
        public ActionResult Index()
        {
            var currentInstance = SessionManager.GetActiveInstance();
            if (currentInstance == null)
            {
                SessionManager.SetActiveInstance(SharedService.GetInstances().FirstOrDefault(u => u.IsDefault == true));
            }
            
            ViewBag.Database = SessionManager.GetActiveInstance().Name;
         ViewBag.BDInfo=   SP.GetBDInfo();

        
            return View("Dashboard");
         
        }
        

        public ActionResult ChangeLanguage(int langId, string returnUrl)
        {
            DocFlow.ViewModels.Language lang = SharedService.GetLanguageById(langId);
            SessionManager.SetActiveCulture(lang);
            this.AuthService.GetMenu(true);
            return Redirect(returnUrl);
        }

        public ActionResult ChangeInstance(int instanceid, string returnUrl)
        {
            InstanceRecord inst = SharedService.GetInstances().FirstOrDefault(u => u.Id == instanceid);
            SessionManager.SetActiveInstance(inst);
            this.AuthService.GetMenu(true);
            return Redirect(returnUrl);
        }

        public ActionResult MenuOptions()
        {
            TopMenuViewModel model = new TopMenuViewModel()
            {
                Languages = SharedService.GetAppLanguages(),
                Instances=SharedService.GetInstances(),
                //Templates = SharedService.GetAppTemplates()
            };

            var currentInstance = SessionManager.GetActiveInstance();
            if (currentInstance != null)
                model.CurrentInstance = currentInstance;
            else
            {
                model.CurrentInstance = model.Instances.FirstOrDefault(u=>u.IsDefault==true);
                SessionManager.SetActiveInstance(model.CurrentInstance);
            }

            var currentLanguage = SessionManager.GetActiveCulture();
            var defaultlang = SharedService.GetDefaultLanguage().Code;

            if (currentLanguage != null)
                defaultlang = currentLanguage.Code;

            model.Languages.Where(l => l.Code == defaultlang).ForEach(e => { e.Selected = true; });
            model.Languages.Where(l => l.Code != defaultlang).ForEach(e => { e.Selected = false; });


            model.Services = SharedService.GetServices();

            model.CurrentService = new Service
            {
                Type = SessionManager.GetActiveService(),
                Selected = true
            };

            if (User.IsInAction("DASH_NOTIF_PROC"))
            {
                //   model.FileNotifications = FileService.GetFileProcessingLogs(month, true);
                model.FileNotifications = Mocking.MockFileNotifications();
            }

            return PartialView("MenuOptions", model);
        }

      
        public ActionResult Menu()
        {
            MenuViewModel model = AuthService.GetMenu();
            return PartialView("Menu", model);
        }

        public ActionResult ChangeService(int service, string returnUrl)
        {
            SessionManager.SetActiveService((Enums.Services)service);
            return Redirect(returnUrl);
        }

        public ActionResult DownloadTemplate(TemplateModel templat)
        {

            string filepath = AppDomain.CurrentDomain.BaseDirectory + "/FileTemplates/" + templat.FileName;
            byte[] filedata = System.IO.File.ReadAllBytes(filepath);
            string contentType = MimeMapping.GetMimeMapping(filepath);

            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = templat.FileName,
                Inline = true,
            };
            Response.AppendHeader("Content-Disposition", cd.ToString());

            return File(filedata, contentType);
        }

        public ActionResult GetMinMonth()
        {

            DateTime month = new DateTime(2006,1,1);// SharedService.GetMinMonth(SessionManager.GetActiveService());
            return Json(month, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetTypeData(int? type)
        {
            DropDownListEditModel typeDataSelect = this.SharedService.GetSelectTypeData(type);
            return PartialView("_typeDataSelect", typeDataSelect);
        }

        #endregion

        #region NewMethods
        public ActionResult DashboardKPI(DateTime month)
        {
            DataOperationResult<List<DashLastValue>> result = this.DashBoardService.GetKPI(month);
            return PartialView("~/Views/Home/Partials/KPIs.cshtml", result.Data);
        }

        public ActionResult DashboardLists(DateTime month)
        {
            DataOperationResult<List<DashboardList>> result = this.DashBoardService.GetLists(month);
            return PartialView("~/Views/Home/Partials/Lists.cshtml", result.Data);
        }


        public ActionResult DashboardChartsPartial(DateTime month)
        {
            DataOperationResult<List<ChartWrapper>> result = this.DashBoardService.GetCharts(month);
            return PartialView("~/Views/Home/Partials/Charts.cshtml", result.Data);
        }

        public ActionResult DashboardCharts(int colValueType, int lineValueType, DateTime month)
        {
            DataOperationResult<List<ChartWrapper>> result = this.DashBoardService.GetCharts(month);
            return PartialView("~/Views/Home/Partials/Charts.cshtml", result.Data);
        }

        public ActionResult GetChart(int ValueTypeId, int? LineTypeId, DateTime month, int charttype)
        {
            //  return Content(string.Empty);

            var config = this.DashBoardService.InitConfiguration();
            if (charttype == 1)
            {
                config.RemoveAt(1);
                config.First().Buttons.ForEach(u => u.Selected = false);
                config.First().Buttons.Where(u => u.LineTypeId == LineTypeId && u.ValueTypeId == ValueTypeId).ForEach(u => u.Selected = true);
            }
            if (charttype == 2)
            {
                config.RemoveAt(0);
                config.First().Buttons.ForEach(u => u.Selected = false);
                config.First().Buttons.Where(u => u.ValueTypeId == ValueTypeId).ForEach(u => u.Selected = true);
            }
          

            DataOperationResult<List<ChartWrapper>> result = this.DashBoardService.GetCharts(month, config, LineTypeId);
            ChartWrapper model = result.Data.First();
            model.Buttons.Where(f => f.Active).ForEach(u => u.Active = false);
       
            model.Buttons.Where(f => f.ValueTypeId == ValueTypeId && LineTypeId == f.LineTypeId).ForEach(u => u.Active = true);

            if(charttype==2)
                model.Buttons.Where(f => f.ValueTypeId == ValueTypeId ).ForEach(u => u.Active = true);
            return PartialView("~/Views/Home/Partials/Chart.cshtml", model);
        }


        #endregion
    }
}
