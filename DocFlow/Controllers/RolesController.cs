﻿using DocFlow.Services;
using DocFlow.ViewModels;
using System.Web.Mvc;

namespace DocFlow.Controllers
{
    [Authorize]
    public class RolesController : Controller
    {
        //
        // GET: /Roles/

        public ActionResult Index()
        {
            return View();
        }

        [AuthorizeActions("ADM_USR_ROLE_NEW")]
        public ActionResult New()
        {
            RolesService service = new RolesService();
            UserProfileEditModel model = new UserProfileEditModel();
            model.ActionsGroups = service.GetAllActionsGroups();
            return View(model);
        }

        [AuthorizeActions("ADM_USR_ROLE_EDIT")]
        public ActionResult Edit(int roleid)
        {
            RolesService service = new RolesService();
            UserProfileEditModel model = service.GetRoleDetails(roleid, true);
            return View(model);
        }

        [HttpPost]
        [AuthorizeActions("ADM_USR_ROLE_EDIT", "ADM_USR_ROLE_NEW")]
        public ActionResult Save(UserProfileEditModel role)
        {
            RolesService service = new RolesService();
            service.SaveRole(role);
            return Json(role.Id, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeActions("ADM_USR_ROLE_VIEW")]
        public ActionResult Details(int roleid)
        {
            RolesService service = new RolesService();
            UserService uservice = new UserService();
            UserProfileEditModel model = service.GetRoleDetails(roleid, false);
            model.Users = uservice.SearchUsersByRole(roleid);
            return View(model);
        }

         [AuthorizeActions("ADM_USR_ROLE_DEL")]
        public ActionResult Delete(int roleid)
        {
            RolesService service = new RolesService();
            service.DeleteRole(roleid);
            return RedirectToAction("Details", new { roleid = roleid });
        }

    }
}
