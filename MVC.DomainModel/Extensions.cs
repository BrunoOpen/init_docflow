﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocFlow.Data
{
    public static class Extensions
    {
        public static string ToRoundFormat(this decimal? value, int decimals)
        {
            if (value.HasValue)
                return value.Value.ToRoundFormat(decimals);
            else return (0m).ToRoundFormat(decimals);
        }

        public static string ToRoundFormat(this decimal value, int decimals)
        {
            return value.ToString(string.Format("N{0}", decimals), new System.Globalization.CultureInfo("de-DE"));
        }

        public static string ToNumberFormat(this decimal? value)
        {
            if (value.HasValue)
                return value.Value.ToString("N2", new System.Globalization.CultureInfo("de-DE"));
            else return "0,00";
        }

        public static string ToNumberOrNULLFormat(this decimal? value)
        {
            if (value.HasValue)
                return value.Value.ToString("N2", new System.Globalization.CultureInfo("de-DE"));
            else return "";
        }
        public static string ToNumberOrNULLFormat4(this decimal? value)
        {
            if (value.HasValue)
                return value.Value.ToString("N4", new System.Globalization.CultureInfo("de-DE"));
            else return "";
        }

        public static string ToNumberFormat(this decimal value)
        {
            return value.ToString("N2", new System.Globalization.CultureInfo("de-DE"));
        }

        public static string ToNumberFormat(this int value)
        {
            return value.ToString("N0", new System.Globalization.CultureInfo("de-DE"));
        }

        public static string ToNumberFormat(this int? value)
        {
            if (value.HasValue)
                return value.Value.ToString("N0", new System.Globalization.CultureInfo("de-DE"));
            else return "0";
        }

        public static string ToMeteringFormat(this DateTime value)
        {
            return value.ToString("dd/MM/yyyy");
        }


        
    }
}
