﻿
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

using System;
using System.Data.Linq;

namespace DocFlow.Data.Repository
{
    public class NewGenericSProcRepository
    {
        private string connection;

        public NewGenericSProcRepository(string cnn)
        {
            this.connection = cnn;
        }

        public virtual List<T> GetList<T>(string spName, SqlParameter[] sqlParams)
        {
            List<T> list;
            using (var context = new DocDBDataContext(connection))
            {
                string spExec = CreateSpExec(spName, sqlParams);
                list = context.ExecuteQuery<T>(spExec).ToList();
            }
            return list;
        }

        public virtual void ExecProcedure(string spName, SqlParameter[] sqlParams)
        {
            using (var context = new DocDBDataContext(connection))
            {
                string spExec = CreateSpExec(spName, sqlParams);
                context.ExecuteCommand(spExec);
            }
        }

        public virtual T GetSingle<T>(string spName, SqlParameter[] sqlParams)
        {
            T dataset;
            using (var context = new DocDBDataContext(connection))
            {
                string spExec = CreateSpExec(spName, sqlParams);
                dataset = context.ExecuteQuery<T>(spExec).FirstOrDefault();
            }
            return dataset;
        }

        private string CreateSpExec(string spName, params SqlParameter[] sqlParams)
        {
            string spExec = spName;
            foreach (SqlParameter sqlParam in sqlParams)
            {
                if (sqlParam.Value != System.DBNull.Value)
                    spExec = spExec + " " + sqlParam.ParameterName + " = " + (sqlParam.Value == null ? "null" : "'" + sqlParam.SqlValue.ToString() + "'") + ", ";
            }
            spExec = spExec.Trim().Trim(',');
            using (var context = new DocDBDataContext(connection))
            {
                Table<log_DebugLog> dbSet = context.GetTable<log_DebugLog>();
                dbSet.InsertOnSubmit(new log_DebugLog()
                {
                    UserID = null,
                    LogDate = DateTime.Now,
                    Context = "SPExec",
                    ContextID = null,
                    Description = spExec
                });
                context.SubmitChanges();
            }

            return spExec;

        }
    }
}
