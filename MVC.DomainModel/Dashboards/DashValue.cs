﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocFlow.Data.Dashboards
{
  
    public class DashValue
    {
        public string Text { get; set; }
        public decimal Value { get; set; }
        public int Decimals { get; set; }
        public string Icon { get; set; }
        public string Color { get; set; }
        public string Link { get; set; }
        public string Size { get; set; }
    }
}
