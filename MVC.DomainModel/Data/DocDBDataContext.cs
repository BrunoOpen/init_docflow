﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocFlow.Data
{
    public partial class DocDBDataContext
    {
        public DocDBDataContext() : base(ConfigurationManager.ConnectionStrings["SupervisionEntities"].ConnectionString)
        {
            OnCreated();
        }

    }
}
