﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocFlow.Data
{
    public class TableColumnsToRows
    {
        public string Table { get; set; }
        public string Title { get; set; }
        public int Id { get; set; }
        public string Column { get; set; }
        public string Value { get; set; }
    }
}
