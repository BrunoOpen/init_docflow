﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocFlow.Data
{
  
    public class ChartValueDetails
    {
        public DateTime MonthDate { get; set; }
        public decimal? ColumnValue { get; set; }
        public string DetailName { get; set; }
        public string Color { get; set; }
    }
}
