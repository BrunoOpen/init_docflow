﻿using MVC.Business;
using MVC.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVC.UnitTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(" ---------------------------------------------------------------------------- ");
            Console.WriteLine(" -------------------------- EMPTY MVC Project Data -------------------------- ");
            Console.WriteLine(" ---------------------------------------------------------------------------- ");
            Console.WriteLine(" ------------------------------------ END ------------------------------------ ");
            Console.ReadLine();
        }
    }
}
