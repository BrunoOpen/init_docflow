﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using MVC.DomainModel;

namespace MVC.Data
{
    public interface IGenericDataRepository<T> where T : class, IEntity
    {
        List<T> GetAll(params Expression<Func<T, object>>[] navigationProperties);
        List<T> GetList(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties);
        T GetSingle(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties);
        void Add(params T[] items);
        void Update(params T[] items);
        void Remove(params T[] items);
    }
}
