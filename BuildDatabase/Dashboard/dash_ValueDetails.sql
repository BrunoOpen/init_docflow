﻿CREATE TABLE [dbo].[dash_ValueDetails]
(
	[ValueDetailID] INT NOT NULL IDENTITY, 
    [ValueTypeID] INT NOT NULL, 
    [DetailID] INT NOT NULL, 
    [Value] DECIMAL(18, 4) NOT NULL, 
	[Description] NVARCHAR(250) NOT NULL, 
    [Link] NVARCHAR(250) NULL, 
    [Period] INT NULL, 
    CONSTRAINT [PK_dash_ValueDetails] PRIMARY KEY ([ValueDetailID]), 
    CONSTRAINT [FK_dash_ValueDetails_ValueTypes] FOREIGN KEY ([ValueTypeID]) REFERENCES [dash_ValueTypes]([ValueTypeID]), 
    CONSTRAINT [FK_dash_ValueDetails_Details] FOREIGN KEY ([DetailID]) REFERENCES [dash_Details]([DetailID]) 
)
