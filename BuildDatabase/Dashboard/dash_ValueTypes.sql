﻿CREATE TABLE [dbo].[dash_ValueTypes]
(
	[ValueTypeID] INT NOT NULL IDENTITY, 
    [SourceID] INT NOT NULL, 
    [Name] NVARCHAR(150) NOT NULL, 
    [Rounding] INT NULL, 
    [Unit] NVARCHAR(50) NULL, 
    [Icon] NVARCHAR(50) NULL, 
    [Color] NVARCHAR(50) NULL,  
    [CreatedOn] DATETIME2(7) NOT NULL DEFAULT GETDATE(), 
    [CreatedBy] INT NOT NULL DEFAULT 1, 
    [ModifiedOn] DATETIME2 NULL, 
    [ModifiedBy] INT NULL DEFAULT 1, 
    CONSTRAINT [PK_dash_ValueTypes] PRIMARY KEY ([ValueTypeID]), 
    CONSTRAINT [FK_dash_ValueTypes_Sources] FOREIGN KEY ([SourceID]) REFERENCES [dash_Sources]([SourceID])  
)

GO

CREATE TRIGGER [dbo].[Tr_dash_ValueTypesModifiedOn]
    ON [dbo].[dash_ValueTypes]
    AFTER UPDATE
    AS
    BEGIN
		SET NOCOUNT ON

        UPDATE dash_ValueTypes
		SET ModifiedOn = GETDATE()
		FROM inserted i 
		WHERE dash_ValueTypes.ValueTypeID = i.ValueTypeID
    END
