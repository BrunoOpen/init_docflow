﻿CREATE TABLE [dbo].[dash_DetailTypes]
(
	[DetailTypeID] INT NOT NULL IDENTITY, 
    [Name] NVARCHAR(100) NOT NULL, 
    [MainTable] NVARCHAR(100) NULL,  
    [CreatedOn] DATETIME2(7) NOT NULL DEFAULT GETDATE(), 
    [CreatedBy] INT NOT NULL DEFAULT 1, 
    [ModifiedOn] DATETIME2 NULL, 
    [ModifiedBy] INT NULL DEFAULT 1, 
    CONSTRAINT [PK_dash_DetailTypes] PRIMARY KEY ([DetailTypeID])  
)

GO

CREATE TRIGGER [dbo].[Tr_dash_DetailTypesModifiedOn]
    ON [dbo].[dash_DetailTypes]
    AFTER UPDATE
    AS
    BEGIN
		SET NOCOUNT ON

        UPDATE dash_DetailTypes
		SET ModifiedOn = GETDATE()
		FROM inserted i 
		WHERE dash_DetailTypes.DetailTypeID = i.DetailTypeID
    END
