﻿CREATE TABLE [dbo].[dash_Calculations]
(
	[Id] BIGINT NOT NULL IDENTITY, 
	[ExecOrder] INT NOT NULL, 
    [Name] NVARCHAR(150) NOT NULL, 
    CONSTRAINT [PK_dash_Calculations] PRIMARY KEY ([Id]), 
)

GO