﻿CREATE TABLE [dbo].[TextModel]
(
	[Id] INT NOT NULL IDENTITY, 
    [TextModelTypeID] INT NOT NULL, 
    [Description] NVARCHAR(MAX) NOT NULL, 
    [Quantity] INT NULL, 
    [CreatedOn] DATETIME2 NOT NULL DEFAULT GETDATE(), 
	[CreatedBy] INT NULL , 
    [ModifiedOn] DATETIME2 NULL, 
	[ModifiedBy] INT NULL, 
    [RowVersion] TIMESTAMP NOT NULL, 
    CONSTRAINT [PK_TextModel] PRIMARY KEY ([Id]), 
    CONSTRAINT [FK_TextModel_TextModelType] FOREIGN KEY ([TextModelTypeID]) REFERENCES [TextModelType]([Id]) 
)

GO

CREATE TRIGGER [dbo].[TR_TextModel_ModifiedOn]
    ON [dbo].[TextModel]
    AFTER UPDATE
    AS
    BEGIN
		SET NOCOUNT ON;
        UPDATE dbo.TextModel
		SET ModifiedOn = GETDATE()
		FROM inserted i 
		WHERE dbo.TextModel.Id = i.Id
    END

GO


