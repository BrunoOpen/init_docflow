﻿CREATE TABLE [dbo].[EditColumn]
(
	[Id] INT NOT NULL IDENTITY, 
    [EditTableID] INT NOT NULL, 
    [Name] NVARCHAR(250) NOT NULL, 
    CONSTRAINT [PK_EditColumn] PRIMARY KEY ([Id]), 
    CONSTRAINT [FK_EditColumn_EditTable] FOREIGN KEY ([EditTableID]) REFERENCES [EditTable]([Id]) 
)
