﻿CREATE TABLE [dbo].[log_SPLog]
(
	[SPLogID] INT NOT NULL IDENTITY, 
    [SPName] NVARCHAR(200) NOT NULL, 
    [StartDate] DATETIME2(3) NOT NULL, 
    [EndDate] DATETIME2(3) NOT NULL, 
    [NRows] INT NOT NULL, 
    [Params] NVARCHAR(MAX) NULL, 
    [Execution] NVARCHAR(MAX) NULL, 
    CONSTRAINT [PK_log_SPLog] PRIMARY KEY ([SPLogID]) 
)
