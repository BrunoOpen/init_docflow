﻿CREATE FUNCTION [dbo].[GetRoundPeriods]
(
	@MonthDate datetime
)
RETURNS @returntable TABLE
(
	MinPeriod int,
	MaxPeriod int
)
AS
BEGIN
	INSERT @returntable
	select
		MIN(b.Period) as MinPeriod,
		MAX(b.Period) as MaxPeriod
	from
		(
			select top 12
				a.Period,
				ABS(DATEDIFF(MONTH,a.MonthDate,@MonthDate)) as dist
			from
				Periods a
			where
				case when a.MonthDate > @MonthDate then ABS(DATEDIFF(MONTH,a.MonthDate,@MonthDate)) else 5 end < 6
	order by Period desc) b
	RETURN
END
