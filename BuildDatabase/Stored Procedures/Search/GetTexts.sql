﻿CREATE PROCEDURE [dbo].[GetTexts]
AS
	SELECT
		Id = t.Id,
        TextType = tt.Name,
        Text = t.Description
	FROM
		TextModel t
		inner join TextModelType tt on t.TextModelTypeID = tt.Id
	ORDER BY
		tt.Id,
		t.Description
RETURN 0
