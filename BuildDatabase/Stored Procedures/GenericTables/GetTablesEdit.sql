﻿CREATE PROCEDURE [dbo].[GetTablesEdit]
AS
	SET NOCOUNT ON;

	declare @TableResult table (
		[Table] nvarchar(250),
        [Title] nvarchar(max),
        [Id] int,
        [Column] nvarchar(250),
        [Value] nvarchar(max)
	)

	insert into @TableResult([Table],Title,Id,[Column],[Value])
	SELECT
		'ActivitySector',
		'Setor Atividade',
		1,
		'Id',
		cast('1' as nvarchar)

	SELECT
		*
	FROM
		@TableResult

RETURN 0
