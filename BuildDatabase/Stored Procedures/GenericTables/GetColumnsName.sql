﻿CREATE PROCEDURE [dbo].[GetColumnsName]
	@TableName nvarchar(max)
AS
	SELECT
		[Key] = cast(c.Id as nvarchar),
        [Value] = c.Name
	FROM
		EditTable t
		inner join EditColumn c on t.Id = c.EditTableID
	WHERE
		t.Name = @TableName
RETURN 0
