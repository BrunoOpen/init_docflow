﻿CREATE PROCEDURE [dbo].[GetList]
	@SourceID int,
	@MonthDate datetime
AS
BEGIN
	declare @LogDate datetime2(3) = GETDATE()
	declare @Params nvarchar(max) = '@SourceID='+case when @SourceID is null then 'null' else cast(@SourceID as nvarchar) end+',
	@MonthDate='''+case when @MonthDate is null then 'null' else CONVERT(nvarchar,@MonthDate,112) end+''''

	DECLARE @Period int = YEAR(@MonthDate)*100+MONTH(@MonthDate)
	
	SELECT
		replace(v.Description,'{X}', cast(ROUND(v.Value,vt.Rounding) as float)) [Text],
		v.Value [Value],
        vt.Rounding [Decimals],
        vt.Icon [Icon],
        vt.Color [Color],
        v.Link [Link]
	FROM
		dash_Values v
		inner join dash_ValueTypes vt on v.ValueTypeID = vt.ValueTypeID
	WHERE
		(v.Period = @Period or v.Period is null)
		and vt.SourceID = @SourceID
		
	insert into log_SPLog(SPName,StartDate,EndDate,NRows,Params,Execution)
	values (OBJECT_NAME(@@PROCID),@LogDate,GETDATE(),@@ROWCOUNT,@Params,OBJECT_NAME(@@PROCID)+' '+@Params)

	RETURN 0
END