﻿CREATE PROCEDURE [dbo].[GetGraph]
	@ColumnValueTypeID int,
	@LineValueTypeID int,
	@MonthDate datetime
AS
BEGIN
	declare @LogDate datetime2(3) = GETDATE()
	declare @Params nvarchar(max) = '@ColumnValueTypeID='+case when @ColumnValueTypeID is null then 'null' else cast(@ColumnValueTypeID as nvarchar) end+',
	@LineValueTypeID='+case when @LineValueTypeID is null then 'null' else cast(@LineValueTypeID as nvarchar) end+',
	@MonthDate='''+case when @MonthDate is null then 'null' else CONVERT(nvarchar,@MonthDate,112) end+''''

	declare @ValueTable table (MonthDate datetime, ColumnValue decimal(18,4), LineValue decimal(18,4), Color nvarchar(50))

	insert into @ValueTable(MonthDate,ColumnValue,LineValue,Color)
	SELECT
		DATEFROMPARTS(v.Period/100,v.Period%100,1) MonthDate,
		sum(case when v.ValueTypeID = @ColumnValueTypeID then v.Value else null end) ColumnValue,
		sum(case when v.ValueTypeID = @LineValueTypeID then v.Value else null end) LineValue,
		--sum(case when v.ValueTypeID = @ColumnValueTypeID then v.Value else null end)/count(distinct v.Period) AverageValue,
		max(case when v.ValueTypeID = @ColumnValueTypeID then vt.Color else null end) Color
	FROM
		dash_Values v
		inner join GetRoundPeriods(@MonthDate) per on v.Period between per.MinPeriod and per.MaxPeriod
		inner join dash_ValueTypes vt on v.ValueTypeID = vt.ValueTypeID
	WHERE
		v.ValueTypeID in (@ColumnValueTypeID,@LineValueTypeID)
	GROUP BY
		DATEFROMPARTS(v.Period/100,v.Period%100,1)
	
	select
		t.MonthDate,
		t.ColumnValue,
		t.LineValue,
		(select sum(a.ColumnValue)/count(distinct a.MonthDate) from @ValueTable a) AverageValue,
		t.Color
	from
		@ValueTable t

	insert into log_SPLog(SPName,StartDate,EndDate,NRows,Params,Execution)
	values (OBJECT_NAME(@@PROCID),@LogDate,GETDATE(),@@ROWCOUNT,@Params,OBJECT_NAME(@@PROCID)+' '+@Params)
	RETURN 0
END